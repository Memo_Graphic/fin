using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ActionPlanElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_HourText;

    [Header("Images")]
    [SerializeField] private Image m_ActionImage;
    
    [Header("GameObjects")]
    [SerializeField] private GameObject m_FrameObj;

    public UIMainPage uiPage;
    public bool isSleep;
    public bool haveData;
    public int index;

    // Start is called before the first frame update
    void Start()
    {
        uiPage = UIManager.Instance.uIElement[(int)State.UIState.MainPage].GetComponent<UIMainPage>();
        UpdateUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (StateManager.Instance.currentState == State.GameState.Story_11_2 && index == 0)
        {
            UIManager.Instance.uIElement[(int)State.UIState.TutorialPage].GetComponent<UITutorialPage>().SetArrow(m_ActionImage.gameObject);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_12_2 && index == -2)
        {
            // Debug.Log(index);
            UIManager.Instance.uIElement[(int)State.UIState.TutorialPage].GetComponent<UITutorialPage>().SetArrow(gameObject.transform.GetChild(0).GetChild(0).gameObject);
        }
    }

    public void UpdateUI()
    {
        haveData = index >= 0;
        m_FrameObj.SetActive(isSleep || haveData);

        if (isSleep)
        {
            m_HourText.text = CalManager.Instance.TextMinTime(PlayerManager.Instance.playerData.sleepTimeInHour * CalManager.Instance.MIN_IN_HOUR, "HH");
        }
        else
        {
            if (haveData)
            {
                int hour = ActionManager.Instance.activityPlanner[index].time * ActionManager.Instance.activityPlanner[index].timeCost;
                m_HourText.text = CalManager.Instance.TextMinTime(hour, "HH");

                if (m_ActionImage && ActionManager.Instance.activityPlanner[index].activityImage)
                {
                    m_ActionImage.sprite = ActionManager.Instance.activityPlanner[index].activityImage;
                }
            }
        }
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }

    public void OnClickSlot()
    {
        
        if (isSleep)
        {
            uiPage.OpenSetSleep();
        }
        else
        {
            if (haveData)
            {
                uiPage.UpdateSetAction(index);
                uiPage.OpenSetAction();
            }
            else
            {
                uiPage.OpenSelectAction(-1);
            }

            if (StateManager.Instance.currentState == State.GameState.Story_11_2 && index == 0)
            {
                StateManager.Instance.NextState();
            }
            else if (StateManager.Instance.currentState == State.GameState.Story_12_2 && index == -2)
            {
                StateManager.Instance.NextState();
            }
        }
        
    }
}
