using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlannerElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_ActionNameText;
    [SerializeField] private TextMeshProUGUI m_ActionTimeText;
    [SerializeField] private TextMeshProUGUI m_ActionUseText;

    [Header("GameObjects")]
    [SerializeField] private GameObject m_PanelSelect;

    public string actionType;
    public State.Expense type;
    public int actionTime;

    public Activity action;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateUI()
    {
        m_ActionNameText.text = action.activity;
        m_ActionTimeText.text = actionTime.ToString("N0") + " ครั้ง";
        m_ActionUseText.text = "ใช้พลังกาย: " + (actionTime * action.health) + ", พลังใจ: " + (actionTime * action.happiness);
    }

    public void OnClickSelectActionBtn(string _ActionType)
    {
        this.GetComponent<RectTransform>().sizeDelta = new Vector2(this.GetComponent<RectTransform>().sizeDelta.x, this.GetComponent<RectTransform>().sizeDelta.y + 0.1f);
        
        actionType = _ActionType;
        switch(actionType) 
        {
        case "Drink01":
            type = State.Expense.Drink;
            break;
        case "Movie01":
            type = State.Expense.Activity;
            break;
        case "Game01":
            type = State.Expense.Media;
            break;
        default:
            actionType = "";
            break;
        }

        action = ActionManager.Instance.FindActivity(actionType, type);
        m_PanelSelect.SetActive(false);
        UpdateUI();
    }

    public void OnClickChangeTime(int _Value)
    {   
        actionTime += _Value;
        if (actionTime > 5)
        {
            actionTime = 5;
        }
        else if (actionTime < 0)
        {
            actionTime = 0;
        }
        UpdateUI();
        // UIManager.Instance.uIElement[(int)State.UIState.PlannerPage].GetComponent<UIPlannerPage>().CalAddingTime();
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
