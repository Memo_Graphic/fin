using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LottoElement : MonoBehaviour
{   
    [Header("TMP - Lotto")]
    [SerializeField] private TextMeshProUGUI[] m_NumberText;

    [Header("TMP - LottoHistory")]
    [SerializeField] private TextMeshProUGUI m_DrawDateText;
    [SerializeField] private TextMeshProUGUI m_AmountText;
    [SerializeField] private TextMeshProUGUI m_costText;
    [SerializeField] private TextMeshProUGUI m_winText;

    [Header("TMP - GameObject")]
    [SerializeField] private GameObject[] m_winLostObj;
    [SerializeField] private Image m_lottoImg;
    [SerializeField] private Image m_PanelImg;
    [SerializeField] private Sprite m_CurrentSprt;
    [SerializeField] private Sprite m_OldSprt;
    [SerializeField] private Color m_OldColor;

    private int index;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetLotto(int _Index)
    {
        index = _Index;
        string numText = BankManager.Instance.lottoData.lottoNumber[index].ToString("000000");
        for (int i = 0; i < m_NumberText.Length; i++)
        {
            m_NumberText[i].text = numText[i].ToString();
        }

        gameObject.GetComponent<Button>().interactable = CalManager.Instance.Day() % CalManager.Instance.DAY_IN_WEEK != 0;
    }

    public void SetLottoHistory(int _DrawTime, int _Index)
    {
        index = _Index;
        string numText = PlayerManager.Instance.assetData.lottoHistory[_DrawTime].lottery[_Index].lotto.ToString("000000");
        for (int i = 0; i < m_NumberText.Length; i++)
        {
            m_NumberText[i].text = numText[i].ToString();
        }
        m_DrawDateText.text = "งวด " + CalManager.Instance.DateTime("DD/MM/YYYY", _DrawTime * CalManager.Instance.DAY_IN_WEEK);
        m_AmountText.text = "จำนวน " + PlayerManager.Instance.assetData.lottoHistory[_DrawTime].lottery[_Index].amount.ToString("N0") + " ใบ";
        m_costText.text = (PlayerManager.Instance.assetData.lottoHistory[_DrawTime].lottery[_Index].amount * BankManager.Instance.lottoData.lottoPrice).ToString("N0") + " ฿";
        
        if (_DrawTime == BankManager.Instance.lottoData.drawTime)
        {
            m_lottoImg.sprite = m_CurrentSprt;
            m_winLostObj[0].SetActive(false);
            m_winLostObj[1].SetActive(false);
        }
        else
        {
            m_lottoImg.sprite = m_OldSprt;
            m_PanelImg.color = m_OldColor;
            m_winLostObj[0].SetActive(PlayerManager.Instance.assetData.lottoHistory[_DrawTime].lottery[_Index].winAmount > 0);
            m_winLostObj[1].SetActive(!m_winLostObj[0].activeSelf);
            m_winText.text = "ได้รับรางวัล " + PlayerManager.Instance.assetData.lottoHistory[_DrawTime].lottery[_Index].winAmount.ToString("N0") + " ฿";
        }
    }

    public void BuyLotto()
    {
        UIManager.Instance.uIElement[(int)State.UIState.LottoPage].GetComponent<UILotteryPage>().OpenPopup(BankManager.Instance.lottoData.lottoNumber[index]);
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
