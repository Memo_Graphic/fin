using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenURL : MonoBehaviour
{
    [SerializeField] private string m_url;

    // Start is called before the first frame update
    public void OpenURLBtn(string _url)
    {
        Application.OpenURL(_url);
    }
}
