using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LocationElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_SellStatus;

    [Header("GameObjects")]
    [SerializeField] private Color m_Color;
    [SerializeField] private Image m_Land;
    [SerializeField] private Image m_Tree;
    [SerializeField] private Image m_Building;
    [SerializeField] private GameObject m_Floor;
    [SerializeField] private GameObject m_SellFlag;
    [SerializeField] private GameObject m_RentFlag;
    [SerializeField] private GameObject m_OwnFlag;
    [SerializeField] private GameObject m_LiveFlag;
    [SerializeField] private GameObject m_WorkFlag;

    [Header("Sprite")]
    [SerializeField] private Sprite[] m_LandSpt;
    [SerializeField] private Sprite[] m_TreeSpt;

    [Header("Transforms")]
    public Transform m_Pivot;

    private int locationIndex = -1;
    private bool isMap;
    private bool isClicked;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // if (GameManager.Instance.isTutorial)
        // {
        //     if (!isClicked && PlayerManager.Instance.jobData.fullTimeJob.jobID >= 0
        //     && LocationManager.Instance.location[locationIndex].locationType == CalManager.Instance.startHome[(int)GameManager.Instance.gameLevel])
        //     {
        //         UIManager.Instance.uIElement[(int)State.UIState.TutorialPage].GetComponent<UITutorialPage>().SetArrow(m_Building.gameObject);
        //     }
        //     else if (!isClicked && PlayerManager.Instance.jobData.fullTimeJob.jobID < 0
        //     && LocationManager.Instance.location[locationIndex].type == State.Location.Office)
        //     {
        //         UIManager.Instance.uIElement[(int)State.UIState.TutorialPage].GetComponent<UITutorialPage>().SetArrow(m_Building.gameObject);
        //     }
        // }
    }

    public void SetProperty(int _locationIndex)
    {
        locationIndex = _locationIndex;

        if (LocationManager.Instance.location[locationIndex].isLand)
        {
            // m_Floor.SetActive(false);
            m_Building.gameObject.SetActive(false);
            m_Tree.gameObject.SetActive(true);

            int rnd = Random.Range(0, m_TreeSpt.Length);
            int rnd2 = Random.Range(1, m_LandSpt.Length);
            m_Tree.sprite = m_TreeSpt[rnd];
            m_Land.sprite = m_LandSpt[rnd2];
        }
        else
        {
            // m_Floor.SetActive(true);
            m_Building.gameObject.SetActive(true);
            m_Tree.gameObject.SetActive(false);

            m_Building.sprite = LocationManager.Instance.location[locationIndex].locationImage;
            m_Land.sprite = m_LandSpt[0];
        }

        // if (UIManager.Instance.currentState == State.UIState.MainPage
        // || UIManager.Instance.currentState == State.UIState.MapPage)
        // {
        //     isMap = true;
        // }
        // else if (UIManager.Instance.currentState == State.UIState.EstatePage)
        // {
        //     isMap = false;
        // }
        // else
        // {
        //     isMap = false;
        // }

        UpdateFlag();
    }

    public void UpdateFlag()
    {
        // if (isMap)
        // {
        //     m_SellFlag.SetActive(false);
        //     m_RentFlag.SetActive(false);
        //     m_LiveFlag.SetActive(false);
        //     m_OwnFlag.SetActive(false);
        //     m_WorkFlag.SetActive(false);
            
            // if (LocationManager.Instance.location[locationIndex].type == State.Location.Food
            // || LocationManager.Instance.location[locationIndex].type == State.Location.Mall
            // || LocationManager.Instance.location[locationIndex].type == State.Location.Store
            // || LocationManager.Instance.location[locationIndex].type == State.Location.Office
            // || LocationManager.Instance.location[locationIndex].type == State.Location.School)
            // {
            //     m_SellFlag.SetActive(true);
            //     m_SellStatus.text = LocationManager.Instance.location[locationIndex].locationName;
            // }
            // else
            // {
            //     if (locationIndex == LocationManager.Instance.currentHomeLo)
            //     {
            //         m_SellFlag.SetActive(true);
            //         m_SellStatus.text = "บ้าน";
            //     }
            //     else
            //     {
            //         CannotClick();
            //     }
            // }
            
        // }
        // else
        // {
            m_SellFlag.SetActive(false);
            m_RentFlag.SetActive(false);
            m_LiveFlag.SetActive(false);
            m_OwnFlag.SetActive(false);
            m_WorkFlag.SetActive(false);

            if (LocationManager.Instance.location[locationIndex].type == State.Location.Land
            || LocationManager.Instance.location[locationIndex].type == State.Location.Home
            || LocationManager.Instance.location[locationIndex].type == State.Location.Apartment_Condo)
            {

                if (PlayerManager.Instance.haveThisAsset(LocationManager.Instance.location[locationIndex].locationType, State.AssetType.estate))
                {
                    m_OwnFlag.SetActive(true);
                    if (PlayerManager.Instance.IsUseAsset(LocationManager.Instance.location[locationIndex].locationType, State.AssetType.estate))
                    {
                        m_LiveFlag.SetActive(true);
                    }
                }
                else
                {
                    if (AssetManager.Instance.ConvertAsset(LocationManager.Instance.location[locationIndex].locationType, State.AssetType.estate).price > 0
                    && AssetManager.Instance.ConvertAsset(LocationManager.Instance.location[locationIndex].locationType, State.AssetType.estate).rentPrice > 0)
                    {
                        m_SellFlag.SetActive(true);
                    }
                    else if (AssetManager.Instance.ConvertAsset(LocationManager.Instance.location[locationIndex].locationType, State.AssetType.estate).price > 0)
                    {
                        m_SellFlag.SetActive(true);
                    }
                    else if (AssetManager.Instance.ConvertAsset(LocationManager.Instance.location[locationIndex].locationType, State.AssetType.estate).rentPrice > 0)
                    {
                        m_RentFlag.SetActive(true);
                    }
                }
            }
            else
            {
                if (locationIndex == LocationManager.Instance.locationData.currentJobLo && PlayerManager.Instance.jobData.fullTimeJob.jobID >= 0)
                {
                    m_WorkFlag.SetActive(true);
                }
                // else
                // {
                //     CannotClick();
                // }
            }

            if (GameManager.Instance.isTutorial 
            && PlayerManager.Instance.jobData.fullTimeJob.jobID >= 0
            && LocationManager.Instance.location[locationIndex].locationType != CalManager.Instance.startHome[(int)GameManager.Instance.gameLevel])
            {
                CannotClick();
            }
            else if (GameManager.Instance.isTutorial 
            && PlayerManager.Instance.jobData.fullTimeJob.jobID < 0
            && LocationManager.Instance.location[locationIndex].type != State.Location.Office)
            {
                CannotClick();
            }
        // }
    }

    public void OnClickBuilding()
    {
        // if (isMap)
        // {
        //     if (!LocationManager.Instance.location[locationIndex].isLand)
        //     {
        //         LocationManager.Instance.viewLocation = locationIndex;
        //         Location location = LocationManager.Instance.location[locationIndex];
        //         // UIManager.Instance.uIElement[(int)State.UIState.MainPage].GetComponent<UIMainPage>().CloseActionPanel();

        //         List<Activity> action = new List<Activity>();

        //         if (location.type == State.Location.Home || location.type == State.Location.Apartment_Condo)
        //         {
        //             if (PlayerManager.Instance.IsUseAsset(LocationManager.Instance.location[locationIndex].locationType, State.AssetType.estate))
        //             {
        //                 for (int j = 0; j < ActionManager.Instance.activity[(int)State.Location.Home].activity.Length; j++)
        //                 {
        //                     Activity newAction = ActionManager.Instance.activity[(int)State.Location.Home].activity[j];
        //                     action.Add(newAction);
        //                 }
        //             }
        //         }

        //         for (int i = 0; i < location.activityType.Length; i++)
        //         {
        //             int _index = i;
        //             for (int j = 0; j < ActionManager.Instance.activity[(int)location.type].activity.Length; j++)
        //             {
        //                 int _jndex = j;
        //                 if (location.activityType[_index] == ActionManager.Instance.activity[(int)location.type].activity[_jndex].activityType)
        //                 {
        //                     Activity newAction = ActionManager.Instance.activity[(int)location.type].activity[_jndex];
        //                     action.Add(newAction);
        //                     break;
        //                 }
        //             }
        //         }

        //         // UIManager.Instance.uIElement[(int)State.UIState.MainPage].GetComponent<UIMainPage>().OpenActionPanel(action, location.type);
        //     }
        //     else
        //     {
                
        //     }
        // }
        // else
        // {
            if (LocationManager.Instance.location[locationIndex].type == State.Location.Land
            || LocationManager.Instance.location[locationIndex].type == State.Location.Home
            || LocationManager.Instance.location[locationIndex].type == State.Location.Apartment_Condo)
            {
                if (!GameManager.Instance.isTutorial)
                {
                    if (LocationManager.Instance.location[locationIndex].locationType == "Home00")
                    {
                        return;
                    }
                    UIManager.Instance.uIElement[(int)State.UIState.EstatePage].GetComponent<UIEstatePage>().UpdateInfo(LocationManager.Instance.location[locationIndex]);
                    return;
                }
                // if (!GameManager.Instance.isTutorial && LocationManager.Instance.location[locationIndex].locationType == "Home00")
                // {
                //     return;
                // }
                
                isClicked = true;
                StateManager.Instance.NextState();
                UIManager.Instance.uIElement[(int)State.UIState.EstatePage].GetComponent<UIEstatePage>().UpdateInfo(LocationManager.Instance.location[locationIndex]);
                // UIManager.Instance.uIElement[(int)State.UIState.TutorialPage].GetComponent<UITutorialPage>().SetArrow(6);
            }
            else if (LocationManager.Instance.location[locationIndex].type == State.Location.Office
            && PlayerManager.Instance.jobData.fullTimeJob.jobID < 0)
            {
                isClicked = true;
                StateManager.Instance.NextState();
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.GetJob);
                // UIManager.Instance.uIElement[(int)State.UIState.TutorialPage].GetComponent<UITutorialPage>().SetArrow(5);
            }
        // }
    }

    private void CannotClick()
    {
        m_Building.color = m_Color;
        this.GetComponent<Image>().color = m_Color;
        m_Floor.GetComponent<Image>().color = m_Color;
        gameObject.GetComponent<Button>().interactable = false;
    }
    
    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
