﻿public class State
{
    public enum GameState
    {
        None = -1,
        Load,
        Story_1_0,
        Story_2_0,
        Story_3_0,
        Story_4_0,
        Story_5_0,
        Story_6_0,
        Story_7_0,
        Story_8_0,
        Story_9_0,
        Story_10_0,
        Story_10_1,
        Story_10_2,
        Story_11_0,
        Story_11_1,
        Story_11_2,
        Story_12_0,
        Story_12_1,
        Story_12_2,
        Story_13_0,
        Story_13_1,
        Story_14_0,
        Story_15_0,
        Story_16_0,
        Story_17_0,
        Story_18_0,
        Story_19_0,
        Story_20_0,
        Endless
    }

    public enum Level
    {
        None = -1,
        Easy,
        Hard,
        Length
    }

    public enum DayState
    {
        None = -1,
        DailyEvent,
        DayStart,
        Day,
        DayEnd,
        Night,
    }

    public enum UIState
    {
        None = -1,
        MainPage,
        PopupPage,
        TimePage,
        Profile,
        EstatePage,
        JobPage,
        PartTimePage,
        UpSkillPage,
        FeedbackPage,
        BalancePage,
        BillPage,
        StatementPage,
        PaymentPage,
        MapPage,
        StorePage,
        MallPage,
        InvestmentPage,
        CreditCardPage,
        AccountPage,
        // PlannerPage,
        InvestUnitPage,
        LottoPage,
        TaxPage,
        InsurancePage,
        LoanPage,
        CreateCharPage,
        TutorialPage,
        LENGTH
    }

    public enum JobType
    {
        None = -1,
        FullTime,
        PartTime,
        LENGTH
    }

    public enum Skill
    {
        None = -1,
        Skill1
    }

    public enum AssetType
    {
        None = -1,
        car,
        estate,
        stock,
        fund,
        gold,
        business,
        LENGTH
    }

    public enum LiabilityType
    {
        None = -1,
        Rent,
        Loan,
        Shot,
        Personal,
        LENGTH
    }

    public enum Location
    {
        None = -1,
        Land,
        Home,
        Food,
        Mall,
        Store,
        Office,
        School,
        Apartment_Condo,
    }

    public enum Expense
    {
        None = -1,
        Home,
        Car,
        HomeFixedCost,
        CarFixedCost,
        Work,
        Food,
        Drink,
        Item,
        Media,
        Activity,
        UpSkill,
        ETC,
        LENGTH
    }

    public enum MissionGoal
    {
        None = -1,
        MoneyGoal
    }

    public enum Deduction
    {
        None = -1,
        FixedCost,
        Personal,
        DeptEstate,
        LifeInsurance,
        HealthInsurance,
        SSFfund,
        RMFfund,
        Donation,
        LENGTH
    }

    public enum Insurance
    {
        None = -1,
        LifeInsurance,
        HealthInsurance,
        AccidentInsurance,
        LENGTH
    }

    public enum Hospital
    {
        None = -1,
        Health,
        Accident,
        LENGTH
    }

    public enum PopupPage
    {
        Null = -1,
        GetSalaly,
        GetSkill,
        TimeLimit,
        MoneyLimit,
        CreditLimit,
        PowerLimit,
        ChangePayment,
        TaskComplete,
        TaskSlow,
        Scamming,
        ScamWin,
        ScamLose,
        Hospital,
        GetJob,
        Saving,
        GetInterest,
        OverdueLimit,
        Length
    }

    public enum Mission
    {
        None = -1,
        Saving,
        Wealth,
        Invest,
        LENGTH
    }

    public enum BillType
    {
        None = -1,
        FixedCost,
        RentCost,
        Loan,
        Personal,
        CreditCard,
        Insurance,
        Tax,
        LENGTH

    }
}