using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct AccountData
{
    [UnityEngine.Header("Current Account")]
    public List<PlayerAccountElement> account;
    // public double netWorth;
}

[System.Serializable]
public class PlayerAccountElement
{
    public int accountID = 0;
    public string accountName
    {
        get 
        {
            return BankManager.Instance.allAccount.accounts[accountID].accountName;
        }
    }
    public double balance;
    public float accruedExpenses;
}

[System.Serializable]
public class AccountElement
{
    public Account[] accounts;
}

[System.Serializable]
public class Account
{
    [Header("Account")]
    public string accountType = "Account00";
    public string accountName = "accountName";
    public float interestRate;
}



[System.Serializable]
public struct TaxData
{
    public float netIncome
    {
        get
        {
            float net = totalIncome - totalDeduction;
            if (net < 0)
            {
                net = 0;
            }
            return net;
        }
    }
    public float totalIncome;
    public float totalDeduction;
    public float totalTax;
    public TaxElement[] tax;
    public IncomeElement[] income;
    public DeductionElement[] deduction;
}

[System.Serializable]
public class TaxElement
{
    public int taxLength;
    public float taxRate;
    public float taxFill;
    public float taxCal;
}

[System.Serializable]
public class IncomeElement
{
    public string incomeName;
    public float incomeHidden;
    public float incomeSum;
}

[System.Serializable]
public class DeductionElement
{
    public string deductionName;
    public float deductionLimit;
    public float deductionRate;
    public float deductionRec;
    public float deductionCal;
}