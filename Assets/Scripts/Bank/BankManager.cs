﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BankManager : Singleton<BankManager>
{
    public float currentIR;
    public int currentDeptID;
    public int overdueLimit;
    public AccountElement allAccount;
    public CreditCardElement[] allCreditCard;

    public LottoData lottoData;
    public ScamData scamData;

    void Start()
    {

    }

    public bool haveRent()
    {
        return PlayerManager.Instance.liabilityData.Liability[(int)State.LiabilityType.Rent].element.Count > 0;
    }

    public bool haveLiability(State.LiabilityType _Type)
    {
        return PlayerManager.Instance.liabilityData.Liability[(int)_Type].element.Count > 0;
    }

    public bool haveCreditCard()
    {
        return PlayerManager.Instance.liabilityData.creditCard.Count > 0;
    }

    public bool haveCreditCard(string _CardType)
    {
        for (int i = 0; i < PlayerManager.Instance.liabilityData.creditCard.Count; i++)
        {
            if (_CardType == PlayerManager.Instance.liabilityData.creditCard[i].cardType)
            {
                return true;
            }
        }
        return false;
    }

    public bool haveCreditBill()
    {
        bool have = false;
        for (int i = 0; i < PlayerManager.Instance.liabilityData.creditCard.Count; i++)
        {
            for (int j = 0; j < PlayerManager.Instance.liabilityData.creditCard[i].date.Count; j++)
            {
                have = PlayerManager.Instance.liabilityData.creditCard[i].date[j].element.Count > 0;
                if (have)
                {
                    return have;
                }
            }
        }
        return have;
    }

    public void AddLiability(PlayerLiabilityElement _Liability)
    {
        PlayerManager.Instance.liabilityData.Liability[(int)_Liability.type].element.Add(_Liability);
    }

    public void RemoveLiability(string _AssetName, State.LiabilityType _Type)
    {
        for (int i = 0; i < PlayerManager.Instance.liabilityData.Liability[(int)_Type].element.Count; i++)
        {
            if (PlayerManager.Instance.liabilityData.Liability[(int)_Type].element[i].name == _AssetName)
            {
                PlayerManager.Instance.liabilityData.Liability[(int)_Type].element.RemoveAt(i);
                break;
            }
        }
    }

    public void RemoveLiability(int _Index, State.LiabilityType _Type)
    {
        PlayerManager.Instance.liabilityData.Liability[(int)_Type].element.RemoveAt(_Index);
    }
    
    public void AddRent(Asset _Asset)
    {
        PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
        newLiability.name = _Asset.assetName;
        newLiability.type = State.LiabilityType.Rent;
        newLiability.notPaid = false;
        newLiability.installment = _Asset.rentPrice;
        if (_Asset.type == State.AssetType.estate)
                    {
                        newLiability.expenseType = State.Expense.Home;
                    }
                    else
                    {
                        newLiability.expenseType = State.Expense.ETC;
                    }

        PlayerManager.Instance.liabilityData.Liability[(int)State.LiabilityType.Rent].element.Add(newLiability);
    }

    public void RemoveRent(string _AssetType)
    {
        for (int i = 0; i < PlayerManager.Instance.liabilityData.Liability[(int)State.LiabilityType.Rent].element.Count; i++)
        {
            if (PlayerManager.Instance.liabilityData.Liability[(int)State.LiabilityType.Rent].element[i].name == _AssetType)
            {
                PlayerManager.Instance.liabilityData.Liability[(int)State.LiabilityType.Rent].element.RemoveAt(i);
                break;
            }
        }
    }

    public void AddCreditCard(string _CardType)
    {
        if (!haveCreditCard(_CardType))
        {
            for (int i = 0; i < allCreditCard.Length; i++)
            {
                int _Index = i;
                if (_CardType == allCreditCard[_Index].cardType)
                {
                    PlayerCreditCard newCard = new PlayerCreditCard();
                    newCard.cardID = _Index;
                    newCard.maxCredit = allCreditCard[_Index].maxCredit;
                    newCard.date = new List<PlayerCreditCardDate>();
                    for (int j = 0; j < CalManager.Instance.DAY_IN_MONTH; j++)
                    {
                        PlayerCreditCardDate newDate = new PlayerCreditCardDate();
                        newCard.date.Add(newDate);
                    }
                    for (int j = 0; j < newCard.date.Count; j++)
                    {
                        newCard.date[j].element = new List<PlayerCreditCardElement>();
                    }

                    PlayerManager.Instance.liabilityData.creditCard.Add(newCard);
                }
            }
        }
    }

    public void GenerateLotto()
    {
        if (CalManager.Instance.realDay == 2 || CalManager.Instance.Day() % CalManager.Instance.DAY_IN_WEEK == 0)
        {

            CheckLotto();
            RandomLottoWin();
        }
        
        if (CalManager.Instance.Day() % CalManager.Instance.DAY_IN_WEEK != 0)
        {
            RandomLottoSell();
        }
    }

    public void CheckLotto()
    {
        for (int i = 0; i < PlayerManager.Instance.assetData.lottoHistory[lottoData.drawTime].lottery.Count; i++)
        {
            if (PlayerManager.Instance.assetData.lottoHistory[lottoData.drawTime].lottery[i].lotto == lottoData.lottoWin)
            {
                float winAmount = lottoData.lottoWinPrice * PlayerManager.Instance.assetData.lottoHistory[lottoData.drawTime].lottery[i].amount;
                PlayerManager.Instance.assetData.lottoHistory[lottoData.drawTime].lottery[i].winAmount = winAmount;
                PlayerManager.Instance.ApplyMoney(winAmount);
            }
        }
        lottoData.drawTime ++;
        LottoHistory newLottoHis = new LottoHistory();
        newLottoHis.lottery = new List<Lotto>();
        PlayerManager.Instance.assetData.lottoHistory.Add(newLottoHis);
    }

    public void RandomLottoSell()
    {
        for (int i = 0; i < lottoData.lottoNumber.Length; i++)
        {
            lottoData.lottoNumber[i] = Random.Range(0, 999999);
        }
    }

    public void RandomLottoWin()
    {
        lottoData.lottoWin = Random.Range(0, 999999);
    }

    public void OnScamming()
    {
        PlayerManager.Instance.SubtractMoney(scamData.scamPriceSubmit, ()=>
        {
            scamData.scamDate = CalManager.Instance.realDay + CalManager.Instance.SCAM_LENGHT_DATE;
            UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OnClickClosePopup(State.PopupPage.Scamming);
        });
    }

    public void OnScamWin()
    {
        PlayerManager.Instance.ApplyMoney(scamData.scamPriceSubmit * scamData.scamRatio);

        scamData.scamDate = 0;
        scamData.scamPriceSubmit = scamData.scamPriceDefault;
    }

    public void OnScamLose()
    {
        scamData.scamDate = 0;
        scamData.scamPriceSubmit = scamData.scamPriceDefault;
    }
}

[System.Serializable]
public class LottoData
{
    public int[] lottoNumber;
    public int drawTime;
    public int lottoWin;
    public float lottoWinPrice;
    public float lottoPrice;
}

[System.Serializable]
public class ScamData
{
    public bool isFirstTime;
    public int scamRatio;
    public int scamDate;
    public float scamPriceSubmit;
    public float scamPriceDefault;
    public int scamMissing;
}