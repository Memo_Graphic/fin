using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ActionElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_TypeText;
    [SerializeField] private TextMeshProUGUI m_TimeText;
    [SerializeField] private TextMeshProUGUI m_CostText;
    [SerializeField] private TextMeshProUGUI m_cPriceText;
    [SerializeField] private TextMeshProUGUI m_HappyText;
    [SerializeField] private TextMeshProUGUI m_HealthText;
    [SerializeField] private TextMeshProUGUI m_CoolText;

    [Header("Image")]
    [SerializeField] private Image m_ActionImage;
    [SerializeField] private Image m_BGImage;

    private Button m_Button;

    private Activity action;
    private Asset asset;
    private float type;

    // Start is called before the first frame update
    void Start()
    {
        m_Button = gameObject.GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        // if (StateManager.Instance.currentState == State.GameState.Story_12_0 && action.activityType == "Fast02")
        // {
        //     UIManager.Instance.uIElement[(int)State.UIState.TutorialPage].GetComponent<UITutorialPage>().SetArrow(gameObject);
        // }
        // else if (StateManager.Instance.currentState == State.GameState.Story_13_0 && action.activityType == "Fast03")
        // {
        //     UIManager.Instance.uIElement[(int)State.UIState.TutorialPage].GetComponent<UITutorialPage>().SetArrow(gameObject);
        // }
    }

    public void SetAction(Activity _Action, int _Type, System.Action callback = null)
    {
        m_Button = gameObject.GetComponent<Button>();
        
        action = _Action;
        type = _Type;

        if (m_Button)
        {
            m_Button.onClick.RemoveAllListeners();
            m_Button.onClick.AddListener(delegate 
            {
                OnClickAction();
                if (callback != null)
                {
                    callback();
                }  
            });
        }

        UpdateAction();
    }

    public void SetActionSet(Activity _Action, int _Type, System.Action callback = null)
    {
        m_Button = gameObject.GetComponent<Button>();
        
        action = _Action;
        type = _Type;

        if (m_Button)
        {
            m_Button.onClick.RemoveAllListeners();
            m_Button.onClick.AddListener(delegate 
            {
                if (callback != null)
                {
                    callback();
                }

                if (GameManager.Instance.isTutorial && action.activityType == "Fast02")
                {
                    if (StateManager.Instance.currentState == State.GameState.Story_12_0)
                    {
                        StateManager.Instance.NextState();
                    }
                }
                else if (GameManager.Instance.isTutorial && action.activityType == "Fast03")
                {
                    if (StateManager.Instance.currentState == State.GameState.Story_13_0)
                    {
                        StateManager.Instance.NextState();
                    }
                }
            });
        }

        UpdateAction();
    }

    public void SetAsset(Asset _Asset, int _Type, System.Action callback = null)
    {
        m_Button = gameObject.GetComponent<Button>();
        
        asset = _Asset;
        type = _Type;

        if (m_Button)
        {
            m_Button.onClick.RemoveAllListeners();
            m_Button.onClick.AddListener(delegate 
            {
                OnClickAsset();
                if (callback != null)
                {
                    callback();
                }  
            });
        }

        UpdateAsset();
    }

    public void UpdateAction()
    {
        m_Button.interactable = PlayerManager.Instance.CanDoAction(action);

        if (m_NameText)
            m_NameText.text = action.activity;

        if (m_TimeText)  
            m_TimeText.text = action.timeCost.ToString() + " นาที";

        
        if (m_TypeText)
            m_TypeText.text = action.activityType;

        if (m_CostText)
            m_CostText.text = action.moneyCost.ToString("N2") + " ฿";

        if (m_HappyText)
            m_HappyText.text = action.happiness.ToString("+0;-#");

        if (m_HealthText)
            m_HealthText.text = action.health.ToString("+0;-#");

        if (m_CoolText)
            m_CoolText.text = action.cool.ToString("+0;-#");

        if (m_ActionImage && action.activityImage)
            m_ActionImage.sprite = action.activityImage;
        
        if (action.timeCost >= 99999f)
        {
            m_Button.interactable = true;
            m_TimeText.text = "ข้ามวัน";
        }
    }

    public void OnClickAction()
    {
        State.Expense _Type = (State.Expense)type;
        if (_Type != State.Expense.Work)
        {
            // PlayerManager.Instance.SubtractMoney(action.moneyCost, ()=>
            // {
            //     PlayerManager.Instance.SubtractTime(action.timeCost);
            //     PlayerManager.Instance.ApplyHealth(action.health);
            //     PlayerManager.Instance.ApplyHappiness(action.happiness);
            //     PlayerManager.Instance.ApplyCool(action.cool);

            //     PlayerBalanceElement balance = new PlayerBalanceElement();
            //     balance.balanceName = action.activity;
            //     balance.amount = action.moneyCost;
            //     PlayerManager.Instance.RecordBalance(balance, _Type);

            //     UIManager.Instance.UpdateUI(State.UIState.MainPage);
            // });

            UIManager.Instance.Show(State.UIState.PaymentPage);
            UIPaymentPage paymentPage = UIManager.Instance.uIElement[(int)State.UIState.PaymentPage].GetComponent<UIPaymentPage>();
            paymentPage.SetPayment(action, ()=>
                            {
                                PlayerManager.Instance.SubtractTime(action.timeCost);
                                PlayerManager.Instance.ApplyHealth(action.health);
                                PlayerManager.Instance.ApplyHappiness(action.happiness);
                                PlayerManager.Instance.ApplyCool(action.cool);

                                PlayerBalanceElement balance = new PlayerBalanceElement();
                                balance.balanceName = action.activity;
                                balance.amount = action.moneyCost;
                                PlayerManager.Instance.RecordBalance(balance, _Type);

                                UIManager.Instance.UpdateUI(State.UIState.MainPage);
                            }, ()=>
                            {
                                
                            });
        }
        else
        {
            // PlayerManager.Instance.ApplyHealth(PlayerManager.Instance.jobData.fullTimeJob.useHealth);
            // PlayerManager.Instance.ApplyHappiness(PlayerManager.Instance.jobData.fullTimeJob.useHappy);

            // PlayerManager.Instance.DoTask(1);

            // UIManager.Instance.UpdateUI(State.UIState.MainPage);
        }
    }

    public void UpdateAsset()
    {
        // m_Button.interactable = PlayerManager.Instance.CanDoAction(action);

        m_NameText.text = asset.assetName;
        m_TimeText.text = "";
        m_CostText.text = asset.price.ToString("N2") + " ฿";

        
        if (m_TypeText)
            m_TypeText.text = asset.assetType;

        if (m_CostText)
            m_CostText.text = asset.price.ToString("N2") + " ฿";

        if (asset.type == State.AssetType.stock
        || asset.type == State.AssetType.fund
        || asset.type == State.AssetType.gold)
        {
            if (m_cPriceText)
                m_cPriceText.text = asset.cPrice.ToString("+0.00;-0.00;") + "%";
            
            if (m_BGImage)
            {
                if (asset.cPrice < 0)
                {
                    m_BGImage.color = GameManager.Instance.red;
                }
                else
                {
                    m_BGImage.color = GameManager.Instance.green;
                }
            }

            if (m_HappyText)
                m_HappyText.text = "-";

            if (m_HealthText)
                m_HealthText.text = "-";

            if (m_CoolText)
            {
                m_CoolText.text = "-";
                m_CoolText.color = Color.black;
            }

            for (int i = 0; i < PlayerManager.Instance.assetData.Asset[(int)asset.type].element.Count; i++)
            {
                if (PlayerManager.Instance.assetData.Asset[(int)asset.type].element[i].assetType == asset.assetType)
                {
                    PlayerAssetElement playerAsset = PlayerManager.Instance.assetData.Asset[(int)asset.type].element[i];

                    if (m_HappyText)
                        m_HappyText.text = (playerAsset.amount * playerAsset.price).ToString("N2") + "฿";

                    if (m_HealthText)
                        m_HealthText.text = playerAsset.amount.ToString("N2");

                    if (m_CoolText)
                    {
                        m_CoolText.text = playerAsset.unrealized.ToString("+0.00;-0.00;") + " ฿";
                                        
                        // m_CoolText.text = playerAsset.unrealized.ToString("+0.00;-0.00;") + " ฿("
                        //                 + playerAsset.unrealizedP.ToString("N2") + "%)";

                        if (playerAsset.unrealized < 0)
                        {
                            m_CoolText.color = GameManager.Instance.red;
                        }
                        else
                        {
                            m_CoolText.color = GameManager.Instance.green;
                        }
                    }
                    break;
                }
            }
            
        }
        else
        {
            if (m_HappyText)
                m_HappyText.text = "";

            if (m_HealthText)
                m_HealthText.text = "";

            if (m_CoolText)
                m_CoolText.text = "";
        }

        if (m_ActionImage && asset.sprite)
            m_ActionImage.sprite = asset.sprite;
    }

    public void OnClickAsset()
    {
        if (asset.type == State.AssetType.fund || asset.type == State.AssetType.stock || asset.type == State.AssetType.gold)
        {
            UIManager.Instance.Show(State.UIState.InvestUnitPage);
            UIManager.Instance.uIElement[(int)State.UIState.InvestUnitPage].GetComponent<UIInvestUnitPage>().SetAsset(asset, (int)asset.type);
        }
        else
        {
            Debug.Log("Fix It Now !!!");
        }
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
