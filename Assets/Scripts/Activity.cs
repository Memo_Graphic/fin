using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ActivityElement
{
    public string typeName = "typeName";
    public Sprite typeIcon;
    public Activity[] activity;
}

[System.Serializable]
public class Activity
{
    [Header("Activity")]
    public string activityType = "Activity00";
    public string activity = "activityName";
    public Sprite activityImage;

    [Header("Cost")]
    public float moneyCost;
    public int timeCost;

    [Header("Effect")]
    public float happiness;
    public float health;
    public float cool;
    public float special;

    [Header("Requirements")]
    public string[] reqType;

    [Header("Get Asset")]
    public string[] assetType;
}

[System.Serializable]
public class ActivityPlanner
{
    [Header("Activity")]
    public int activityID = 0;
    public State.Expense type;
    public int time;
    public string activityType
    {
        get 
        {
            return ActionManager.Instance.activity[(int)type].activity[activityID].activityType;
        }
    }

    public string activity
    {
        get 
        {
            return ActionManager.Instance.activity[(int)type].activity[activityID].activity;
        }
    }
    public Sprite activityImage
    {
        get 
        {
            return ActionManager.Instance.activity[(int)type].activity[activityID].activityImage;
        }
    }

    // [Header("Cost")]
    public float moneyCost
    {
        get 
        {
            return ActionManager.Instance.activity[(int)type].activity[activityID].moneyCost;
        }
    }
    public int timeCost
    {
        get 
        {
            return ActionManager.Instance.activity[(int)type].activity[activityID].timeCost;
        }
    }

    // [Header("Effect")]
    public float happiness
    {
        get 
        {
            return ActionManager.Instance.activity[(int)type].activity[activityID].happiness;
        }
    }
    public float health
    {
        get 
        {
            return ActionManager.Instance.activity[(int)type].activity[activityID].health;
        }
    }
    public float cool
    {
        get 
        {
            return ActionManager.Instance.activity[(int)type].activity[activityID].cool;
        }
    }
    public float special
    {
        get 
        {
            return ActionManager.Instance.activity[(int)type].activity[activityID].special;
        }
    }
}
