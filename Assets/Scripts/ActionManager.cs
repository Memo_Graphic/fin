using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionManager : Singleton<ActionManager>
{
    public State.Expense allType;
    public int MaxActivityPlanner;
    public float dailyCost;
    public bool useCredit;
    public List<ActivityPlanner> activityPlanner;
    public ActivityElement[] activity;
    public HospitalData hospitalData;

    private PlayerManager m_playerManager;

    void Start()
    {
        m_playerManager = PlayerManager.Instance;
    }

    void Update()
    {
        
    }

    public Activity FindActivity(string _Activity, State.Expense _Type)
    {
        for (int i = 0; i < activity[(int)_Type].activity.Length; i++)
        {
            if (_Activity == activity[(int)_Type].activity[i].activityType)
            {
                int index = i;
                Activity newActivity = activity[(int)_Type].activity[index];
                return newActivity;
            }
        }
        Debug.LogWarning("Cannot find " + _Activity);

        return null;
    }

    public ActivityPlanner ConvertActivity(string _ActivityType, State.Expense _Type)
    {
        for (int i = 0; i < activity[(int)_Type].activity.Length; i++)
        {
            if (_ActivityType == activity[(int)_Type].activity[i].activityType)
            {
                int index = i;
                ActivityPlanner newActivity = new ActivityPlanner();
                newActivity.activityID = index;
                newActivity.type = _Type;
                newActivity.time = 1;

                return newActivity;
            }
        }
        Debug.LogWarning("Cannot find " + _ActivityType);
        
        return null;
    }

    public void DoAction(string _Activity, State.Expense _Type, int _Time = 1)
    {
        Activity action = FindActivity(_Activity, _Type);
        if (_Type == State.Expense.Work)
        {
            if (m_playerManager.jobData.fullTimeJob.useHealth * _Time <= m_playerManager.playerData.health &&
            m_playerManager.jobData.fullTimeJob.useHappy * _Time <= m_playerManager.playerData.happiness)
            {
                m_playerManager.ApplyHealth(m_playerManager.jobData.fullTimeJob.useHealth * _Time * -1 );
                m_playerManager.ApplyHappiness(m_playerManager.jobData.fullTimeJob.useHappy * _Time * -1 );

                m_playerManager.DoTask(_Time);
                // m_playerManager.DoTask(1 * _Time);

                UIManager.Instance.UpdateUI(State.UIState.MainPage);
            }
        }
        else
        {
            if (action.health * _Time * -1 <= m_playerManager.playerData.health &&
            action.happiness * _Time * -1 <= m_playerManager.playerData.happiness)
            {
                m_playerManager.SubtractMoney(action.moneyCost * _Time, ()=>
                {
                    m_playerManager.SubtractTime(action.timeCost * _Time);
                    m_playerManager.ApplyHealth(action.health * _Time);
                    m_playerManager.ApplyHappiness(action.happiness * _Time);
                    m_playerManager.ApplyCool(action.cool * _Time);

                    PlayerBalanceElement balance = new PlayerBalanceElement();
                    balance.balanceName = action.activity;
                    balance.amount = action.moneyCost * _Time;
                        
                    if (balance.amount > 0)
                    {
                        m_playerManager.RecordBalance(balance, _Type);
                    }

                    UIManager.Instance.UpdateUI(State.UIState.MainPage);
                }, ()=>
                {
                    UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.MoneyLimit);
                });
            }
        }
    }

    public void DoingAction(string _Activity, State.Expense _Type, int _Time = 1)
    {
        Activity action = FindActivity(_Activity, _Type);
        if (_Type == State.Expense.Work)
        {
            if (m_playerManager.jobData.fullTimeJob.useHealth * _Time <= m_playerManager.playerData.health &&
            m_playerManager.jobData.fullTimeJob.useHappy * _Time <= m_playerManager.playerData.happiness)
            {
                m_playerManager.ApplyHealth(m_playerManager.jobData.fullTimeJob.useHealth * _Time * -1 );
                m_playerManager.ApplyHappiness(m_playerManager.jobData.fullTimeJob.useHappy * _Time * -1 );

                m_playerManager.DoTask(_Time);
                // m_playerManager.DoTask(1 * _Time);

                UIManager.Instance.UpdateUI(State.UIState.MainPage);
            }
        }
        else
        {
            if (action.health * _Time * -1 <= m_playerManager.playerData.health &&
            action.happiness * _Time * -1 <= m_playerManager.playerData.happiness)
            {
                if (useCredit)
                {
                    string creditName = action.activity;
                    if (_Time > 1)
                    {
                        creditName = action.activity + " x" + _Time.ToString("N0");
                    }

                    m_playerManager.UseCredit(0, action.moneyCost * _Time, creditName, ()=>
                    {
                        m_playerManager.SubtractTime(action.timeCost * _Time);
                        m_playerManager.ApplyHealth(action.health * _Time);
                        m_playerManager.ApplyHappiness(action.happiness * _Time);
                        m_playerManager.ApplyCool(action.cool * _Time);

                        UIManager.Instance.UpdateUI(State.UIState.MainPage);
                    });
                }
                else
                {
                    m_playerManager.SubtractMoney(action.moneyCost * _Time, ()=>
                    {
                        m_playerManager.SubtractTime(action.timeCost * _Time);
                        m_playerManager.ApplyHealth(action.health * _Time);
                        m_playerManager.ApplyHappiness(action.happiness * _Time);
                        m_playerManager.ApplyCool(action.cool * _Time);

                        PlayerBalanceElement balance = new PlayerBalanceElement();
                        balance.balanceName = action.activity;
                        balance.amount = action.moneyCost * _Time;

                        if (balance.amount > 0)
                        {
                            m_playerManager.RecordBalance(balance, _Type);
                        }

                        UIManager.Instance.UpdateUI(State.UIState.MainPage);
                    });
                }
            }
        }
    }

    public void AddActivityPlanner()
    {
        if (activityPlanner[activityPlanner.Count - 1].time > 0)
        {

        }
        else
        {
            activityPlanner[activityPlanner.Count - 1].time = 1;
            
            ActivityPlanner newPlanner = new ActivityPlanner();
            newPlanner.activityID = 0;
            newPlanner.type = State.Expense.None;
            newPlanner.time = 0;
            activityPlanner.Add(newPlanner);
        }
    }
}

[System.Serializable]
public class HospitalData
{
    public State.Hospital currentHospital;
    public float[] hospitalDefaultPrice;
    public float hospitalPrice;
    public float hospitalInsurance;
    public float hospitalNetPrice
    {
        get
        {
            return hospitalPrice - hospitalInsurance;
        }
    }
    public int hospitalExitDate;
    public int hospitalMissing;
}
