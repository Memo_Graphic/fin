﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class LocationManager : Singleton<LocationManager>
{
    public int GRID_SIZE = 3;
    public int KM_PER_UNIT = 1;
    public LocationData locationData;

    [Header("Travel")]
    public TravelBy[] travelBy;
    public TravelBy currenTravel
    {
        get
        {
            return travelBy[locationData.currentTravelIndex];
        }
    }
    
    [Header("Game Location")]
    public State.Location allLocation;
    public List<Location> location;
    public int currentDistance
    {
        get
        {
            return Distance(locationData.currentHomeLo, locationData.currentJobLo);
        }
    }
    public int JobDistance(int Location)
    {
        return Distance(Location, locationData.currentJobLo);
    }
    public int Distance(int LocaA, int LocaB)
    {
        int horizontalA = LocaA % GRID_SIZE;
        int verticalA = (int)Mathf.Ceil(LocaA / GRID_SIZE);
        int horizontalB = LocaB % GRID_SIZE;
        int verticalB = (int)Mathf.Ceil(LocaB / GRID_SIZE);

        int hAbs = Mathf.Abs(horizontalA - horizontalB);
        int vAbs = Mathf.Abs(verticalA - verticalB);
        return (hAbs + vAbs)  * KM_PER_UNIT;
    }
    private Camera mainCamera;
    
    void Start()
    {
        
    }

    public int TimeToTravel(int _aLocation, int _bLocation)
    {
        locationData.oldLocation = _bLocation;
        locationData.distance = Distance(_aLocation, _bLocation);
        // Debug.Log(_aLocation + " go to " + _bLocation + ": " + distance + " (" + (distance * currenTravel.MIN_PER_KM) + ")");
        return locationData.distance * currenTravel.MIN_PER_KM;
    }

    public void SetHomeLocation(int location)
    {
        locationData.currentHomeLo = location;
    }

    public void SetJobLocation(int location)
    {
        locationData.currentJobLo = location;
    }

    public void GoTo(int _LocationIndex)
    {
        locationData.oldLocation = locationData.currentLocation;
        locationData.currentLocation = _LocationIndex;
        locationData.viewLocation = locationData.currentLocation;
        PlayerManager.Instance.SubtractTime(TimeToTravel(locationData.oldLocation, locationData.currentLocation));
        CalculateTravel();
    }

    public void NextTo(int _LocationIndex)
    {
        locationData.oldLocation = locationData.currentLocation;
        locationData.currentLocation = _LocationIndex;
        locationData.viewLocation = locationData.currentLocation;
        PlayerManager.Instance.SubtractTime(TimeToTravel(locationData.oldLocation, locationData.currentLocation));
        CalculateTravel();
    }
    
    public void MoveTo(int _LocationIndex)
    {
        locationData.oldLocation = locationData.currentLocation;
        locationData.currentLocation = _LocationIndex;
        locationData.viewLocation = locationData.currentLocation;
    }

    public void CalculateTravel()
    {
        PlayerManager.Instance.ApplyHappiness(travelBy[locationData.currentTravelIndex].happy);
        PlayerManager.Instance.ApplyHealth(travelBy[locationData.currentTravelIndex].health);
        if (ActionManager.Instance.useCredit)
        {
            PlayerManager.Instance.UseCredit(0, travelBy[locationData.currentTravelIndex].costPerUse, travelBy[locationData.currentTravelIndex].travelName);
        }
        else
        {
            PlayerManager.Instance.SubtractMoney(travelBy[locationData.currentTravelIndex].costPerUse);
        }
    }
}

[System.Serializable]
public class Location
{
    [Header("Location")]
    public string locationType = "Location00";
    public string locationName = "locationName";
    public State.Location type;
    public Sprite locationImage;
    public bool isLand;

    [Header("Activity")]
    public string[] activityType;
}

[System.Serializable]
public class TravelBy
{
    [Header("Info")]
    public string travelType = "Travel00";
    public string travelName = "TravelName";
    public int MIN_PER_KM;
    public float costPerUse;
    public float health;
    public float happy;
    public Sprite travelImage;
    public Sprite travelBigImage;
    public bool isPublic;

    [Header("Requirements")]
    public string carType;
}

[System.Serializable]
public class LocationData
{
    public int currentTravelIndex;
    public int currentHomeLo;
    public int currentJobLo;
    public int distance;
    public int oldLocation;
    public int currentLocation;
    public int viewLocation;
}