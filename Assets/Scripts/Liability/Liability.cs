﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct LiabilityData
{
    public State.LiabilityType allType;
    public LiabilityTypeElement[] Liability;
    public List<PlayerCreditCard> creditCard;
}

[System.Serializable]
public class LiabilityTypeElement
{
    public string typeName = "typeName";
    public List<PlayerLiabilityElement> element;
}

[System.Serializable]
public class PlayerLiabilityElement
{
    public string name = "liabilityName";
    public int deptID = 0;
    public State.LiabilityType type;
    public State.Expense expenseType;
    public bool notPaid; //ชำระแล้ว
    public float irPerMonth; //ดอกเบี้ยต่อเดือน
    public int term; //จำนวนงวด
    public int maxTerm; //จำนวนงวดทั้งหมด
    public double downPayment; //จำนวนเงินดาวน์
    public double loanAmount; //เงินต้นที่กู
    public double maxAmount; //เงินต้นที่กู
    public double installment; //ยอดชำระต่อเดือน
}

[System.Serializable]
public class PlayerCreditCard
{
    public int cardID;
    public int deptID;
    public string cardName
    {
        get
        {
            return BankManager.Instance.allCreditCard[cardID].cardName;
        }
    }
    public string cardType
    {
        get
        {
            return BankManager.Instance.allCreditCard[cardID].cardType;
        }
    }
    public float ir
    {
        get
        {
            return BankManager.Instance.allCreditCard[cardID].ir;
        }
    }
    public float maxCredit;
    public float usedCredit;
    public float enoughCredit
    {
        get
        {
            return maxCredit - usedCredit;
        }
    }
    public float calUsed
    {
        get
        {
            float caling = 0;
            for (int j = 0; j < date.Count; j ++)
            {
                for (int z = 0; z < date[j].element.Count; z ++)
                {
                    caling += (float)date[j].element[z].loanAmount;
                }
            }
            return caling;
        }
    }
    public float calInterest;
    public float interest;
    public float dept;
    public float deptUsed;
    public float deptInterest;
    public List<PlayerCreditCardDate> date;
}

[System.Serializable]
public class PlayerCreditCardDate
{
    public List<PlayerCreditCardElement> element;
}


[System.Serializable]
public class PlayerCreditCardElement
{
    public string paymentName = "creditName";
    public bool notPaid; // ค้างชำระ
    public double loanAmount; //เงินต้นที่กู
}

[System.Serializable]
public class CreditCardElement
{
    public string cardType = "Card00";
    public string cardName = "cardName";
    public float ir;
    public float maxCredit;
}