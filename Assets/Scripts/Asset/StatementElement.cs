using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StatementElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_AmountText;

    [Header("Image")]
    [SerializeField] private Image m_IconImg;

    private string statementName;
    private int index;
    private double amount;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowElement(double _Amount, int _Index)
    {
        index = _Index;
        amount = _Amount;
        statementName = ActionManager.Instance.activity[index].typeName;

        m_NameText.text = statementName.ToString();
        m_AmountText.text = amount.ToString("N2") + " ฿";
        m_IconImg.sprite = ActionManager.Instance.activity[index].typeIcon;
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
