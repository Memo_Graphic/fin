using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StarUIElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_AmountText;

    [Header("Image")]
    [SerializeField] private Image m_CompleteImg;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowElement(string _Name, int _Amount, bool isCompleted)
    {
        m_NameText.text = _Name;
        m_AmountText.text = _Amount.ToString("N0");
        m_CompleteImg.gameObject.SetActive(isCompleted);
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
