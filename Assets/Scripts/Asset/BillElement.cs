using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BillElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_TitleText;
    [SerializeField] private TextMeshProUGUI m_AmountText;
    [SerializeField] private TextMeshProUGUI m_DeptCreditText;
    [SerializeField] private TextMeshProUGUI m_InterestText;
    [SerializeField] private TextMeshProUGUI m_PenaltyText;

    [SerializeField] private TextMeshProUGUI m_DeadlineText;
    [SerializeField] private TextMeshProUGUI m_SumText;
    [SerializeField] private TextMeshProUGUI m_Sum10PText;

    [Header("GameObjects")]
    [SerializeField] private GameObject m_CreditObj;
    [SerializeField] private GameObject m_InterestObj;
    [SerializeField] private GameObject m_PenaltyObj;
    [SerializeField] private GameObject m_10PObj;

    private int index;
    private float sum;
    private PlayerBillElement bill;
    private PlayerManager m_PlayerManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowElement(PlayerBillElement _Bill, int _Index)
    {
        m_PlayerManager = PlayerManager.Instance;

        bill = _Bill;
        index = _Index;

        m_NameText.text = bill.billName.ToString();
        m_AmountText.text = bill.amount.ToString("N2") + " ฿";
        m_DeptCreditText.text = bill.creditDept.ToString("N2") + " ฿";
        m_InterestText.text = bill.interest.ToString("N2") + " ฿";
        m_PenaltyText.text = bill.penalty.ToString("N2") + " ฿";
        m_DeadlineText.text = "";
        if (bill.deadline > 0)
        {
            if (bill.deadline == CalManager.Instance.realDay)
            {
                m_DeadlineText.text = "ชำระภายในวันนี้";
            }
            else if (bill.deadline > CalManager.Instance.realDay)
            {
                m_DeadlineText.text = "ชำระภายใน " + (bill.deadline - CalManager.Instance.realDay).ToString("N0")  +  " วัน";
            }
            else if (bill.deadline < CalManager.Instance.realDay)
            {
                m_DeadlineText.text = "เกินกำหนดชำระ " + Mathf.Abs(bill.deadline - CalManager.Instance.realDay).ToString("N0")  +  " วัน";
            }
            
            if (bill.overdue > 0)
            {
                m_DeadlineText.text = "เกินกำหนดชำระ " + Mathf.Abs(bill.overdue).ToString("N0")  +  " รอบบิล";
            }
        }
        sum = bill.amount + bill.interest + bill.penalty + bill.creditDept;
        m_SumText.text = sum.ToString("N2") + " ฿";
        m_Sum10PText.text = (sum * 0.1f).ToString("N2") + " ฿";

        m_CreditObj.SetActive(bill.creditDept > 0);
        m_InterestObj.SetActive(bill.interest > 0);
        m_PenaltyObj.SetActive(bill.penalty > 0);
        m_10PObj.SetActive(bill.canMn);
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }

    public void PayBill()
    {
        m_PlayerManager.SubtractMoney(sum, () => {


            PlayerBalanceElement balance = new PlayerBalanceElement();
            balance.balanceName = bill.billName;
            balance.amount = (float)sum;
            m_PlayerManager.RecordBalance(balance, bill.expenseType);

            // if (m_PlayerManager.billData.bill[index].CallbackYes != null)
            // {
            //     m_PlayerManager.billData.bill[index].CallbackYes();
            // }
            m_PlayerManager.billData.bill[index].CallbackYes();
        });

        UIManager.Instance.UpdateUI(State.UIState.MainPage);
        UIManager.Instance.UpdateUI(State.UIState.BillPage);
    }

    public void PayBill(float _Percentage)
    {
        float _Amount = (sum) * (_Percentage / 100f);

        m_PlayerManager.SubtractMoney(_Amount, () => {

            PlayerBalanceElement balance = new PlayerBalanceElement();
            balance.balanceName = bill.billName;
            balance.amount = (float)_Amount;
            m_PlayerManager.RecordBalance(balance, bill.expenseType);
            
            if (_Percentage < 100)
            {
                m_PlayerManager.billData.bill[index].amount -= (bill.amount * _Percentage / 100);
                m_PlayerManager.billData.bill[index].creditDept -= (bill.creditDept * _Percentage / 100);
                
                // if (m_PlayerManager.billData.bill[index].CallbackNo != null)
                // {
                //     m_PlayerManager.billData.bill[index].CallbackNo();
                // }
                m_PlayerManager.billData.bill[index].CallbackNo();
            }
            else
            {
                // if (m_PlayerManager.billData.bill[index].CallbackYes != null)
                // {
                //     m_PlayerManager.billData.bill[index].CallbackYes();
                // }
                m_PlayerManager.billData.bill[index].CallbackYes();
            }
        });

        UIManager.Instance.UpdateUI(State.UIState.MainPage);
        UIManager.Instance.UpdateUI(State.UIState.BillPage);
    }

    // void CallbackYes()
    // {
    //     if (m_PlayerManager.billData.bill[index].billType == State.BillType.FixedCost)
    //     {
    //         if (m_PlayerManager.billData.bill[index].deptID > 0)
    //         {
    //             int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element.FindIndex(item => item.deptID == m_PlayerManager.billData.bill[index].deptID);
    //             BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Shot);
    //         }

    //         Debug.Log("Paid " + m_PlayerManager.billData.bill[index].billName);
    //         m_PlayerManager.billData.bill.Remove(m_PlayerManager.billData.bill[index]);
    //     }
    //     else if (m_PlayerManager.billData.bill[index].billType == State.BillType.RentCost)
    //     {
    //         if (m_PlayerManager.billData.bill[index].penalty == 0)
    //         {
    //             m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Rent].element[index].term++;
    //         }

    //         if (m_PlayerManager.billData.bill[index].deptID > 0)
    //         {
    //             int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element.FindIndex(item => item.deptID == m_PlayerManager.billData.bill[index].deptID);
    //             BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Shot);
    //         }

    //         Debug.Log("Paid Rent");
    //         m_PlayerManager.billData.bill.Remove(m_PlayerManager.billData.bill[index]);
    //     }
    // }

    // void CallbackNo()
    // {
    //     if (m_PlayerManager.billData.bill[index].billType == State.BillType.FixedCost)
    //     {
    //         Debug.LogWarning("Not Paid Bill fixedCost");
    //         m_PlayerManager.billData.bill[index].deadline += CalManager.Instance.DAY_IN_MONTH;
    //         m_PlayerManager.billData.bill[index].penalty += 100f;

    //         PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
    //         newLiability.name = m_PlayerManager.billData.bill[index].billName;
    //         newLiability.type = State.LiabilityType.Shot;
    //         newLiability.expenseType = m_PlayerManager.billData.bill[index].expenseType;
    //         newLiability.loanAmount = m_PlayerManager.billData.bill[index].amount;
            
    //         BankManager.Instance.currentDeptID ++;
    //         newLiability.deptID = BankManager.Instance.currentDeptID;
    //         m_PlayerManager.billData.bill[index].deptID = BankManager.Instance.currentDeptID;
    //         BankManager.Instance.AddLiability(newLiability);
    //     }
    //     else if (m_PlayerManager.billData.bill[index].billType == State.BillType.RentCost)
    //     {
    //         if (m_PlayerManager.billData.bill[index].penalty == 0)
    //         {
    //             m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Rent].element[index].term++;
    //         }

    //         Debug.LogWarning("Not Paid Rent");

    //         m_PlayerManager.billData.bill[index].deadline += CalManager.Instance.DAY_IN_MONTH;
    //         m_PlayerManager.billData.bill[index].penalty += 100f;

    //         PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
    //         newLiability.name = m_PlayerManager.billData.bill[index].billName;
    //         newLiability.type = State.LiabilityType.Shot;
    //         newLiability.expenseType = m_PlayerManager.billData.bill[index].expenseType;
    //         newLiability.loanAmount = m_PlayerManager.billData.bill[index].amount;
            
    //         BankManager.Instance.currentDeptID ++;
    //         newLiability.deptID = BankManager.Instance.currentDeptID;
    //         m_PlayerManager.billData.bill[index].deptID = BankManager.Instance.currentDeptID;
    //         BankManager.Instance.AddLiability(newLiability);
    //     }
    // }
}
