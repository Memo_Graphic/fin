﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public struct AssetData
{
    public AssetTypeElement[] Asset;
    public InsuranceTypeElement[] insurances;
    public List<LottoHistory> lottoHistory;
}

[System.Serializable]
public class AssetTypeElement
{
    public string typeName = "typeName";
    public List<PlayerAssetElement> element;
}

[System.Serializable]
public class PlayerAssetElement
{
    public int assetID = 0;
    public int deptID = 0;
    public State.AssetType type;
    public string assetName
    {
        get 
        {
            return AssetManager.Instance.Asset[(int)type].element[assetID].assetName;
        }
    }
    public string assetType
    {
        get 
        {
            return AssetManager.Instance.Asset[(int)type].element[assetID].assetType;
        }
    }
    public int location
    {
        get 
        {
            return AssetManager.Instance.Asset[(int)type].element[assetID].location;
        }
    }
    public Sprite sprite
    {
        get 
        {
            return AssetManager.Instance.Asset[(int)type].element[assetID].sprite;
        }
    }
    public bool isRent; //เช่าอยู่
    public bool isUsing; //อยู่อาศัย
    public int buyDate;
    public float amount; // จำนวน
    public float cost; // ราคาที่ซื้อ
    public float price
    {
        // ราคา
        get 
        {
            return AssetManager.Instance.Asset[(int)type].element[assetID].price;
        }
    }
    public float pPrice
    {
        // ราคาเก่า
        get 
        {
            return AssetManager.Instance.Asset[(int)type].element[assetID].pPrice;
        }
    }
    public float cPrice
    {
        // ราคาเก่า
        get 
        {
            return AssetManager.Instance.Asset[(int)type].element[assetID].cPrice;
        }
    }
    public float unrealized 
    {
        // กำไร
        get 
        {
            float value = (price * amount) - cost;
            return Mathf.RoundToInt(value * 100f) / 100f;
        }
    }
    public float unrealizedP
    {
        // กำไร %
        get 
        {
            float value = (((price * amount) - cost) / cost) * 100f;
            return Mathf.RoundToInt(value * 100f) / 100f;
        }
    }
    public float rentMonth; // จำนวนเดือนที่เช่า
    public float rentPrice
    {
        // ค่าเช่า
        get 
        {
            return AssetManager.Instance.Asset[(int)type].element[assetID].rentPrice;
        }
    }
    public float fixedCost
    {
        // ค่าใช้จ่ายรายเดือน
        get 
        {
            return AssetManager.Instance.Asset[(int)type].element[assetID].fixedCost;
        }
    }
    public float cool
    {
        // ค่าใช้จ่ายรายเดือน
        get 
        {
            return AssetManager.Instance.Asset[(int)type].element[assetID].cool;
        }
    }
}

[System.Serializable]
public class AssetElement
{
    public string typeName = "typeName";
    public Asset[] element;
}

[System.Serializable]
public class Asset
{
    [Header("Asset")]
    public string assetType = "Asset00";
    public string assetName = "assetName";
    public State.AssetType type;
    public string locationType;
    public int location
    {
        get
        {
            for (int i = 0; i < LocationManager.Instance.location.Count; i++)
            {
                int index = i;
                if (locationType == LocationManager.Instance.location[index].locationType)
                {
                    return index;
                }
            }
            Debug.LogWarning("Asset: " + assetName + " location Type Error");
            return 0;
        }
    }
    public Sprite sprite;
    public float price; // ราคา
    public float pPrice; // ราคาเก่า
    public float cPrice
    {
        // ราคาเก่า
        get 
        {
            return (price - pPrice) / pPrice * 100f;
        }
    }
    public float rentPrice; // ค่าเช่า
    public float fixedCost; // ค่าใช้จ่าย
    public float growthRate; // อัตราการเติบโต
    public float cool; // ดาว
}

[System.Serializable]
public class LottoHistory
{
    public List<Lotto> lottery;
}

[System.Serializable]
public class Lotto
{
    public int lotto;
    public int amount;
    public float winAmount;
}

[System.Serializable]
public class InsuranceElement
{
    public string typeName = "typeName";
    public Insurance[] element;
}

[System.Serializable]
public class Insurance
{
    public string insuranceType;
    public string insuranceName;
    public State.Insurance type;
    public float protectionLimit;
    public float price;
    public float totalPrice;
}

[System.Serializable]
public class InsuranceTypeElement
{
    public string typeName = "typeName";
    public PlayerInsuranceElement element;
}

[System.Serializable]
public class PlayerInsuranceElement
{
    public int insuranceID = 0;
    public State.Insurance type;
    public string insuranceName
    {
        get 
        {
            return AssetManager.Instance.insurances[(int)type].element[insuranceID].insuranceName;
        }
    }
    public string insuranceType
    {
        get 
        {
            return AssetManager.Instance.insurances[(int)type].element[insuranceID].insuranceType;
        }
    }
    public int expiredDate;
    public float protectionLimit;
    public float protectionMaxLimit;
    public float price
    {
        // ราคา
        get 
        {
            return AssetManager.Instance.insurances[(int)type].element[insuranceID].price;
        }
    }
    public float totalPrice;
}