using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CreditFolder : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_Title;

    [Header("Prefabs")]
    [SerializeField] private CreditElement m_AddingPrefab;
    [SerializeField] private Transform m_BlockParent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowElement(int Date)
    {
        m_Title.text = CalManager.Instance.Day(Date + 1).ToString("00") + "/" + CalManager.Instance.DateTime("MM/YYYY");

        for (int i = 0; i < PlayerManager.Instance.liabilityData.creditCard[0].date[Date].element.Count; i++)
        {
            int index = i;
            CreditElement creditElement = Instantiate(m_AddingPrefab, m_BlockParent);
            creditElement.ShowElement(PlayerManager.Instance.liabilityData.creditCard[0].date[Date].element[i].paymentName,
                                    (PlayerManager.Instance.liabilityData.creditCard[0].date[Date].element[i].loanAmount).ToString("N0") + " ฿");
        }
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
