using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CreditElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_AmountText;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowElement(string _Name, string _Amount)
    {
        m_NameText.text = _Name.ToString();
        m_AmountText.text = _Amount.ToString();
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
