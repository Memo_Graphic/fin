using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AssetLiabilityFolder : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_Title;
    [SerializeField] private TextMeshProUGUI m_Value;

    [Header("Prefabs")]
    [SerializeField] private AssetLiabilityElement m_BlockPrefab;
    [SerializeField] private AssetLiabilityElement m_AddingPrefab;
    [SerializeField] private Transform m_BlockParent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Account
    public void ShowElement(string _Name, List<PlayerAccountElement> _Element)
    {
        m_Title.text = _Name;
        float value = 0;
        for (int i = 0; i < _Element.Count; i ++)
        {
            int _Index = i;
            value += (float)_Element[_Index].balance;
            CreateAssetElement(_Element[_Index].accountName,
                            (_Element[_Index].balance).ToString("N2") + " ฿",
                            m_BlockParent, ()=>
                            {
                                // if (_Index > 0)
                                // {
                                //     UIManager.Instance.Show(State.UIState.AccountPage);
                                //     UIManager.Instance.uIElement[(int)State.UIState.AccountPage].GetComponent<UIAccountPage>().UpdateAccount(_Index);
                                // }
                            });
        }
        m_Value.text = value.ToString("N2") + " ฿";
    }

    // PlayerAsset
    public void ShowElement(string _Name, List<PlayerAssetElement> _Element)
    {
        m_Title.text = _Name;
        float value = 0;
        foreach(var _Asset in _Element)
        {
            if (!_Asset.isRent)
            {
                value += (float)_Asset.price * _Asset.amount;
                CreateAssetElement(_Asset.assetName,
                                (_Asset.price * _Asset.amount).ToString("N2") + " ฿", 
                                m_BlockParent, ()=>
                                {
                                    // UIManager.Instance.Show(State.UIState.InvestUnitPage);
                                    // UIManager.Instance.uIElement[(int)State.UIState.InvestUnitPage].GetComponent<UIInvestUnitPage>().SetAsset(_Asset, (int)_Asset.type);
                                });
            }
        }
        m_Value.text = value.ToString("N2") + " ฿";
    }

    // Lotto
    public void ShowElement(string _Name, List<Lotto> _Element)
    {
        m_Title.text = _Name;
        float value = 0;
        for (int i = 0; i < _Element.Count; i ++)
        {
            int _Index = i;
            CreateAssetElement(_Element[_Index].lotto.ToString("0 0 0 0 0 0"),
                            _Element[_Index].amount.ToString("N0") + " ใบ",
                            m_BlockParent, ()=>
                            {
                                
                            });
        }
        m_Value.text = value.ToString("N2") + " ฿";
    }

    // PlayerLiability
    public void ShowElement(string _Name, List<PlayerLiabilityElement> _Element)
    {
        m_Title.text = _Name;
        float value = 0;
        
        for (int i = 0; i < _Element.Count; i ++)
        {
            int _Index = i;
            value += (float)_Element[_Index].loanAmount;
            CreateAssetElement(_Element[_Index].name,
                            _Element[_Index].loanAmount.ToString("N2") + " ฿",
                            m_BlockParent, ()=>
                            {
                                
                            });
        }
        m_Value.text = value.ToString("N2") + " ฿";
    }

    private void CreateAssetElement(string _Name, string _Value, Transform _Parent, System.Action Callback = null)
    {
        AssetLiabilityElement newElement;
        newElement = Instantiate(m_BlockPrefab, _Parent);
        newElement.ShowElement(_Name, _Value, ()=>
                            {
                                if (Callback != null)
                                {
                                    Callback();
                                }
                            });
    }

    public void CreateAddingElement(System.Action Callback = null)
    {
        AssetLiabilityElement newElement = Instantiate(m_AddingPrefab, m_BlockParent);
        newElement.ShowElement("+", "", ()=>
                            {
                                if (Callback != null)
                                {
                                    Callback();
                                }
                            });
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
