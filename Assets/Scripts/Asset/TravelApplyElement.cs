using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TravelApplyElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_SpeedText;
    [SerializeField] private TextMeshProUGUI m_CostText;
    [SerializeField] private TextMeshProUGUI m_HealthText;
    [SerializeField] private TextMeshProUGUI m_HappyText;

    [Header("TMP - Not Have")]
    [SerializeField] private TextMeshProUGUI m_NotHaveNameText;
    [SerializeField] private TextMeshProUGUI m_NotHaveCostText;

    [Header("GameObjects")]
    [SerializeField] private GameObject m_HaveThisObj;
    [SerializeField] private GameObject m_SellBtn;

    [Header("Image")]
    [SerializeField] private Image m_TravelImage;
    [SerializeField] private Image m_NotHaveTravelImage;

    [Header("Button")]
    [SerializeField] private Button m_ComfirmBtn;


    private TravelBy m_Travel;
    private Asset newCar;
    private int index = -1;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // if (StateManager.Instance.currentState == State.GameState.Story_11_0
        // && LocationManager.Instance.locationData.currentTravelIndex > 0
        // && index == 0)
        // {
        //     UIManager.Instance.uIElement[(int)State.UIState.TutorialPage].GetComponent<UITutorialPage>().SetArrow(gameObject);
        // }
    }

    public void SetTravel(TravelBy _Travel, int _Index)
    {
        m_Travel = _Travel;
        index = _Index;

        UpdateUI();
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }

    private void UpdateUI()
    {
        m_SpeedText.text = m_Travel.MIN_PER_KM.ToString("N0") + " นาที/กม.";
        m_CostText.text = m_Travel.costPerUse.ToString("N0") + " ฿";
        m_HealthText.text = m_Travel.health.ToString("N0");
        m_HappyText.text = m_Travel.happy.ToString("N0");

        if (m_Travel.carType != "")
        {
            newCar = AssetManager.Instance.FindAsset(m_Travel.carType, State.AssetType.car);
            m_NameText.text = newCar.assetName;
            m_HaveThisObj.SetActive(PlayerManager.Instance.haveThisAsset(m_Travel.carType, State.AssetType.car));
            m_SellBtn.SetActive(PlayerManager.Instance.haveThisAsset(m_Travel.carType, State.AssetType.car));
            m_ComfirmBtn.interactable = PlayerManager.Instance.haveThisAsset(m_Travel.carType, State.AssetType.car);

            m_NotHaveNameText.text = newCar.assetName;
            m_NotHaveCostText.text = newCar.price.ToString("N0") + " ฿";
            
            m_NotHaveTravelImage.sprite = m_Travel.travelImage;
        }
        else
        {
            m_NameText.text = m_Travel.travelName;
            m_HaveThisObj.SetActive(true);
            m_SellBtn.SetActive(false);
            m_ComfirmBtn.interactable = true;
        }

        m_TravelImage.sprite = m_Travel.travelImage;
    }

    public void OnClickBuy()
    {
        UIManager.Instance.uIElement[(int)State.UIState.TimePage].GetComponent<UITimePage>().UpdateNotOwnUI(m_Travel, newCar);
    }

    public void OnClickSellBtn()
    {
        // ขาย
        AssetManager.Instance.SellAsset(newCar.assetType, 
                                    State.AssetType.car, 
                                    PlayerManager.Instance.accountData.account[0].accountID);

        UIManager.Instance.uIElement[(int)State.UIState.TimePage].GetComponent<UITimePage>().UpdateUI();
    }

    public void OnClickComfirmBtn()
    {
        if (index >= 0)
        {
            LocationManager.Instance.locationData.currentTravelIndex = index;
            UIManager.Instance.UpdateUI(State.UIState.TimePage);
            
            if (StateManager.Instance.currentState == State.GameState.Story_11_0
            && LocationManager.Instance.locationData.currentTravelIndex == 0 )
            {
                StateManager.Instance.SwitchState(State.GameState.Story_11_1);
            }
        }
    }
}
