using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TaxDeductionUIElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_AmountText;

    [Header("Slider")]
    [SerializeField] private Slider m_SliderText;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowElement(int _Index)
    {
        m_NameText.text = PlayerManager.Instance.taxData.deduction[_Index].deductionName;
        m_AmountText.text = PlayerManager.Instance.taxData.deduction[_Index].deductionCal.ToString("N0") + " ฿";

        // m_SliderText.minValue = PlayerManager.Instance.taxData.tax[_Index - 1].taxLength;
        // m_SliderText.maxValue = PlayerManager.Instance.taxData.tax[_Index].taxLength;
        // m_SliderText.value = PlayerManager.Instance.taxData.tax[_Index - 1].taxLength + PlayerManager.Instance.taxData.tax[_Index].taxFill;
    }

    public void ShowIncomeElement(int _Index)
    {
        float income = PlayerManager.Instance.taxData.income[_Index].incomeSum
         + PlayerManager.Instance.taxData.income[_Index].incomeHidden;

        m_NameText.text = PlayerManager.Instance.taxData.income[_Index].incomeName;
        m_AmountText.text = income.ToString("N0") + " ฿";

        // m_SliderText.minValue = PlayerManager.Instance.taxData.tax[_Index - 1].taxLength;
        // m_SliderText.maxValue = PlayerManager.Instance.taxData.tax[_Index].taxLength;
        // m_SliderText.value = PlayerManager.Instance.taxData.tax[_Index - 1].taxLength + PlayerManager.Instance.taxData.tax[_Index].taxFill;
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
