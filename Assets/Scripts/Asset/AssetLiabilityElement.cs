using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AssetLiabilityElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_AmountText;

    public System.Action callback;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowElement(string _Name, string _Amount, System.Action _Callback = null)
    {
        m_NameText.text = _Name.ToString();
        if (m_AmountText)
        {
            m_AmountText.text = _Amount;
        }
        callback = _Callback;
    }

    public void OnClickAdd()
    {
        if(callback != null)
        {
            callback();
        }
        else
        {
            Debug.Log("Clicked");
        }
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
