using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetManager : Singleton<AssetManager>
{
    public State.AssetType allType;
    public AssetElement[] Asset;
    public InsuranceElement[] insurances;

    private PlayerManager m_PlayerManager;
    
    
    // Start is called before the first frame update
    void Start()
    {
        m_PlayerManager = PlayerManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public PlayerAssetElement ConvertAsset(string _AssetType, State.AssetType _Type)
    {
        for (int i = 0; i < Asset[(int)_Type].element.Length; i++)
        {
            if (_AssetType == Asset[(int)_Type].element[i].assetType)
            {
                int index = i;
                PlayerAssetElement newAsset = new PlayerAssetElement();
                newAsset.assetID = index;
                newAsset.type = Asset[(int)_Type].element[index].type;
                newAsset.cost = Asset[(int)_Type].element[index].price;
                newAsset.amount = 1;

                return newAsset;
            }
        }
        Debug.LogWarning("Cannot find " + _AssetType);
        
        return null;
    }

    public Asset FindAsset(string _AssetType, State.AssetType _Type)
    {
        for (int i = 0; i < Asset[(int)_Type].element.Length; i++)
        {
            if (_AssetType == Asset[(int)_Type].element[i].assetType)
            {
                int index = i;
                Asset newAsset = Asset[(int)_Type].element[index];
                return newAsset;
            }
        }
        Debug.LogWarning("Cannot find " + _AssetType);

        return null;
    }

    public void BuyAsset(Asset _Asset, int _accountID = 0, float _Amount = 1)
    {
        m_PlayerManager.SubtractMoney(_accountID, _Asset.price * _Amount,
                                            // buy
                                            () => 
                                            {
                                                AddAsset(_Asset.assetType, _Asset.type, _Amount);
                                                UIManager.Instance.UpdateCurrentUI();

                                                if (_Asset.type == State.AssetType.fund)
                                                {
                                                    if (_Asset.assetType.Contains("SSF"))
                                                    {
                                                        m_PlayerManager.taxData.deduction[(int)State.Deduction.SSFfund].deductionRec += (_Asset.price * _Amount);
                                                    }
                                                    else if (_Asset.assetType.Contains("RMF"))
                                                    {
                                                        m_PlayerManager.taxData.deduction[(int)State.Deduction.RMFfund].deductionRec += (_Asset.price * _Amount);
                                                    }

                                                    m_PlayerManager.TotalInvest(_Asset.price * _Amount);
                                                    m_PlayerManager.CheckMission(State.Mission.Invest);
                                                }
                                            },
                                            // loan
                                            () =>
                                            {
                                                Debug.LogWarning("No Down Payment");
                                            });
    }

    public void BuyEstate(Asset _Asset, float _DownPercentage, float _Year, int _accountID = 0)
    {
        float downPrice = _Asset.price * _DownPercentage;
        m_PlayerManager.SubtractMoney(_accountID, downPrice,
                                    () =>
                                    {

                                        if (_DownPercentage < 100)
                                        {
                                            PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
                                            newLiability.name = _Asset.assetName;
                                            newLiability.type = State.LiabilityType.Loan;
                                            newLiability.expenseType = State.Expense.Home;
                                            newLiability.notPaid = false;
                                            newLiability.irPerMonth = BankManager.Instance.currentIR / 100f / CalManager.Instance.MONTH_IN_YEAR;
                                            newLiability.maxTerm = (int)_Year * CalManager.Instance.MONTH_IN_YEAR;
                                            newLiability.term = newLiability.maxTerm;
                                            newLiability.downPayment = downPrice;
                                            newLiability.loanAmount = _Asset.price - newLiability.downPayment;
                                            newLiability.maxAmount = newLiability.loanAmount;
                                            newLiability.installment = newLiability.loanAmount / ((1f - (1f / Mathf.Pow(1 + (newLiability.irPerMonth), newLiability.maxTerm))) / (newLiability.irPerMonth));
                                            
                                            BankManager.Instance.currentDeptID ++;
                                            newLiability.deptID = BankManager.Instance.currentDeptID;
                                            BankManager.Instance.AddLiability(newLiability);

                                            LoanAsset(_Asset.assetType, _Asset.type, BankManager.Instance.currentDeptID, 1);
                                        }
                                        else
                                        {
                                            AddAsset(_Asset.assetType, _Asset.type, 1);
                                        }


                                        PlayerBalanceElement balance = new PlayerBalanceElement();
                                        balance.balanceName = "ซื้อ " + _Asset.assetName;
                                        balance.amount = downPrice;
                                        m_PlayerManager.RecordBalance(balance, State.Expense.Home);

                                        UIManager.Instance.UpdateCurrentUI();
                                        Debug.Log("Installment " + _Asset.assetName);
                                    },
                                    () =>
                                    {
                                        Debug.LogWarning("Can't Installment");
                                    });
    }

    public void BuyCar(Asset _Asset, float _DownPercentage, float _Year, int _accountID = 0)
    {
        float downPrice = _Asset.price * _DownPercentage;
        m_PlayerManager.SubtractMoney(_accountID, downPrice,
                                    () =>
                                    {
                                        if (_DownPercentage < 100)
                                        {
                                            PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
                                            newLiability.name = _Asset.assetName;
                                            newLiability.type = State.LiabilityType.Loan;
                                            newLiability.expenseType = State.Expense.Car;
                                            newLiability.notPaid = false;
                                            // newLiability.irPerMonth = BankManager.Instance.currentIR / 100f / CalManager.Instance.MONTH_IN_YEAR;
                                            newLiability.irPerMonth = BankManager.Instance.currentIR / CalManager.Instance.MONTH_IN_YEAR;
                                            newLiability.maxTerm = (int)_Year * CalManager.Instance.MONTH_IN_YEAR;
                                            newLiability.term = newLiability.maxTerm;
                                            newLiability.downPayment = downPrice;
                                            newLiability.loanAmount = _Asset.price - newLiability.downPayment;
                                            newLiability.maxAmount = newLiability.loanAmount;
                                            newLiability.installment = (newLiability.loanAmount + (newLiability.loanAmount * (BankManager.Instance.currentIR / 100f * (int)_Year))) / (newLiability.maxTerm);
                                            // newLiability.installment = newLiability.loanAmount / ((1f - (1f / Mathf.Pow(1 + (newLiability.irPerMonth), newLiability.maxTerm))) / (newLiability.irPerMonth));

                                            BankManager.Instance.currentDeptID ++;
                                            newLiability.deptID = BankManager.Instance.currentDeptID;
                                            BankManager.Instance.AddLiability(newLiability);

                                            LoanAsset(_Asset.assetType, _Asset.type, BankManager.Instance.currentDeptID, 1);
                                        }
                                        else
                                        {
                                            AddAsset(_Asset.assetType, _Asset.type, 1);
                                        }

                                        PlayerBalanceElement balance = new PlayerBalanceElement();
                                        balance.balanceName = "ซื้อ " + _Asset.assetName;
                                        balance.amount = downPrice;
                                        m_PlayerManager.RecordBalance(balance, State.Expense.Car);

                                        UIManager.Instance.UpdateCurrentUI();
                                        Debug.Log("Installment " + _Asset.assetName);
                                    },
                                    () =>
                                    {
                                        Debug.LogWarning("Can't Installment");
                                    });
    }

    public void SellAsset(string _AssetType, State.AssetType _Type, int _accountID, float _Amount = 1)
    {
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)_Type].element.Count; i++)
        {
            if (m_PlayerManager.assetData.Asset[(int)_Type].element[i].assetType == _AssetType)
            {
                PlayerAssetElement _Asset = m_PlayerManager.assetData.Asset[(int)_Type].element[i];
                if (m_PlayerManager.assetData.Asset[(int)_Type].element[i].amount >= _Amount)
                {
                    if (_Asset.deptID <= 0)
                    {
                        m_PlayerManager.ApplyMoney(_accountID, _Asset.price * _Amount,
                                                () =>
                                                {
                                                    RemoveAsset(_Asset.assetType, _Asset.type, _Amount);
                                                    UIManager.Instance.UpdateCurrentUI();
                                                });
                    }
                    else
                    {
                        int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element.FindIndex(item => item.deptID == _Asset.deptID);
                        if (m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[_index].loanAmount > _Asset.price * _Amount)
                        {
                            m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[_index].loanAmount -= _Asset.price * _Amount;
                            RemoveAsset(_Asset.assetType, _Asset.type, _Amount);
                            UIManager.Instance.UpdateCurrentUI();
                        }
                        else
                        {
                            float loanAmount = (float)m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[_index].loanAmount;
                            m_PlayerManager.ApplyMoney(_accountID, (_Asset.price * _Amount) - loanAmount,
                                                    () =>
                                                    {
                                                        BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Loan);
                                                        RemoveAsset(_Asset.assetType, _Asset.type, _Amount);
                                                        UIManager.Instance.UpdateCurrentUI();
                                                    });
                        }
                    }
                }
                break;
            }
        }
    }

    public void RentEstate(Asset _Asset)
    {
        AddAsset(_Asset.assetType, _Asset.type);
        BankManager.Instance.AddRent(_Asset);
        int _Index = m_PlayerManager.assetData.Asset[(int)_Asset.type].element.Count - 1;
        m_PlayerManager.assetData.Asset[(int)_Asset.type].element[_Index].isRent = true;

        UIManager.Instance.UpdateCurrentUI();
    }

    public void CancelRentAsset(string _AssetType, State.AssetType _Type)
    {
        if (m_PlayerManager.rentedThisAsset(_AssetType, _Type))
        {
            RemoveAsset(_AssetType, _Type);
            BankManager.Instance.RemoveRent(_AssetType);
        }
        UIManager.Instance.UpdateCurrentUI();
    }

    public void AddAsset(string _AssetType, State.AssetType _Type, float _Amount = 1)
    {
        PlayerAssetElement m_Asset = ConvertAsset(_AssetType, _Type);

        if (m_PlayerManager.haveThisAsset(_AssetType, _Type))
        {
            m_PlayerManager.assetData.Asset[(int)m_Asset.type].element[m_PlayerManager.assetIndex(_AssetType, _Type)].amount += _Amount;
            m_PlayerManager.assetData.Asset[(int)m_Asset.type].element[m_PlayerManager.assetIndex(_AssetType, _Type)].cost += (m_Asset.price * _Amount);
        }
        else
        {
            m_Asset.amount = _Amount;
            m_Asset.cost *= _Amount;
            m_Asset.buyDate = CalManager.Instance.realDay;
            m_PlayerManager.assetData.Asset[(int)m_Asset.type].element.Add(m_Asset);
        }
    }

    public void LoanAsset(string _AssetType, State.AssetType _Type, int _DeptID, float _Amount = 1)
    {
        PlayerAssetElement m_Asset = ConvertAsset(_AssetType, _Type);
        m_Asset.deptID = _DeptID;

        if (m_PlayerManager.haveThisAsset(_AssetType, _Type))
        {
            m_PlayerManager.assetData.Asset[(int)m_Asset.type].element[m_PlayerManager.assetIndex(_AssetType, _Type)].amount += _Amount;
            m_PlayerManager.assetData.Asset[(int)m_Asset.type].element[m_PlayerManager.assetIndex(_AssetType, _Type)].cost += (m_Asset.price * _Amount);
        }
        else
        {
            m_Asset.amount = _Amount;
            m_Asset.cost *= _Amount;
            m_Asset.buyDate = CalManager.Instance.realDay;
            m_PlayerManager.assetData.Asset[(int)m_Asset.type].element.Add(m_Asset);
        }
    }

    public void RemoveAsset(string _AssetType, State.AssetType _Type)
    {
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)_Type].element.Count; i++)
        {
            if (m_PlayerManager.assetData.Asset[(int)_Type].element[i].assetType == _AssetType)
            {
                m_PlayerManager.assetData.Asset[(int)_Type].element.RemoveAt(i);
                break;
            }
        }
    }

    public void RemoveAsset(string _AssetType, State.AssetType _Type, float _Amount)
    {
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)_Type].element.Count; i++)
        {
            if (m_PlayerManager.assetData.Asset[(int)_Type].element[i].assetType == _AssetType)
            {
                m_PlayerManager.assetData.Asset[(int)_Type].element[i].amount -= _Amount;
                m_PlayerManager.assetData.Asset[(int)_Type].element[i].cost -= (m_PlayerManager.assetData.Asset[(int)_Type].element[i].price * _Amount);

                if (m_PlayerManager.assetData.Asset[(int)_Type].element[i].amount <= 0)
                {
                    m_PlayerManager.assetData.Asset[(int)_Type].element.RemoveAt(i);
                }
                break;
            }
        }
    }

    public void UseAsset(string _AssetType, State.AssetType _Type)
    {
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)_Type].element.Count; i++)
        {
            if (m_PlayerManager.assetData.Asset[(int)_Type].element[i].assetType == _AssetType)
            {
                m_PlayerManager.assetData.Asset[(int)_Type].element[i].isUsing = true;
                if (_Type == State.AssetType.estate)
                {   
                    LocationManager.Instance.SetHomeLocation(m_PlayerManager.assetData.Asset[(int)_Type].element[i].location);
                }
                Debug.Log("is Using " + _AssetType);
                break;
            }
            else
            {
                Debug.Log("Can not find " + _AssetType + " " + m_PlayerManager.assetData.Asset[(int)_Type].element[i].assetType);
            }
        }
    }

    public void UnUseAsset(string _AssetType, State.AssetType _Type)
    {
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)_Type].element.Count; i++)
        {
            if (m_PlayerManager.assetData.Asset[(int)_Type].element[i].assetType == _AssetType)
            {
                m_PlayerManager.assetData.Asset[(int)_Type].element[i].isUsing = false;
                break;
            }
        }
    }

    public void BuyInsurance(Insurance _Insurance, int _accountID = 0)
    {
        m_PlayerManager.SubtractMoney(_accountID, _Insurance.price,
                                        // buy
                                        () => 
                                        {
                                            AddInsurance(_Insurance.insuranceType, _Insurance.type);
                                            if (_Insurance.type == State.Insurance.LifeInsurance)
                                            {
                                                m_PlayerManager.taxData.deduction[(int)State.Deduction.LifeInsurance].deductionRec += _Insurance.price;
                                            }
                                            else if (_Insurance.type == State.Insurance.HealthInsurance)
                                            {
                                                m_PlayerManager.taxData.deduction[(int)State.Deduction.HealthInsurance].deductionRec += _Insurance.price;
                                            }
                                            UIManager.Instance.UpdateCurrentUI();
                                        },
                                        // loan
                                        () =>
                                        {
                                            Debug.LogWarning("No Down Payment");
                                        });
    }

    public void SellLifeInsurance()
    {

    }

    public void AddInsurance(string _InsuranceType, State.Insurance _Type)
    {
        PlayerInsuranceElement m_Insurance = ConvertInsurance(_InsuranceType, _Type);

        if (m_PlayerManager.assetData.insurances[(int)_Type].element.type != State.Insurance.None)
        {
            if (m_PlayerManager.assetData.insurances[(int)_Type].element.insuranceType == _InsuranceType)
            {
                m_PlayerManager.assetData.insurances[(int)_Type].element.expiredDate = m_Insurance.expiredDate;
            }
            else
            {
                Debug.LogWarning("Fix It");
            }
        }
        else
        {
            m_PlayerManager.assetData.insurances[(int)_Type].element = m_Insurance;
        }
    }

    public void RemoveInsurance(State.Insurance _Type)
    {
        m_PlayerManager.assetData.insurances[(int)_Type].element = new PlayerInsuranceElement();
        m_PlayerManager.assetData.insurances[(int)_Type].element.type = State.Insurance.None;
    }

    public PlayerInsuranceElement ConvertInsurance(string _InsuranceType, State.Insurance _Type)
    {
        for (int i = 0; i < insurances[(int)_Type].element.Length; i++)
        {
            if (_InsuranceType == insurances[(int)_Type].element[i].insuranceType)
            {
                int index = i;
                PlayerInsuranceElement newInsurance = new PlayerInsuranceElement();
                newInsurance.insuranceID = index;
                newInsurance.type = insurances[(int)_Type].element[index].type;
                newInsurance.protectionLimit = insurances[(int)_Type].element[index].protectionLimit;
                newInsurance.protectionMaxLimit = insurances[(int)_Type].element[index].protectionLimit;
                if (_Type == State.Insurance.LifeInsurance)
                {
                    newInsurance.expiredDate = CalManager.Instance.realDay + CalManager.Instance.DAY_IN_YEAR;
                }
                else
                {
                    newInsurance.expiredDate = CalManager.Instance.realDay + CalManager.Instance.DAY_IN_YEAR;
                }

                return newInsurance;
            }
        }
        Debug.LogWarning("Cannot find " + _InsuranceType);
        
        return null;
    }
}
