using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InsuranceUIElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_LimitText;
    [SerializeField] private TextMeshProUGUI m_PriceText;

    private int index;
    private State.Insurance type;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowElement(int _Index, State.Insurance _Type)
    {
        index = _Index;
        type = _Type;
        m_NameText.text = AssetManager.Instance.insurances[(int)type].element[index].insuranceName;
        m_LimitText.text = "วงเงินคุ้มครอง " + AssetManager.Instance.insurances[(int)type].element[index].protectionLimit.ToString("N0") + " ฿";
        m_PriceText.text = AssetManager.Instance.insurances[(int)type].element[index].price.ToString("N0") + " ฿";
    }

    public void OnClickBuy()
    {
        AssetManager.Instance.BuyInsurance(AssetManager.Instance.insurances[(int)type].element[index]);
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}