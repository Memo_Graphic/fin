using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TaxUIElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_PercentageText;
    [SerializeField] private TextMeshProUGUI m_AmountText;
    [SerializeField] private TextMeshProUGUI m_LengthText;
    [SerializeField] private TextMeshProUGUI m_MinText;
    [SerializeField] private TextMeshProUGUI m_MaxText;

    [Header("Slider")]
    [SerializeField] private Slider m_SliderText;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowElement(int _Index)
    {
        m_PercentageText.text = PlayerManager.Instance.taxData.tax[_Index].taxRate.ToString("N0") + "%";
        m_AmountText.text = PlayerManager.Instance.taxData.tax[_Index].taxCal.ToString("N0") + " ฿";
        m_LengthText.text = PlayerManager.Instance.taxData.tax[_Index].taxFill.ToString("N0") + " ฿";
        m_MinText.text = PlayerManager.Instance.taxData.tax[_Index - 1].taxLength.ToString("N0");
        m_MaxText.text = PlayerManager.Instance.taxData.tax[_Index].taxLength.ToString("N0");

        m_SliderText.minValue = PlayerManager.Instance.taxData.tax[_Index - 1].taxLength;
        m_SliderText.maxValue = PlayerManager.Instance.taxData.tax[_Index].taxLength;
        m_SliderText.value = PlayerManager.Instance.taxData.tax[_Index - 1].taxLength + PlayerManager.Instance.taxData.tax[_Index].taxFill;

        if (PlayerManager.Instance.taxData.tax[_Index].taxLength < 0)
        {
            m_MaxText.text = "";
            m_SliderText.maxValue = PlayerManager.Instance.taxData.tax[_Index - 1].taxLength + 100000;
            if (PlayerManager.Instance.taxData.tax[_Index].taxFill > 0)
            {
                m_SliderText.value = PlayerManager.Instance.taxData.tax[_Index - 1].taxLength + 100000;
            }
        }

    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
