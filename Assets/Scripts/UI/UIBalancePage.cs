﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIBalancePage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NetWorthAmount;
    [SerializeField] private TextMeshProUGUI m_AssetAmount;
    [SerializeField] private TextMeshProUGUI m_Asset2Amount;
    [SerializeField] private TextMeshProUGUI m_LiabilityAmount;
    [SerializeField] private TextMeshProUGUI m_Liability2Amount;

    [Header("GameObject")]
    [SerializeField] private GameObject[] m_ALBtn;

    [Header("Prefabs")]
    [SerializeField] private AssetLiabilityFolder m_FolderPrefab;
    [SerializeField] private Transform m_AssetParent;
    [SerializeField] private Transform m_LiabilityParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private bool isAsset = true;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroyElementPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(State.UIState.MainPage);
                                                });
    }

    public override void UpdateUI()
    {
        UpdateAsset();
    }

    private void UpdateAsset()
    {
        DestroyElementPrefab();

        m_NetWorthAmount.text = m_PlayerManager.netWorth.ToString("N2") + " ฿";
        m_AssetAmount.text = m_PlayerManager.AssetValue.ToString("N2") + " ฿";
        m_Asset2Amount.text = m_PlayerManager.AssetValue.ToString("N2") + " ฿";
        m_LiabilityAmount.text = m_PlayerManager.LiabilityValue.ToString("N2") + " ฿";
        m_Liability2Amount.text = m_PlayerManager.LiabilityValue.ToString("N2") + " ฿";

        m_AssetParent.transform.parent.parent.gameObject.SetActive(isAsset);
        m_LiabilityParent.transform.parent.parent.gameObject.SetActive(!isAsset);
    
        // Asset
        if (isAsset)
        {
            m_ALBtn[0].SetActive(true);
            m_ALBtn[1].SetActive(false);

            // Account
            AssetLiabilityFolder accountFolder = Instantiate(m_FolderPrefab, m_AssetParent);
            accountFolder.ShowElement("สินทรัพย์สภาพคล่อง", m_PlayerManager.accountData.account);

            // Invest
            List<PlayerAssetElement> invest = new List<PlayerAssetElement>();
            invest.AddRange(m_PlayerManager.assetData.Asset[(int)State.AssetType.stock].element);
            invest.AddRange(m_PlayerManager.assetData.Asset[(int)State.AssetType.fund].element);
            invest.AddRange(m_PlayerManager.assetData.Asset[(int)State.AssetType.gold].element);

            AssetLiabilityFolder investFolder = Instantiate(m_FolderPrefab, m_AssetParent);
            investFolder.ShowElement("สินทรัพย์เพื่อการลงทุน", invest);

            // Asset
            List<PlayerAssetElement> asset = new List<PlayerAssetElement>();
            asset.AddRange(m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element);
            asset.AddRange(m_PlayerManager.assetData.Asset[(int)State.AssetType.car].element);

            AssetLiabilityFolder assetFolder = Instantiate(m_FolderPrefab, m_AssetParent);
            assetFolder.ShowElement("สินทรัพย์ส่วนตัว", asset);

            // Asset Old
            // for (int i = 0; i < m_PlayerManager.assetData.Asset.Length; i ++)
            // {
            //     int _index = i;
            //     AssetTypeElement Asset = m_PlayerManager.assetData.Asset[_index];
            //     AssetLiabilityFolder assetFolder = Instantiate(m_FolderPrefab, m_AssetParent);
            //     assetFolder.ShowElement(Asset.typeName, Asset.element);
            // }
            
            // Lotto
            // AssetLiabilityFolder lottoFolder = Instantiate(m_FolderPrefab, m_AssetParent);
            // lottoFolder.ShowElement("ฉลาก", m_PlayerManager.assetData.lottery);
        }
        // Liability
        else
        {
            m_ALBtn[0].SetActive(false);
            m_ALBtn[1].SetActive(true);

            // Liability Shot Term
            AssetLiabilityFolder deptFolder = Instantiate(m_FolderPrefab, m_LiabilityParent);
            deptFolder.ShowElement("หนี้สินระยะสั้น", m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element);

            // Liability Long Term
            AssetLiabilityFolder liabilityFolder = Instantiate(m_FolderPrefab, m_LiabilityParent);
            List<PlayerLiabilityElement> _Element = new List<PlayerLiabilityElement>();
            
            _Element.AddRange(m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element);
            _Element.AddRange(m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element);

            liabilityFolder.ShowElement("หนี้สินระยะยาว", _Element);

            // Liability Old
            // for (int i = 0; i < m_PlayerManager.liabilityData.Liability.Length; i ++)
            // {
            //     int _index = i;
            //     LiabilityTypeElement Liability = m_PlayerManager.liabilityData.Liability[_index];
            //     if (Liability.typeName != "Rent")
            //     {
            //         AssetLiabilityFolder liabilityFolder = Instantiate(m_FolderPrefab, m_LiabilityParent);
            //         liabilityFolder.ShowElement(Liability.typeName ,m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element);
            //     }
            // }
        }
    }

    private void DestroyElementPrefab()
    {
        for (int i = 0; i < m_AssetParent.childCount; i++)
        {
            m_AssetParent.GetChild(i).GetComponent<AssetLiabilityFolder>().DestroySelf();
        }
        for (int i = 0; i < m_LiabilityParent.childCount; i++)
        {
            m_LiabilityParent.GetChild(i).GetComponent<AssetLiabilityFolder>().DestroySelf();
        }
    }

    public void OnClickSwitchBtn(bool _IsAsset)
    {
        if (isAsset != _IsAsset)
        {
            isAsset = _IsAsset;
            UpdateAsset();
        }
    }

    public void OnClickConfirmBtn()
    {
        Hide();
    }
}
