﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIInvestmentPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_CostText;
    [SerializeField] private TextMeshProUGUI m_UPLText;
    [SerializeField] private TextMeshProUGUI m_UPLPText;

    [Header("Prefabs")]
    [SerializeField] private ActionElement m_MallPrefab;
    [SerializeField] private Transform m_InvestParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private List<Asset> assets;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroyElementPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(UIManager.Instance.previusState);
                                                });
    }

    public override void UpdateUI()
    {
        UpdateBuyAsset();
    }

    public void UpdateBuyAsset()
    {
        assets = new List<Asset>();

        List<Asset> assetsHave = new List<Asset>();
        List<Asset> assetsNotHave = new List<Asset>();

        DestroyElementPrefab();

        for (int i = 0; i < AssetManager.Instance.Asset[(int)State.AssetType.stock].element.Length; i++)
        {
            Asset newAsset = AssetManager.Instance.Asset[(int)State.AssetType.stock].element[i];

            if (m_PlayerManager.haveThisAsset(newAsset.assetType, newAsset.type))
            {
                assetsHave.Add(newAsset);
            }
            else
            {
                assetsNotHave.Add(newAsset);
            }
        }
        for (int i = 0; i < AssetManager.Instance.Asset[(int)State.AssetType.fund].element.Length; i++)
        {
            Asset newAsset = AssetManager.Instance.Asset[(int)State.AssetType.fund].element[i];

            if (m_PlayerManager.haveThisAsset(newAsset.assetType, newAsset.type))
            {
                assetsHave.Add(newAsset);
            }
            else
            {
                assetsNotHave.Add(newAsset);
            }
        }
        for (int i = 0; i < AssetManager.Instance.Asset[(int)State.AssetType.gold].element.Length; i++)
        {
            Asset newAsset = AssetManager.Instance.Asset[(int)State.AssetType.gold].element[i];

            if (m_PlayerManager.haveThisAsset(newAsset.assetType, newAsset.type))
            {
                assetsHave.Add(newAsset);
            }
            else
            {
                assetsNotHave.Add(newAsset);
            }
        }

        for (int i = 0; i < assetsHave.Count; i++)
        {
            assets.Add(assetsHave[i]);
        }
        for (int i = 0; i < assetsNotHave.Count; i++)
        {
            assets.Add(assetsNotHave[i]);
        }

        if (assets != null)
        {
            for (int i = 0; i < assets.Count; i ++)
            {
                ActionElement newObj;
                newObj = Instantiate(m_MallPrefab, m_InvestParent);
                newObj.SetAsset(assets[i], (int)assets[i].type);
            }
        }
        
        float totalCost = 0;
        float totalUPL = 0;
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)State.AssetType.fund].element.Count; i++)
        {
            totalCost += m_PlayerManager.assetData.Asset[(int)State.AssetType.fund].element[i].cost;
            totalUPL += m_PlayerManager.assetData.Asset[(int)State.AssetType.fund].element[i].amount
                        * m_PlayerManager.assetData.Asset[(int)State.AssetType.fund].element[i].price;
        }
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)State.AssetType.stock].element.Count; i++)
        {
            totalCost += m_PlayerManager.assetData.Asset[(int)State.AssetType.stock].element[i].cost;
            totalUPL += m_PlayerManager.assetData.Asset[(int)State.AssetType.stock].element[i].amount
                        * m_PlayerManager.assetData.Asset[(int)State.AssetType.stock].element[i].price;
        }
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)State.AssetType.gold].element.Count; i++)
        {
            totalCost += m_PlayerManager.assetData.Asset[(int)State.AssetType.gold].element[i].cost;
            totalUPL += m_PlayerManager.assetData.Asset[(int)State.AssetType.gold].element[i].amount
                        * m_PlayerManager.assetData.Asset[(int)State.AssetType.gold].element[i].price;
        }
        m_CostText.text = totalCost.ToString("N2") + " ฿";
        m_UPLText.text = (totalUPL - totalCost).ToString("N2") + " ฿";
        if (totalCost != 0)
        {
            m_UPLPText.text = ((totalUPL - totalCost) / totalCost * 100f).ToString("+0.00;-0.00;") + " %";
        }
        else
        {
            m_UPLPText.text = totalCost.ToString("+0.00;-0.00;") + "%";
        }
        if ((totalUPL - totalCost) < 0)
        {
            m_UPLText.color = GameManager.Instance.red;
        }
        else
        {
            m_UPLText.color = GameManager.Instance.green;
        }        
    }

    private void DestroyElementPrefab()
    {
        for (int i = 0; i < m_InvestParent.childCount; i++)
        {
            m_InvestParent.GetChild(i).GetComponent<ActionElement>().DestroySelf();
        }
    }

    public void OnClickFadeBtn()
    {
        Hide();
    }
}