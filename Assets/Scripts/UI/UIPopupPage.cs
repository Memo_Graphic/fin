﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

public class UIPopupPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_TitleText;
    [SerializeField] private TextMeshProUGUI m_CostText;
    [SerializeField] private TextMeshProUGUI m_SalaryText;
    [SerializeField] private TextMeshProUGUI m_NetSalaryText;
    [SerializeField] private TextMeshProUGUI m_ScamPriceText;
    [SerializeField] private TextMeshProUGUI m_ScamPrice2Text;
    [SerializeField] private TextMeshProUGUI m_ScamPriceWinText;
    [SerializeField] private TextMeshProUGUI m_ScamPriceWin2Text;
    [SerializeField] private TextMeshProUGUI m_ScamWinText;
    [SerializeField] private TextMeshProUGUI m_ScamLoseText;
    [SerializeField] private TextMeshProUGUI m_HospitalHintText;
    [SerializeField] private TextMeshProUGUI m_TaskText;
    [SerializeField] private TextMeshProUGUI m_NextSalaryText;
    [SerializeField] private TextMeshProUGUI m_JobNextSpeedText;
    [SerializeField] private TextMeshProUGUI m_JobSkillText;
    [SerializeField] private TextMeshProUGUI m_InterestText;

    [Header("Slider")]
    [SerializeField] private Slider m_KPISlider;
    [SerializeField] private Slider m_SkillSlider;

    [Header("GameObject")]
    [SerializeField] private GameObject[] m_PopupObj;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private int selectLocation;
    private Location location;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
        CloseAll();
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(State.UIState.MainPage);
                                                });
    }

    public void OnClickPopup(GameObject obj)
    {
        obj.SetActive(false);
    }

    public void OnClickClosePopup(State.PopupPage popupPage)
    {
        m_PopupObj[(int)popupPage].SetActive(false);
    }

    public override void UpdateUI()
    {

    }

    public void CloseAll()
    {
        for (int i = 0; i < m_PopupObj.Length; i ++)
        {
            m_PopupObj[i].SetActive(false);
        }
        Debug.Log("Close Popup all");
    }

    public void OpenPopup(State.PopupPage popupPage)
    {
        m_PopupObj[(int)popupPage].SetActive(true);

        if (popupPage == State.PopupPage.GetSalaly)
        {
            GetSalaty();
        }
        else if (popupPage == State.PopupPage.Scamming)
        {
            UpdateScaming();
        }
        else if (popupPage == State.PopupPage.Scamming)
        {
            UpdateScaming();
        }
        else if (popupPage == State.PopupPage.ScamWin)
        {
            UpdateScamEnd();
        }
        else if (popupPage == State.PopupPage.ScamLose)
        {
            UpdateScamEnd();
        }
        else if (popupPage == State.PopupPage.Hospital)
        {
            UpdateHospital();
        }
        else if (popupPage == State.PopupPage.TaskComplete)
        {
            UpdateTask();
        }
        else if (popupPage == State.PopupPage.GetSkill)
        {
            UpdateSkill();
        }
        else if (popupPage == State.PopupPage.GetInterest)
        {
            UpdateInterest();
        }
    }

    public async void OpenPopupAsync(State.PopupPage popupPage)
    {
        GameManager.Instance.ToggleEventSystem(false);
        await Task.Delay(750);
        OpenPopup(popupPage);
        GameManager.Instance.ToggleEventSystem(true);
    }

    public void GetSalaty()
    {
        float salary = m_PlayerManager.jobData.fullTimeJob.salary;
        float cost = JobManager.Instance.salaryCost;

        m_TitleText.text = "เงินเดือน " + CalManager.Instance.DateTime("MM/YYYY");
        m_CostText.text = cost.ToString("N0") + " ฿";
        m_SalaryText.text = salary.ToString("N0") + " ฿";
        m_NetSalaryText.text = (salary - cost).ToString("N0") + " ฿";
    }

    public void OnClickSaving()
    {
        OnClickClosePopup(State.PopupPage.GetSalaly);
        OnClickOpenPageBtn("AccountPage");
    }

    public void OnClickChangeToCash(bool _useCredit)
    {
        ActionManager.Instance.useCredit = _useCredit;
        OnClickClosePopup(State.PopupPage.ChangePayment);
        UIManager.Instance.UpdateUI(State.UIState.MainPage);
    }

    public void OnClickScamming()
    {
        BankManager.Instance.OnScamming();
    }

    public void UpdateScaming()
    {
        m_ScamPriceText.text = BankManager.Instance.scamData.scamPriceSubmit.ToString("N0") + " ฿";
        m_ScamPriceWinText.text = "รับเงินคืน ";
        m_ScamPriceWinText.text += (BankManager.Instance.scamData.scamPriceSubmit * BankManager.Instance.scamData.scamRatio).ToString("N0") + " ฿ ";
        m_ScamPriceWinText.text += "ภายในวันที่ " + CalManager.Instance.DateTime("DD/MM/YY", CalManager.Instance.realDay + CalManager.Instance.SCAM_LENGHT_DATE);
    }

    public void UpdateScamEnd()
    {
        m_ScamWinText.text = (BankManager.Instance.scamData.scamPriceSubmit * BankManager.Instance.scamData.scamRatio).ToString("N0") + " ฿";
        m_ScamLoseText.text = BankManager.Instance.scamData.scamPriceSubmit.ToString("N0") + " ฿";
    }

    public void OnClickChangeScam(float _Value)
    {
        BankManager.Instance.scamData.scamPriceSubmit += _Value;
        if (BankManager.Instance.scamData.scamPriceSubmit < 1000)
        {
            BankManager.Instance.scamData.scamPriceSubmit = 1000;
        }
        UpdateScaming();
    }

    public void UpdateHospital()
    {
        m_HospitalHintText.text = "แก้ไขด่วน";
        if (ActionManager.Instance.hospitalData.currentHospital == State.Hospital.Health)
        {
            m_HospitalHintText.text = "เนื่องจากอาการป่วย และสุขภาพ";
        }
        else if (ActionManager.Instance.hospitalData.currentHospital == State.Hospital.Accident)
        {
            m_HospitalHintText.text = "เนื่องจากเกิดอุบัติเหตุ";
        }
    }

    public void UpdateTask()
    {
        Job nextJob = JobManager.Instance.Job[(int)State.JobType.FullTime].jobs[m_PlayerManager.jobData.fullTimeJob.jobID + 1];
        m_TaskText.text = m_PlayerManager.jobData.completedTask.ToString("N0") + "/" + m_PlayerManager.jobData.fullTimeJob.kpi.ToString("N0");;
        m_NextSalaryText.text = nextJob.startSalary.ToString("N0") + " ฿";
        m_KPISlider.maxValue = m_PlayerManager.jobData.fullTimeJob.kpi;
        m_KPISlider.value = m_PlayerManager.jobData.completedTask;
    }

    public void UpdateSkill()
    {
        m_JobNextSpeedText.text = (m_PlayerManager.playerData.lvSkill.JobSpeed + JobManager.Instance.upSkills.getSpeed).ToString("N0");
        m_JobSkillText.text = m_PlayerManager.playerData.lvSkill.currentEXP.ToString("N0") + "/" + JobManager.Instance.upSkills.maxExp.ToString("N0");

        m_SkillSlider.maxValue = JobManager.Instance.upSkills.maxExp;
        m_SkillSlider.value = m_PlayerManager.playerData.lvSkill.currentEXP;
    }

    public void UpdateInterest()
    {
        m_InterestText.text = m_PlayerManager.accountData.account[1].accruedExpenses.ToString("N2") + "฿";
    }

    public void OnClickGetJob()
    {
        JobManager.Instance.ApplyFullTime(JobManager.Instance.Job[(int)State.JobType.FullTime].jobs[0]);
        UIManager.Instance.UpdateUI(State.UIState.EstatePage);
        OnClickClosePopup(State.PopupPage.GetJob);
        StateManager.Instance.NextState();
    }

    public void OnClickStartGame()
    {
        CalManager.Instance.SetStarterValue();
        StateManager.Instance.SwitchState(State.GameState.Story_6_0);
        UIManager.Instance.Show(State.UIState.TutorialPage);
    }

}
