﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIInvestUnitPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_TypeText;
    [SerializeField] private TextMeshProUGUI m_NAVText;
    [SerializeField] private TextMeshProUGUI m_cNAVText;
    [SerializeField] private TextMeshProUGUI m_ValueText;
    [SerializeField] private TextMeshProUGUI m_UnitText;
    [SerializeField] private TextMeshProUGUI m_CostText;
    [SerializeField] private TextMeshProUGUI m_UPLText;
    [SerializeField] private TextMeshProUGUI m_isBuyText;
    [SerializeField] private TextMeshProUGUI m_CanUnitTitleText;
    [SerializeField] private TextMeshProUGUI m_CanAmountTitleText;
    [SerializeField] private TextMeshProUGUI m_CanUnitText;
    [SerializeField] private TextMeshProUGUI m_CanAmountText;

    [SerializeField] private TextMeshProUGUI m_CalAmountText;
    [SerializeField] private TextMeshProUGUI m_CalUnitText;

    [Header("Image")]
    [SerializeField] private Sprite m_Spt1;
    [SerializeField] private Sprite m_Spt2;
    [SerializeField] private Image m_Input1;
    [SerializeField] private Image m_Input2;
    [SerializeField] private Image m_BGImg;
    [SerializeField] private Image m_BuyBGImg;

    [Header("Button")]
    [SerializeField] private Button m_DoneBtn;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;


    private PlayerAssetElement playerAsset;
    private Asset asset;
    private int type;

    private int currentAccount;
    private float unitBuy;
    private float amountBuy;
    private float unitSell;
    private float amountSell;
    private float amount;
    private bool isBuy = true;
    private bool isUnit = true;

    private float canSellUnit;
    private float canBuyAmount;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {

    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    gameObject.SetActive(false);
                                                });
    }

    public override void UpdateUI()
    {
        currentAccount = 1;
    }
    
    public void SetAsset(Asset _Asset, int _Type, System.Action callback = null)
    {
        playerAsset = null;
        asset = _Asset;
        type = _Type;
        
        for (int i = 0; i < m_PlayerManager.assetData.Asset[type].element.Count; i++)
        {
            if (m_PlayerManager.assetData.Asset[type].element[i].assetType == asset.assetType)
            {
                playerAsset = m_PlayerManager.assetData.Asset[type].element[i];
                break;
            }
        }

        // isBuy = false;
        // OnClickBuySell();
        OnClickAmountUnit(isUnit);

        UpdateAsset();
    }

    public void SetAsset(PlayerAssetElement _Asset, int _Type, System.Action callback = null)
    {
        playerAsset = _Asset;
        asset = null;
        type = _Type;

        asset = AssetManager.Instance.FindAsset(_Asset.assetType, (State.AssetType)_Type);

        // isBuy = false;
        // OnClickBuySell();
        OnClickAmountUnit(isUnit);

        UpdateAsset();
    }

    public void UpdateAsset()
    {
        canBuyAmount = (float)m_PlayerManager.accountData.account[currentAccount].balance;

        if (playerAsset != null)
        {
            m_NameText.text = playerAsset.assetType;
            // m_TypeText.text = AssetManager.Instance.Asset[(int)playerAsset.type].typeName;
            m_TypeText.text = playerAsset.assetName;
            m_NAVText.text = playerAsset.price.ToString("N2") + " ฿";
            m_cNAVText.text = playerAsset.cPrice.ToString("+0.00;-0.00;") + "%";
            m_ValueText.text = (playerAsset.amount * playerAsset.price).ToString("N2") + " ฿";
            m_UnitText.text = playerAsset.amount.ToString("N2");
            m_CostText.text = playerAsset.cost.ToString("N2") + " ฿";
            m_UPLText.text = playerAsset.unrealized.ToString("+0.00;-0.00;") + " ฿("
                            + playerAsset.unrealizedP.ToString("N2") + "%)";
            if (playerAsset.unrealized < 0)
            {
                m_UPLText.color = GameManager.Instance.red;
            }
            else
            {
                m_UPLText.color = GameManager.Instance.green;
            }

            if (playerAsset.cPrice < 0)
            {
                m_BGImg.color = GameManager.Instance.red;
            }
            else
            {
                m_BGImg.color = GameManager.Instance.green;
            }

            canSellUnit = playerAsset.amount;
        }
        else
        {
            m_NameText.text = "N/A";
            m_TypeText.text = "N/A";
            m_NAVText.text = "N/A ฿";
            m_cNAVText.text = "-";
            m_ValueText.text = "-";
            m_UnitText.text = "-";
            m_CostText.text = "-";
            m_UPLText.text = "-";
            m_UPLText.color = Color.white;
            m_BGImg.color = GameManager.Instance.green;

            canSellUnit = 0;

            if (asset != null)
            {
                m_NameText.text = asset.assetType;
                // m_TypeText.text = AssetManager.Instance.Asset[(int)asset.type].typeName;
                m_TypeText.text = asset.assetName;
                m_NAVText.text = asset.price.ToString("N2") + " ฿";
                m_cNAVText.text = asset.cPrice.ToString("N2") + "%";
            
                if (asset.cPrice < 0)
                {
                    m_BGImg.color = GameManager.Instance.red;
                }
                else
                {
                    m_BGImg.color = GameManager.Instance.green;
                }
            }
        }

        amount = 0;
        unitBuy = 0;
        unitSell = 0;
        CalculateUnitAmount();
        UpdateCalculator();
    }

    private void UpdateCalculator()
    {
        if (isBuy)
        {
            m_isBuyText.text = "ซื้อ";
            m_BuyBGImg.color = GameManager.Instance.green;

            m_CanUnitTitleText.text = "จำนวนที่ซื้อได้";
            m_CanAmountTitleText.text = "จำนวนที่ซื้อได้";
            m_CanUnitText.text = (Mathf.FloorToInt(canBuyAmount / asset.price * 100f) / 100f).ToString("N2") + " หน่วย";
            m_CanAmountText.text = canBuyAmount.ToString("N2") + " ฿";
            m_CalAmountText.text = (amountBuy).ToString("N2");
            m_CalUnitText.text = unitBuy.ToString("N2");
            m_DoneBtn.interactable = canBuyAmount >= amountBuy;
        }
        else
        {
            m_isBuyText.text = "ขาย";
            m_BuyBGImg.color = GameManager.Instance.red;

            m_CanUnitTitleText.text = "จำนวนที่ขายได้";
            m_CanAmountTitleText.text = "จำนวนที่ขายได้";

            m_CanUnitText.text = canSellUnit.ToString("N2") + " หน่วย";
            m_CanAmountText.text = (canSellUnit * asset.price).ToString("N2") + " ฿";
            m_CalAmountText.text = (amountSell).ToString("N2");
            m_CalUnitText.text = unitSell.ToString("N2");
            m_DoneBtn.interactable = canSellUnit >= unitSell;
        }
    }

    public void OnClickBuySell()
    {
        isBuy = !isBuy;
        CalculateUnitAmount();
        UpdateCalculator();
    }

    public void OnClickAmountUnit(bool _IsUnit)
    {
        isUnit = _IsUnit;
        if (!isUnit && type == (int)State.AssetType.stock)
        {
            isUnit = true;
        }

        if (isUnit)
        {
            m_Input1.sprite = m_Spt1;
            m_Input2.sprite = m_Spt2;
        }
        else
        {
            m_Input1.sprite = m_Spt2;
            m_Input2.sprite = m_Spt1;
        }
        UpdateCalculator();
    }
    
    public void OnClickChangeUnit(float _Value)
    {
        if (isBuy)
        {
            unitBuy += _Value;

            if (unitBuy < 0)
            {
                unitBuy = 0;
            }
        }
        else
        {
            if (playerAsset != null )
            {
                if (_Value < 0 && unitSell > Mathf.Floor(unitSell))
                {
                    unitSell = Mathf.Floor(unitSell);
                }
                else
                {
                    unitSell += _Value;
                }

                if (unitSell > playerAsset.amount)
                {
                    unitSell = playerAsset.amount;
                }
                else if (unitSell < 0)
                {
                    unitSell = 0;
                }
            }
        }
        
        UpdateCalculator();
    }

    public void OnClickCalDone()
    {
        if (isBuy)
        {
            if (unitBuy > 0)
            {
                AssetManager.Instance.BuyAsset(asset, 
                                            PlayerManager.Instance.accountData.account[currentAccount].accountID, 
                                            unitBuy);

                if (playerAsset == null)
                {
                    for (int i = 0; i < m_PlayerManager.assetData.Asset[type].element.Count; i++)
                    {
                        if (m_PlayerManager.assetData.Asset[type].element[i].assetType == asset.assetType)
                        {
                            playerAsset = m_PlayerManager.assetData.Asset[type].element[i];
                            break;
                        }
                    }
                }
            }
            SetAsset(asset, (int)asset.type);
        }
        else
        {
            if (unitSell > 0)
            {
                AssetManager.Instance.SellAsset(playerAsset.assetType, 
                                            playerAsset.type, 
                                            PlayerManager.Instance.accountData.account[currentAccount].accountID, 
                                            unitSell);
            }
            SetAsset(asset, (int)asset.type);
        }
    }

    public void OnClickCalBtn(string BTN)
    {
        if (isBuy)
        {
            if (isUnit)
            {
                amount = unitBuy;
            }
            else
            {
                amount = amountBuy;
            }
        }
        else
        {
            if (isUnit)
            {
                amount = unitSell;
            }
            else
            {
                amount = amountSell;
            }
        }

        switch(BTN) 
        {
        case "1":
            amount = (Mathf.Floor(amount) * 10) + 1;
            break;
        case "2":
            amount = (Mathf.Floor(amount) * 10) + 2;
            break;
        case "3":
            amount = (Mathf.Floor(amount) * 10) + 3;
            break;
        case "4":
            amount = (Mathf.Floor(amount) * 10) + 4;
            break;
        case "5":
            amount = (Mathf.Floor(amount) * 10) + 5;
            break;
        case "6":
            amount = (Mathf.Floor(amount) * 10) + 6;
            break;
        case "7":
            amount = (Mathf.Floor(amount) * 10) + 7;
            break;
        case "8":
            amount = (Mathf.Floor(amount) * 10) + 8;
            break;
        case "9":
            amount = (Mathf.Floor(amount) * 10) + 9;
            break;
        case "0":
            amount = (Mathf.Floor(amount) * 10) + 0;
            break;
        case "00":
            amount = (Mathf.Floor(amount) * 10) * 10;
            break;
        case ".":
            
            break;
        case "<":
            amount = Mathf.Floor(amount / 10);
            break;
        case "AC":
            amount = 0;
            break;
        case "DONE":
            OnClickCalDone();
            break;
        default:
            // code block
            break;
        }

        CalculateUnitAmount();
        UpdateCalculator();
    }

    public void CalculateUnitAmount()
    {
        if (isBuy)
        {
            if (isUnit)
            {
                if (amount * asset.price > canBuyAmount)
                {
                    amount = Mathf.FloorToInt(canBuyAmount / asset.price * 100f) / 100f;
                }

                unitBuy = amount;
                amountBuy = unitBuy * asset.price;
            }
            else
            {
                
                if (amount > canBuyAmount)
                {
                    amount = canBuyAmount;
                }

                unitBuy = Mathf.FloorToInt(amount / asset.price * 100f) / 100f;
                // amountBuy = unitBuy * asset.price;
                amountBuy = amount;
            }
        }
        else
        {
            
            if (playerAsset == null)
            {
                amount = 0;
                unitSell = amount;
                amountSell = amount;
            }
            else
            {
                if (isUnit)
                {
                    if (amount > playerAsset.amount)
                    {
                        amount = playerAsset.amount;
                    }

                    unitSell = amount;
                    amountSell = unitSell * playerAsset.price;
                }
                else
                {
                    if (amount > (playerAsset.amount * playerAsset.price))
                    {
                        amount = (playerAsset.amount * playerAsset.price);
                    }

                    unitSell = Mathf.FloorToInt(amount / playerAsset.price * 100f) / 100f;
                    // amountSell = unitSell * playerAsset.price;
                    amountSell = amount;
                }
            }
        }
    }

    public void OnClickFadeBtn()
    {
        Hide();
    }
}
