﻿using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIMenuPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_MoneyText;

    [Header("ETC")]
    public CanvasGroup canvasGroup;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public override void Show()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                false,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    canvasGroup.interactable = true;
                                                });
    }
    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    canvasGroup.interactable = true;
                                                });
    }

    public void LoadScene(string _SceneName)
    {
        StartCoroutine(LoadAsyncScene(_SceneName));
    }

    IEnumerator LoadAsyncScene(string _SceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(_SceneName);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
