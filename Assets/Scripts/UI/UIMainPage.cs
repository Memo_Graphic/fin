using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIMainPage : UIElement
{
    [Header("TMP - Main")]
    [SerializeField] private TextMeshProUGUI m_CoolText;
    [SerializeField] private TextMeshProUGUI m_MoneyText;
    [SerializeField] private TextMeshProUGUI m_NetWorthText;
    [SerializeField] private TextMeshProUGUI m_JobTravelText;
    [SerializeField] private TextMeshProUGUI m_NotiText;

    [Header("TMP - DateTime")]
    [SerializeField] private TextMeshProUGUI m_DateTimeDayText;
    [SerializeField] private TextMeshProUGUI m_DateTimeMonthText;
    [SerializeField] private TextMeshProUGUI m_DateTimeYearText;
    
    [Header("Planner - Phase A")]
    public int setActionIndex;
    [SerializeField] private Image m_ActionImage;
    [SerializeField] private TextMeshProUGUI m_SetActionNameText;
    [SerializeField] private TextMeshProUGUI m_SetActionTimeText;
    [SerializeField] private TextMeshProUGUI m_SetActionTimerText;
    [SerializeField] private TextMeshProUGUI m_SetActionUseHealthText;
    [SerializeField] private TextMeshProUGUI m_SetActionUseHappyText;
    [SerializeField] private TextMeshProUGUI m_SetActionCashText;
    [SerializeField] private TextMeshProUGUI m_SleepTimeText;
    [SerializeField] private TextMeshProUGUI m_SleepTime2Text;
    [SerializeField] private TextMeshProUGUI m_SleepHealthText;
    [SerializeField] private TextMeshProUGUI m_SleepHappyText;
    [SerializeField] private TextMeshProUGUI m_WorkPriceText;

    [Header("Planner Sum")]
    [SerializeField] private TextMeshProUGUI m_SumTimeText;
    [SerializeField] private TextMeshProUGUI m_SumHealthText;
    [SerializeField] private TextMeshProUGUI m_SumHappinessText;
    [SerializeField] private TextMeshProUGUI m_SumMoneyCostText;

    [Header("TMP - Phase B")]
    [SerializeField] private TextMeshProUGUI m_TaskText;
    [SerializeField] private TextMeshProUGUI m_TaskPText;
    [SerializeField] private TextMeshProUGUI m_HealthText;
    [SerializeField] private TextMeshProUGUI m_HappyText;
    [SerializeField] private TextMeshProUGUI m_TimeText;

    [Header("TMP - Phase Hospital")]
    [SerializeField] private TextMeshProUGUI m_HospitalCostText;
    [SerializeField] private TextMeshProUGUI m_HospitalInsuranceText;
    [SerializeField] private TextMeshProUGUI m_HospitalTotalText;

    [Header("TMP - PartTime")]
    [SerializeField] private TextMeshProUGUI m_PartTimeHealthText;
    [SerializeField] private TextMeshProUGUI m_PartTimeHappyText;
    [SerializeField] private TextMeshProUGUI m_PartTimeTimeText;
    [SerializeField] private TextMeshProUGUI m_PartTimeSalaryText;

    [Header("TMP - UpSkill")]
    [SerializeField] private TextMeshProUGUI m_UpSkillHealthText;
    [SerializeField] private TextMeshProUGUI m_UpSkillHappyText;
    [SerializeField] private TextMeshProUGUI m_UpSkillTimeText;
    [SerializeField] private TextMeshProUGUI m_UpSkillPointText;

    [Header("Slider")]
    [SerializeField] private Slider m_HappinessSlider;
    [SerializeField] private Slider m_HealthSlider;
    [SerializeField] private Slider m_TaskSlider;
    [SerializeField] private Slider m_GoalSlider;

    [Header("Slider - Phase A")]
    [SerializeField] private Slider m_ASleepTimeSlider;
    [SerializeField] private Slider m_AActionTimeSlider;
    [SerializeField] private Slider m_AWorkTimeSlider;
    [SerializeField] private Slider m_AFreeTimeSlider;

    [Header("Slider - Phase B")]
    [SerializeField] private Slider m_SleepTimeSlider;
    [SerializeField] private Slider m_ActionTimeSlider;
    [SerializeField] private Slider m_WorkTimeSlider;
    [SerializeField] private Slider m_FreeTimeSlider;

    [Header("Planner GameObjects")]
    [SerializeField] private GameObject m_PopupAction;
    [SerializeField] private GameObject m_SetSleep;
    [SerializeField] private GameObject m_SetAction; 
    [SerializeField] private GameObject m_SelectAction;

    [Header("GameObject")]
    [SerializeField] private GameObject[] m_ActionTypeBtn;
    [SerializeField] private GameObject[] m_ActivityTypeBtn;
    [SerializeField] private ActionElement m_ActionPrefab;
    [SerializeField] private GameObject m_ActivityPanel;
    [SerializeField] private GameObject m_ActionPanel;
    [SerializeField] private GameObject m_SetActionPanel;

    [Header("Buttons")]
    [SerializeField] private Button m_PartTimeBtn;
    [SerializeField] private Button m_GoHomeBtn;

    [Header("Image")]
    [SerializeField] private Image m_DayImage;
    [SerializeField] private Image m_TimeImage;
    [SerializeField] private Image m_CarImage;
    [SerializeField] private Image m_PaymentImage;
    [SerializeField] private Sprite[] m_PaymentSprt;
    [SerializeField] private Image m_ProfileImage;
    [SerializeField] private Sprite[] m_ProfileSprt;
    [SerializeField] private Image[] m_PlayerImage0;
    [SerializeField] private Image[] m_PlayerImage1;
    [SerializeField] private Image[] m_PlayerImage2;
    
    [Header("Animators")]
    [SerializeField] private Animator m_BottomAnim;

    [Header("Prefabs")]
    [SerializeField] private ActionPlanElement actionPlannerPrefab;
    [SerializeField] private Transform m_ActionPlannerParent;

    [SerializeField] private ActionElement m_SetActionPrefab;
    [SerializeField] private Transform m_SetActionParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private double money;
    private double oldMoney;
    private double worth;
    private double oldWorth;
    private double moneyTimimg;
    private double worthTimimg;

    private float health;
    private float oldHealth;
    private float happy;
    private float oldHappy;
    private float healthTimimg;
    private float happyTimimg;


    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
        UpdateUI();
    }

    void Update()
    {
        UpdateTimeB();
        UpdateMoney();
        UpdateNetWorth();
        UpdateQoLRealTime();
    }

    public override void Show()
    {
        // TouchManager.Instance.isShow = true;
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                false,
                                                true,
                                                0f,
                                                0f,
                                                () =>
                                                {
                                                    // canvasGroup.interactable = true;
                                                });
    }
    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    // canvasGroup.interactable = true;
                                                });
    }

    public override void UpdateUI()
    {
        sleepTimeInHour = m_PlayerManager.playerData.sleepTimeInHour;

        UpdateQoL();
        UpdateTime();
        UpdateSumUI();
        UpdateJobTask();
        UpdateSleepTimeUI();
        UpdateSetActionZone();

        // m_FullTimeHourText.text = CalManager.Instance.TextMinTime(m_PlayerManager.jobData.fullTimeJob.workTime, "HH");
        // m_PartTimeHourText.text = CalManager.Instance.TextMinTime(m_PlayerManager.jobData.partTimeJob.workTime, "HH");
        m_JobTravelText.text = CalManager.Instance.TextMinTime((LocationManager.Instance.currentDistance * LocationManager.Instance.currenTravel.MIN_PER_KM), "HH");   

        
        float income = Mathf.Floor(m_PlayerManager.jobData.fullTimeJob.salary / 50);

        m_PartTimeHealthText.text = m_PlayerManager.jobData.fullTimeJob.useHealth.ToString("N0");
        m_PartTimeHappyText.text = m_PlayerManager.jobData.fullTimeJob.useHappy.ToString("N0");
        m_PartTimeTimeText.text = CalManager.Instance.TextMinTime(CalManager.Instance.MIN_IN_HOUR, "HH");
        m_PartTimeSalaryText.text = income.ToString("N0") + " ฿";
        
        m_UpSkillHealthText.text = JobManager.Instance.upSkills.useHealth.ToString("N0");
        m_UpSkillHappyText.text = JobManager.Instance.upSkills.useHappy.ToString("N0");
        m_UpSkillTimeText.text = CalManager.Instance.TextMinTime(JobManager.Instance.upSkills.learnTime, "HH");
        m_UpSkillPointText.text = JobManager.Instance.upSkills.getExp.ToString("N0");

        m_NotiText.text = m_PlayerManager.billData.bill.Count.ToString("N0");
        m_CarImage.sprite = LocationManager.Instance.currenTravel.travelImage;

        if (ActionManager.Instance.useCredit)
        {
            m_PaymentImage.sprite = m_PaymentSprt[1];
        }
        else
        {
            m_PaymentImage.sprite = m_PaymentSprt[0];
        }

        m_ProfileImage.sprite = m_PlayerManager.playerData.playerGender == "Male" ? m_ProfileSprt[0] : m_ProfileSprt[1];
        m_PlayerImage0[0].gameObject.SetActive(m_PlayerManager.playerData.playerGender == "Male");
        m_PlayerImage1[0].gameObject.SetActive(m_PlayerManager.playerData.playerGender == "Male");
        m_PlayerImage2[0].gameObject.SetActive(m_PlayerManager.playerData.playerGender == "Male");
        m_PlayerImage0[1].gameObject.SetActive(m_PlayerManager.playerData.playerGender != "Male");
        m_PlayerImage1[1].gameObject.SetActive(m_PlayerManager.playerData.playerGender != "Male");
        m_PlayerImage2[1].gameObject.SetActive(m_PlayerManager.playerData.playerGender != "Male");
    }

    private void UpdateMoney()
    {
        if (oldMoney != m_PlayerManager.accountData.account[0].balance)
        {
            if (moneyTimimg > 0.5f)
            {
                oldMoney = m_PlayerManager.accountData.account[0].balance;
                money = oldMoney;
                moneyTimimg = 0f;
            }
            else
            {
                moneyTimimg += Time.deltaTime;
                money = oldMoney + (((m_PlayerManager.accountData.account[0].balance - oldMoney) / 0.5f) * (moneyTimimg));
            }
        }
        else
        {
            money = oldMoney;
        }
        m_MoneyText.text = money.ToString("N2") + " ฿";
    }

    private void UpdateNetWorth()
    {
        if (oldWorth != m_PlayerManager.netWorth)
        {
            if (worthTimimg > 0.5f)
            {
                oldWorth = m_PlayerManager.netWorth;
                worth = oldWorth;
                worthTimimg = 0f;
            }
            else
            {
                worthTimimg += Time.deltaTime;
                // if (worthTimimg > 0.5f)
                // {
                    worth = oldWorth + (((m_PlayerManager.netWorth - oldWorth) / 0.5f) * (worthTimimg));
                // }
            }
        }
        else
        {
            worth = oldWorth;
        }
        m_NetWorthText.text = worth.ToString("N2") + " ฿";
    }

    private void UpdateQoLRealTime()
    {
        if (oldHealth != m_PlayerManager.playerData.health)
        {
            if (healthTimimg > 0.25f)
            {
                oldHealth = m_PlayerManager.playerData.health;
                health = oldHealth;
                healthTimimg = 0f;
            }
            else
            {
                healthTimimg += Time.deltaTime;
                health = oldHealth + (((m_PlayerManager.playerData.health - oldHealth) / 0.25f) * (healthTimimg));
            }
        }
        else
        {
            health = oldHealth;
        }
        m_HealthText.text = health.ToString("N0");

        if (oldHappy != m_PlayerManager.playerData.happiness)
        {
            if (happyTimimg > 0.25f)
            {
                oldHappy = m_PlayerManager.playerData.happiness;
                happy = oldHappy;
                happyTimimg = 0f;
            }
            else
            {
                happyTimimg += Time.deltaTime;
                happy = oldHappy + (((m_PlayerManager.playerData.happiness - oldHappy) / 0.25f) * (happyTimimg));
            }
        }
        else
        {
            happy = oldHappy;
        }
        m_HappyText.text = happy.ToString("N0");
    }

    public void UpdateTimeB()
    {
        
    }

    private void UpdateQoL()
    {
        m_HappinessSlider.maxValue = m_PlayerManager.playerData.maxHappiness;
        m_HappinessSlider.value = m_PlayerManager.playerData.happiness;
        m_HealthSlider.maxValue = m_PlayerManager.playerData.maxHealth;
        m_HealthSlider.value = m_PlayerManager.playerData.health;
        
        m_CoolText.text = m_PlayerManager.NetCool.ToString("N0");
    }

    private void UpdateTime()
    {
        m_DateTimeDayText.text = CalManager.Instance.Day().ToString("N0");
        m_DateTimeMonthText.text = CalManager.Instance.Month().ToString("00");
        m_DateTimeYearText.text = (CalManager.Instance.Year() + 2000).ToString("0000");
        m_TimeText.text = CalManager.Instance.TextMinTime(m_PlayerManager.playerData.Time.minute, "HHMM");
        m_DayImage.fillAmount = CalManager.Instance.Day() * 0.125f + ((CalManager.Instance.Day() - 1) * 0.05f);
        
        if (m_PlayerManager.playerData.Time.minute > (CalManager.Instance.MIN_IN_DAY - AllTime))
        {
            m_FreeTimeSlider.value = CalManager.Instance.MIN_IN_DAY - AllTime;
        }
        else
        {
            m_FreeTimeSlider.value = m_PlayerManager.playerData.Time.minute;
        }
    }

    private void UpdateJobTask()
    {
        m_TaskText.text = m_PlayerManager.jobData.currenTask.ToString("N0") + "/" + m_PlayerManager.jobData.fullTimeJob.workpoint.ToString("N0");
        m_TaskPText.text = m_PlayerManager.jobData.currenTask.ToString("N0") + "/" + m_PlayerManager.jobData.fullTimeJob.workpoint.ToString("N0");
        m_TaskSlider.maxValue = m_PlayerManager.jobData.fullTimeJob.workpoint;
        m_TaskSlider.value = m_PlayerManager.jobData.currenTask;
    }

    public void OpenActionPanel(string _ActionType)
    {
        State.Expense _actionType = (State.Expense)Enum.Parse(typeof(State.Expense), _ActionType);
        for (int i = 0; i < ActionManager.Instance.activity[(int)_actionType].activity.Length; i++)
        {
            ActionElement newAtcion = Instantiate(m_ActionPrefab, m_ActionPanel.transform);
            newAtcion.SetAction(ActionManager.Instance.activity[(int)_actionType].activity[i],
                                (int)_actionType);
        }
        m_ActivityPanel.SetActive(true);
    }

    public void SelectActionPanel(string _ActionType)
    {
        for (int i = 0; i < m_ActionPanel.transform.childCount; i++)
        {
            m_ActionPanel.transform.GetChild(i).GetComponent<ActionElement>().DestroySelf();
        }

        State.Expense _actionType = (State.Expense)Enum.Parse(typeof(State.Expense), _ActionType);

        for (int i = 0; i < m_ActivityTypeBtn.Length; i++)
        {
            m_ActivityTypeBtn[i].SetActive(false);
        }

        if (_actionType == State.Expense.Food)
        {
            m_ActivityTypeBtn[0].SetActive(true);
        }
        else if (_actionType == State.Expense.Drink)
        {
            m_ActivityTypeBtn[1].SetActive(true);
        }
        else if (_actionType == State.Expense.Activity)
        {
            m_ActivityTypeBtn[2].SetActive(true);
        }

        for (int i = 0; i < ActionManager.Instance.activity[(int)_actionType].activity.Length; i++)
        {
            ActionElement newAtcion = Instantiate(m_ActionPrefab, m_ActionPanel.transform);
            newAtcion.SetAction(ActionManager.Instance.activity[(int)_actionType].activity[i],
                                (int)_actionType);
        }
    }

    public void OnClickCloseActionPanel()
    {
        for (int i = 0; i < m_ActionPanel.transform.childCount; i++)
        {
            m_ActionPanel.transform.GetChild(i).GetComponent<ActionElement>().DestroySelf();
        }
        m_ActivityPanel.SetActive(false);

        UpdateUI();
    }

    public void AdjustUISize()
    {
        
    }

    public void ToggleAllBtn(bool isTrue)
    {
        GameManager.Instance.ToggleEventSystem(isTrue);
    }

    public void ToggleDoBtn()
    {
        GameManager.Instance.ToggleEventSystem();
    }


    // ------------ Planner ------- //

    private int sleepTimeInHour;
    private int sleepHealth;
    private int sleepHappy;

    private int AllSetActionTime;
    private float AllSetActionHealth;
    private float AllSetActionHappy;
    private float AllSetActionCost;

    private int BSleepTime;
    private int BTravelTime;
    private int BActionTime;
    private int BWorkTime;
    private int BFreeTime;

    private int AllTime;
    private float AllHealth;
    private float AllHappy;
    
    public void UpdateSumUI()
    {
        AllSetActionTime = 0;
        AllSetActionHealth = 0;
        AllSetActionHappy = 0;
        AllSetActionCost = 0;

        for (int i = 0; i < ActionManager.Instance.activityPlanner.Count; i++)
        {
            if (ActionManager.Instance.activityPlanner[i].type != State.Expense.None
            && ActionManager.Instance.activityPlanner[i].time > 0)
            {
                AllSetActionTime += ActionManager.Instance.activityPlanner[i].timeCost * ActionManager.Instance.activityPlanner[i].time;
                AllSetActionHealth += ActionManager.Instance.activityPlanner[i].health * ActionManager.Instance.activityPlanner[i].time;
                AllSetActionHappy += ActionManager.Instance.activityPlanner[i].happiness * ActionManager.Instance.activityPlanner[i].time;
                AllSetActionCost += ActionManager.Instance.activityPlanner[i].moneyCost * ActionManager.Instance.activityPlanner[i].time;
            }
        }

        AllSetActionCost += (LocationManager.Instance.currenTravel.costPerUse);
        ActionManager.Instance.dailyCost = AllSetActionCost;

        AllTime = (sleepTimeInHour * CalManager.Instance.MIN_IN_HOUR) 
                + (m_PlayerManager.jobData.fullTimeJob.workTime)
                + (LocationManager.Instance.currentDistance * LocationManager.Instance.currenTravel.MIN_PER_KM) 
                + AllSetActionTime;
        AllHealth = (sleepHealth + AllSetActionHealth) + LocationManager.Instance.travelBy[LocationManager.Instance.locationData.currentTravelIndex].health;
        AllHappy = (sleepHappy + AllSetActionHappy) + LocationManager.Instance.travelBy[LocationManager.Instance.locationData.currentTravelIndex].happy;



        m_SumTimeText.text = CalManager.Instance.TextMinTime(AllTime, "HH2");
        m_SumHealthText.text = AllHealth.ToString();
        m_SumHappinessText.text = AllHappy.ToString();
        m_SumMoneyCostText.text = AllSetActionCost.ToString("N2") + " ฿";
        
        int workP = 0;
        float workHealth = AllHealth;
        float workHappy = AllHappy;
        for (int i = 0; i < m_PlayerManager.jobData.fullTimeJob.workTimeHour; i++)
        {
            if (workHealth >= m_PlayerManager.jobData.fullTimeJob.useHealth
            && workHappy >= m_PlayerManager.jobData.fullTimeJob.useHappy)
            {
                workHealth -= m_PlayerManager.jobData.fullTimeJob.useHealth;
                workHappy -= m_PlayerManager.jobData.fullTimeJob.useHappy;
                workP += m_PlayerManager.playerData.lvSkill.JobSpeed;
            }
        }
        m_WorkPriceText.text = workP.ToString("N0");

        m_ASleepTimeSlider.maxValue = CalManager.Instance.MIN_IN_DAY;
        m_AActionTimeSlider.maxValue = CalManager.Instance.MIN_IN_DAY;
        m_AWorkTimeSlider.maxValue = CalManager.Instance.MIN_IN_DAY;
        m_AFreeTimeSlider.maxValue = CalManager.Instance.MIN_IN_DAY;

        m_SleepTimeSlider.maxValue = CalManager.Instance.MIN_IN_DAY;
        m_ActionTimeSlider.maxValue = CalManager.Instance.MIN_IN_DAY;
        m_WorkTimeSlider.maxValue = CalManager.Instance.MIN_IN_DAY;
        m_FreeTimeSlider.maxValue = CalManager.Instance.MIN_IN_DAY;

        m_ASleepTimeSlider.value = (sleepTimeInHour * CalManager.Instance.MIN_IN_HOUR) + (LocationManager.Instance.currentDistance * LocationManager.Instance.currenTravel.MIN_PER_KM);
        m_AActionTimeSlider.value = AllSetActionTime;
        m_AWorkTimeSlider.value = m_PlayerManager.jobData.fullTimeJob.workTime;
        m_AFreeTimeSlider.value = CalManager.Instance.MIN_IN_DAY - AllTime;

        if (AllTime > CalManager.Instance.MIN_IN_DAY)
        {
            m_ASleepTimeSlider.fillRect.GetComponent<Image>().color = GameManager.Instance.red;
            m_AActionTimeSlider.fillRect.GetComponent<Image>().color = GameManager.Instance.red;
        }
        else
        {
            m_ASleepTimeSlider.fillRect.GetComponent<Image>().color = GameManager.Instance.sleepTime;
            m_AActionTimeSlider.fillRect.GetComponent<Image>().color = GameManager.Instance.actionTime;
        }

        float Cost = ActionManager.Instance.dailyCost * CalManager.Instance.DAY_IN_MONTH;
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element.Count; i++)
        {
            Cost += m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element[i].rentPrice;
            Cost += m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element[i].fixedCost;
        }
        for (int i = 0; i < m_PlayerManager.liabilityData.Liability.Length; i++)
        {
            for (int j = 0; j < m_PlayerManager.liabilityData.Liability[i].element.Count; j++)
            {
                Cost += (float)m_PlayerManager.liabilityData.Liability[i].element[j].installment;
            }
        }
        float Goal = Cost * CalManager.Instance.RETIRE_MONTH;

        float NetWorth = (float)m_PlayerManager.netWorth;
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element.Count; i++)
        {
            NetWorth -= m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element[i].price;
        }
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)State.AssetType.car].element.Count; i++)
        {
            NetWorth -= m_PlayerManager.assetData.Asset[(int)State.AssetType.car].element[i].price;
        }
        if (NetWorth < 0)
        {
            NetWorth = 0;
        }

        m_GoalSlider.maxValue = Goal;
        m_GoalSlider.value = NetWorth;
    }

    public void OnClickHomeBtn()
    {
        if (StateManager.Instance.currentState == State.GameState.Story_20_0)
        {
            StateManager.Instance.NextState();
        }

        m_SetActionPanel.SetActive(true);
        m_BottomAnim.SetTrigger("GoHome");
    }

    public void OnClickExitHospital()
    {
        m_PlayerManager.SubtractMoney(ActionManager.Instance.hospitalData.hospitalNetPrice, ()=>
                                    {
                                        
                                        if (ActionManager.Instance.hospitalData.currentHospital == State.Hospital.Accident)
                                        {
                                            m_PlayerManager.assetData.insurances[(int)State.Insurance.AccidentInsurance].element.protectionLimit -= ActionManager.Instance.hospitalData.hospitalInsurance;
                                        }
                                        else if (ActionManager.Instance.hospitalData.currentHospital == State.Hospital.Health)
                                        {
                                            m_PlayerManager.assetData.insurances[(int)State.Insurance.HealthInsurance].element.protectionLimit -= ActionManager.Instance.hospitalData.hospitalInsurance;
                                        }

                                        ActionManager.Instance.hospitalData.currentHospital = State.Hospital.None;
                                        ActionManager.Instance.hospitalData.hospitalPrice = 0;
                                        ActionManager.Instance.hospitalData.hospitalInsurance = 0;
                                        ActionManager.Instance.hospitalData.hospitalExitDate = 0;
                                        
                                        m_SetActionPanel.SetActive(true);
                                        m_BottomAnim.SetTrigger("GoingHome");
                                    });
    }

    public void OnClickChangePaymentBtn()
    {
        UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.ChangePayment);
    }

    public void OnClickGOBtn()
    {
        if (AllTime > CalManager.Instance.MIN_IN_DAY)
        {
            UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.TimeLimit);
        }
        else if (!ActionManager.Instance.useCredit && AllSetActionCost > m_PlayerManager.accountData.account[0].balance)
        {
            UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.MoneyLimit);
        }
        else if (ActionManager.Instance.useCredit && AllSetActionCost > m_PlayerManager.liabilityData.creditCard[0].enoughCredit)
        {
            UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.CreditLimit);
        }
        else
        {
            int maxOverdueLimit = 0;
            for (int i = 0; i < m_PlayerManager.billData.bill.Count; i++)
            {
                if (m_PlayerManager.billData.bill[i].overdue > maxOverdueLimit)
                {
                    maxOverdueLimit = m_PlayerManager.billData.bill[i].overdue;
                }
            }

            if (maxOverdueLimit >= BankManager.Instance.overdueLimit)
            {
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.OverdueLimit);
            }
            else
            {
                UpdateSleepTimeValue();
                m_SetActionPanel.SetActive(false);
                CalManager.Instance.HospitalEvent();

                if (ActionManager.Instance.hospitalData.currentHospital != State.Hospital.None)
                {
                    m_HospitalCostText.text = ActionManager.Instance.hospitalData.hospitalPrice.ToString("N0") + " ฿";
                    m_HospitalInsuranceText.text = ActionManager.Instance.hospitalData.hospitalInsurance.ToString("N0") + " ฿";
                    m_HospitalTotalText.text = ActionManager.Instance.hospitalData.hospitalNetPrice.ToString("N0") + " ฿";

                    StartCoroutine(GoingHospital());
                }
                else
                {
                    StartCoroutine(Doing());
                }
            }
        }
    }

    public void OnClickPartTimeBtn()
    {
        JobManager.Instance.WorkPartTime();
        UpdateUI();
    }

    public void OnClickLearnSkillBtn()
    {
        JobManager.Instance.LearnSkill();
        UpdateUI();
    }

    public void SetSliderPhaseB()
    {
        m_SleepTimeSlider.value = (sleepTimeInHour * CalManager.Instance.MIN_IN_HOUR) + (LocationManager.Instance.currentDistance * LocationManager.Instance.currenTravel.MIN_PER_KM);
        m_ActionTimeSlider.value = AllSetActionTime;
        m_WorkTimeSlider.value = m_PlayerManager.jobData.fullTimeJob.workTime;
        m_FreeTimeSlider.value = CalManager.Instance.MIN_IN_DAY - AllTime;
    }

    public IEnumerator DoingSlider(Slider _Slider, float _ChangeValue, float _Second)
    {
        float oldValue = _Slider.value;
        float newValue = _Slider.value;
        float timer = 0;
        while (timer < _Second)
        {
            timer += Time.deltaTime;
            newValue = oldValue - ((_ChangeValue / _Second) * timer);

            _Slider.value = newValue;
            yield return null;
        }
        _Slider.value = (oldValue - _ChangeValue);
        yield return null;

        // worth = oldWorth + (((m_PlayerManager.netWorth - oldWorth) / 0.5f) * (worthTimimg));
    }
    
    public IEnumerator Doing()
    {
        if (StateManager.Instance.currentState == State.GameState.Story_15_0)
        {
            StateManager.Instance.NextState();
        }

        StateManager.Instance.SwitchDayState(State.DayState.DailyEvent);
        m_BottomAnim.SetTrigger("GoWork");
        ToggleAllBtn(false);
        SetSliderPhaseB();
        yield return new WaitForSeconds(1f);

        PlayerManager.Instance.SleepTime();
        StartCoroutine(DoingSlider(m_SleepTimeSlider, 
                                    sleepTimeInHour * CalManager.Instance.MIN_IN_HOUR, 
                                    0.4f));
        yield return new WaitForSeconds(0.42f);

        for (int i = 0; i < ActionManager.Instance.activityPlanner.Count; i++)
        {
            if (ActionManager.Instance.activityPlanner[i].type != State.Expense.None
            && ActionManager.Instance.activityPlanner[i].time > 0)
            {
                // Debug.Log(ActionManager.Instance.activityPlanner[i].activityType);
                ActionManager.Instance.DoingAction(ActionManager.Instance.activityPlanner[i].activityType, ActionManager.Instance.activityPlanner[i].type, ActionManager.Instance.activityPlanner[i].time);
                StartCoroutine(DoingSlider(m_ActionTimeSlider, 
                                            ActionManager.Instance.activityPlanner[i].timeCost * ActionManager.Instance.activityPlanner[i].time, 
                                            0.08f));
                yield return new WaitForSeconds(0.1f);
            }
        }

        if (!PlayerManager.Instance.haveFullTime() || CalManager.Instance.isDayOff)
        {
            yield return new WaitForSeconds(0.05f);
            StateManager.Instance.SwitchDayState(State.DayState.Day);
        }
        else
        {
            yield return new WaitForSeconds(0.05f);
            StateManager.Instance.SwitchDayState(State.DayState.Day);

            // JobManager.Instance.WorkFullTime();
            JobManager.Instance.UpWorkExp((int)m_PlayerManager.jobData.fullTimeJob.type, m_PlayerManager.jobData.fullTimeJob.workTimeHour);

            LocationManager.Instance.NextTo((int)LocationManager.Instance.locationData.currentJobLo);
            StartCoroutine(DoingSlider(m_SleepTimeSlider, 
                                            LocationManager.Instance.currentDistance * LocationManager.Instance.currenTravel.MIN_PER_KM, 
                                            0.3f));
            yield return new WaitForSeconds(0.32f);

            m_PlayerManager.SubtractTime(m_PlayerManager.jobData.fullTimeJob.workTime);
            m_PlayerManager.jobData.fullTimeJob.dailyWorked = 0;
            m_PlayerManager.jobData.fullTimeJob.dailyFinish = 0;

            for (int i = 0; i < m_PlayerManager.jobData.fullTimeJob.workTimeHour; i ++)
            {
                ActionManager.Instance.DoAction("WorkNormal", State.Expense.Work);
                StartCoroutine(DoingSlider(m_WorkTimeSlider, 
                                            CalManager.Instance.MIN_IN_HOUR, 
                                            0.08f));
                yield return new WaitForSeconds(0.1f);
            }
        }

        yield return new WaitForSeconds(0.1f);

        StateManager.Instance.SwitchDayState(State.DayState.DayEnd);
        ToggleAllBtn(true);
        
        if (StateManager.Instance.currentState == State.GameState.Story_16_0)
        {
            // StateManager.Instance.NextState();
        }
    }
    
    public IEnumerator GoingHospital()
    {
        StateManager.Instance.SwitchDayState(State.DayState.DailyEvent);
        m_BottomAnim.SetTrigger("GoingHospital");
        ToggleAllBtn(false);
        SetSliderPhaseB();
        yield return new WaitForSeconds(1f);

        PlayerManager.Instance.SleepTime();
        StartCoroutine(DoingSlider(m_SleepTimeSlider, 
                                    sleepTimeInHour * CalManager.Instance.MIN_IN_HOUR, 
                                    0.4f));

        yield return new WaitForSeconds(0.5f);

        StateManager.Instance.SwitchDayState(State.DayState.DayEnd);
        UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.Hospital);
        ToggleAllBtn(true);
    }

    private void UpdateSleepTimeValue()
    {
        sleepHealth = sleepTimeInHour * 5;
        sleepHappy = sleepTimeInHour * 4;
        m_PlayerManager.UpdateSleepTime(sleepTimeInHour);
    }

    private void UpdateSleepTimeUI()
    {
        UpdateSleepTimeValue();
        // m_SleepTimeText.text = CalManager.Instance.TextMinTime(m_PlayerManager.playerData.sleepTimeInHour * CalManager.Instance.MIN_IN_HOUR, "HH");
        sleepTimeInHour = m_PlayerManager.playerData.sleepTimeInHour;
        m_SleepTimeText.text = (sleepTimeInHour % 10).ToString("0");
        m_SleepTime2Text.text = Mathf.Floor(sleepTimeInHour / 10).ToString("0");
        m_SleepHealthText.text = sleepHealth.ToString("+0;-#");
        m_SleepHappyText.text = sleepHappy.ToString("+0;-#");
        m_ActionPlannerParent.GetChild(0).GetComponent<ActionPlanElement>().UpdateUI();
    }


    public void OnClickChangeSleepTime(int _Value)
    {   
        sleepTimeInHour += _Value;
        if (sleepTimeInHour > 18)
        {
            sleepTimeInHour = 18;
        }
        else if (sleepTimeInHour < 1)
        {
            sleepTimeInHour = 1;
        }
        

        if (StateManager.Instance.currentState == State.GameState.Story_10_0
        && sleepTimeInHour == 8)
        {
            StateManager.Instance.NextState();
        }

        UpdateSleepTimeUI();
        UpdateSumUI();
    }
    
    public void OnClickChangeActionTime(int _Value)
    {
        ActionManager.Instance.activityPlanner[setActionIndex].time += _Value;
        if (ActionManager.Instance.activityPlanner[setActionIndex].time > 10)
        {
            ActionManager.Instance.activityPlanner[setActionIndex].time = 10;
        }
        else if (ActionManager.Instance.activityPlanner[setActionIndex].time < 0)
        {
            ActionManager.Instance.activityPlanner[setActionIndex].time = 0;
        }
        UpdateSetAction(setActionIndex);
        m_ActionPlannerParent.GetChild(setActionIndex + 1).GetComponent<ActionPlanElement>().UpdateUI();
        UpdateSumUI();
    }

    public void UpdateSetActionZone()
    {
        DestroyPlannerPrefab();
        int Index = 0;
        for (int i = 0; i < ActionManager.Instance.activityPlanner.Count; i++)
        {
            Index = i + 1;
            ActionPlanElement newObj;
            newObj = Instantiate(actionPlannerPrefab, m_ActionPlannerParent);
            newObj.index = i;
            newObj.uiPage = this;
            newObj.UpdateUI();
        }

        for (int i = Index; i < ActionManager.Instance.MaxActivityPlanner; i++)
        {
            ActionPlanElement newObj;
            newObj = Instantiate(actionPlannerPrefab, m_ActionPlannerParent);
            newObj.index = (-1 * i) - 1;
            newObj.uiPage = this;
            newObj.UpdateUI();
        }
    }

    private void DestroyPlannerPrefab()
    {
        for (int i = 2; i < m_ActionPlannerParent.childCount; i++)
        {
            m_ActionPlannerParent.GetChild(i).GetComponent<ActionPlanElement>().DestroySelf();
        }
    }

    public void OnClickDeleteAction()
    {
        ActionManager.Instance.activityPlanner.RemoveAt(setActionIndex);
        setActionIndex = -1;

        UpdateSetActionZone();
        OnClickCloseAll();
    }

    public void OnClickCloseAll()
    {
        m_PopupAction.SetActive(false);

        m_SetSleep.SetActive(false);
        m_SetAction.SetActive(false);
        m_SelectAction.SetActive(false);

        UpdateUI();

        if (StateManager.Instance.currentState == State.GameState.Story_10_1)
        {
            StateManager.Instance.SwitchState(State.GameState.Story_10_2);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_12_1)
        {
            StateManager.Instance.NextState();
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_13_1)
        {
            StateManager.Instance.NextState();
        }
    }

    public void OpenSetSleep()
    {
        m_PopupAction.SetActive(true);

        m_SetSleep.SetActive(true);
        m_SetAction.SetActive(false);
        m_SelectAction.SetActive(false);

        if (StateManager.Instance.currentState == State.GameState.Story_9_0)
        {
            StateManager.Instance.NextState();
        }
    }

    public void OpenSetAction()
    {
        m_PopupAction.SetActive(true);

        m_SetSleep.SetActive(false);
        m_SetAction.SetActive(true);
        m_SelectAction.SetActive(true);
        OnClickSetActionPanel(ActionManager.Instance.activityPlanner[setActionIndex].type.ToString());
    }

    public void OpenSelectAction(int _Index)
    {
        m_PopupAction.SetActive(true);

        setActionIndex = _Index;
        m_SetSleep.SetActive(false);
        m_SetAction.SetActive(false);
        m_SelectAction.SetActive(true);
        OnClickSetActionPanel("Food");
    }

    public void UpdateSetAction(int _Index)
    {
        setActionIndex = _Index;
        int setHour = ActionManager.Instance.activityPlanner[setActionIndex].timeCost * ActionManager.Instance.activityPlanner[setActionIndex].time;
        float setHealth = ActionManager.Instance.activityPlanner[setActionIndex].health * ActionManager.Instance.activityPlanner[setActionIndex].time;
        float setHappy = ActionManager.Instance.activityPlanner[setActionIndex].happiness * ActionManager.Instance.activityPlanner[setActionIndex].time;
        float setCash = ActionManager.Instance.activityPlanner[setActionIndex].moneyCost * ActionManager.Instance.activityPlanner[setActionIndex].time;


        if (ActionManager.Instance.activityPlanner[setActionIndex].activityImage)
        {
            m_ActionImage.sprite = ActionManager.Instance.activityPlanner[setActionIndex].activityImage;
        }
        m_SetActionNameText.text = ActionManager.Instance.activityPlanner[setActionIndex].activity;
        
        m_SetActionTimeText.text = ActionManager.Instance.activityPlanner[setActionIndex].time.ToString("N0");
        m_SetActionTimerText.text = CalManager.Instance.TextMinTime(setHour, "HH");
        m_SetActionUseHealthText.text = setHealth.ToString("+0;-#");
        m_SetActionUseHappyText.text = setHappy.ToString("+0;-#");
        m_SetActionCashText.text = setCash.ToString("N2") + " ฿";
    }
    
    public void OnClickSetActionPanel(string _ActionType)
    {
        State.Expense _actionType = (State.Expense)Enum.Parse(typeof(State.Expense), _ActionType);

        for (int i = 0; i < m_ActionTypeBtn.Length; i++)
        {
            m_ActionTypeBtn[i].SetActive(false);
        }

        if (_actionType == State.Expense.Food)
        {
            m_ActionTypeBtn[0].SetActive(true);
        }
        else if (_actionType == State.Expense.Drink)
        {
            m_ActionTypeBtn[1].SetActive(true);
        }
        else if (_actionType == State.Expense.Media)
        {
            m_ActionTypeBtn[2].SetActive(true);
        }

        for (int i = 0; i < m_SetActionPanel.transform.childCount; i++)
        {
            m_SetActionPanel.transform.GetChild(i).GetComponent<ActionElement>().DestroySelf();
        }

        for (int i = 0; i < ActionManager.Instance.activity[(int)_actionType].activity.Length; i++)
        {
            int _Index = i;
            ActionElement newAtcion = Instantiate(m_SetActionPrefab, m_SetActionPanel.transform);

            newAtcion.SetActionSet(ActionManager.Instance.activity[(int)_actionType].activity[_Index],
                                (int)_actionType, ()=>
                                {
                                    int Jndex = -1;
                                    bool isAgain = false;
                                    for (int j = 0; j < ActionManager.Instance.activityPlanner.Count; j++)
                                    {
                                        Jndex = j;
                                        if (ActionManager.Instance.activityPlanner[Jndex].type == _actionType &&
                                        ActionManager.Instance.activityPlanner[Jndex].activityType == ActionManager.Instance.activity[(int)_actionType].activity[_Index].activityType)
                                        {
                                            isAgain = true;
                                            break;
                                        }
                                    }

                                    if (isAgain)
                                    {
                                        setActionIndex = Jndex;
                                        UpdateSetAction(setActionIndex);
                                        OpenSetAction();
                                        UpdateUI();
                                    }
                                    else
                                    {
                                        if (setActionIndex >= 0)
                                        {
                                            ActivityPlanner setAction = ActionManager.Instance.ConvertActivity(ActionManager.Instance.activity[(int)_actionType].activity[_Index].activityType, _actionType);
                                            ActionManager.Instance.activityPlanner[setActionIndex] = setAction;
                                        }
                                        else
                                        {
                                            ActivityPlanner setAction = ActionManager.Instance.ConvertActivity(ActionManager.Instance.activity[(int)_actionType].activity[_Index].activityType, _actionType);
                                            ActionManager.Instance.activityPlanner.Add(setAction);
                                            setActionIndex = ActionManager.Instance.activityPlanner.Count - 1;
                                        }
                                        UpdateSetAction(setActionIndex);
                                        OpenSetAction();
                                        UpdateUI();
                                    }
                                });
        }
    }
}
