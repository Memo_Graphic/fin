﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIEstatePage : UIElement
{
    [Header("TMP - Info Popup")]
    [SerializeField] private TextMeshProUGUI m_EstateName;
    [SerializeField] private TextMeshProUGUI m_EstateDistance;
    [SerializeField] private TextMeshProUGUI m_EstateCool;
    [SerializeField] private TextMeshProUGUI m_EstatePrice;
    [SerializeField] private TextMeshProUGUI m_EstateFixedCost;
    [SerializeField] private TextMeshProUGUI m_EstateHint;
    [SerializeField] private TextMeshProUGUI m_EstateHint2;
    [SerializeField] private TextMeshProUGUI m_EstateBuy;

    [Header("TMP - Buy")]
    [SerializeField] private TextMeshProUGUI m_BuyEstateName;
    [SerializeField] private TextMeshProUGUI m_BuyEstateDistance;
    [SerializeField] private TextMeshProUGUI m_BuyEstateFixedCost;
    [SerializeField] private TextMeshProUGUI m_BuyEstatePrice;
    [SerializeField] private TextMeshProUGUI m_buyEstateLoanPrice;
    [SerializeField] private TextMeshProUGUI m_buyEstateDownPrice;
    [SerializeField] private TextMeshProUGUI m_buyEstateIR;
    [SerializeField] private TextMeshProUGUI m_buyEstateYear;
    [SerializeField] private TextMeshProUGUI m_buyEstateLoanAmount;
    [SerializeField] private TextMeshProUGUI m_buyEstateInstallment;
    [SerializeField] private TextMeshProUGUI m_buyEstateLoanSum;
    [SerializeField] private TextMeshProUGUI m_buyEstateSum;
    [SerializeField] private TextMeshProUGUI m_buyEstateAllSum;

    [Header("TMP - Own")]
    [SerializeField] private TextMeshProUGUI m_OwnEstateName;
    [SerializeField] private TextMeshProUGUI m_OwnEstateDistance;
    [SerializeField] private TextMeshProUGUI m_OwnEstateFixedCost;
    [SerializeField] private TextMeshProUGUI m_OwnEstateSellPrice;
    [SerializeField] private TextMeshProUGUI m_OwnEstateCancleRent;
    [SerializeField] private TextMeshProUGUI m_CurrentLoanText;
    [SerializeField] private TextMeshProUGUI m_MaxLoanText;
    [SerializeField] private TextMeshProUGUI m_TermText;
    [SerializeField] private TextMeshProUGUI m_InstallmentText;
    [SerializeField] private TextMeshProUGUI m_InterestText;
    [SerializeField] private Slider m_LoanSlider;
    
    [Header("Image")]
    [SerializeField] private Image m_HomeImage;
    [SerializeField] private Image m_BuyHomeImage;
    [SerializeField] private Image m_OwnHomeImage;

    [Header("Buttons")]
    [SerializeField] private Button m_BuyBtn;
    [SerializeField] private Button m_UseBtn;
    [SerializeField] private Button m_SellBtn;
    [SerializeField] private Button m_ConfirmBuyBtn;
    [SerializeField] private Button m_CencelRentBtn;

    [Header("GameObjects")]
    [SerializeField] private GameObject[] m_PerBtn;
    [SerializeField] private GameObject m_BuyPanel;
    [SerializeField] private GameObject m_IsOwnPanel;
    [SerializeField] private GameObject m_NotOwnPanel;
    [SerializeField] private GameObject m_BackBtn;

    [Header("Prefabs")]
    [SerializeField] private LocationElement locationPrefab;
    [SerializeField] private Transform m_LocationParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;

    private PlayerManager m_PlayerManager;
    private Asset newEstate;
    private Location newLocation;
    private float downPercentage;
    private float downPrice;
    private float loanAmount;
    private float ir;
    private float year;
    private float installment;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {

    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroyLocationPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(UIManager.Instance.previusState);
                                                });
    }

    public override void UpdateUI()
    {
        m_BackBtn.SetActive(!GameManager.Instance.isTutorial);
        DestroyLocationPrefab();
        CreateLocationElement();
    }
    
    public void UpdateInfo(Location _Location)
    {
        newLocation = _Location;
        newEstate = AssetManager.Instance.FindAsset(newLocation.locationType, State.AssetType.estate);

        if (PlayerManager.Instance.haveThisAsset(newLocation.locationType, State.AssetType.estate))
        {
            m_IsOwnPanel.SetActive(true);
            m_NotOwnPanel.SetActive(false);

            UpdateOwnedUI();
        }
        else
        {
            m_IsOwnPanel.SetActive(false);
            m_NotOwnPanel.SetActive(true);

            UpdatePopUp();
        }
    }

    public void UpdatePopUp()
    {
        m_HomeImage.sprite = newLocation.locationImage;

        m_EstateName.text = newEstate.assetName;
        m_EstateDistance.text = LocationManager.Instance.JobDistance(newEstate.location).ToString("N0") + " กม.";
        m_EstateFixedCost.text = newEstate.fixedCost.ToString("N0") + " ฿";
        m_EstateCool.text = newEstate.cool.ToString("N0");
        // m_EstateHint.text = "ผ่อนเริ่มต้น " + "฿/เดือน";
        // m_EstateHint2.text = "อัตราเอกเบี้ย " + BankManager.Instance.currentIR.ToString() + "%/ปี";

        if (newEstate.price > 0)
        {
            m_EstateBuy.text = "ขอสินเชื่อ";
            m_EstatePrice.text = newEstate.price.ToString("N0") + " ฿";
        }
        else 
        {
            m_EstateBuy.text = "เช่า";
            m_EstatePrice.text = newEstate.rentPrice.ToString("N0") + " ฿";
        }

        m_CencelRentBtn.interactable = !GameManager.Instance.isTutorial;
    }

    public void UpdateBuyUI()
    {
        downPrice = newEstate.price * downPercentage / 100f;
        loanAmount = newEstate.price - downPrice;
        installment = loanAmount / ((1f - (1f / Mathf.Pow(1 + (ir / 100f / CalManager.Instance.MONTH_IN_YEAR), year * CalManager.Instance.MONTH_IN_YEAR))) / (ir / 100f / CalManager.Instance.MONTH_IN_YEAR));
        float Allsum = installment * year * CalManager.Instance.MONTH_IN_YEAR;
        
        m_BuyEstateName.text = newEstate.assetName;
        m_BuyEstateDistance.text = LocationManager.Instance.JobDistance(newEstate.location).ToString("N0") + " กม.";
        m_BuyEstateFixedCost.text = newEstate.fixedCost.ToString("N0") + " ฿";
        m_BuyEstatePrice.text = newEstate.price.ToString("N0") + " ฿";

        m_buyEstateDownPrice.text = downPrice.ToString("N0") + " ฿";
        m_buyEstateLoanAmount.text = loanAmount.ToString("N0") + " ฿";

        m_buyEstateYear.text = year.ToString("N0") + " ปี";
        m_buyEstateInstallment.text = installment.ToString("N0") + " ฿";

        m_buyEstateLoanPrice.text = downPrice.ToString("N0") + " ฿";

        m_buyEstateIR.text = ir.ToString("N2") + "%/ปี";
        m_buyEstateLoanSum.text = loanAmount.ToString("N0") + " ฿";
        m_buyEstateSum.text = (Allsum - loanAmount).ToString("N0") + " ฿";
        m_buyEstateAllSum.text = Allsum.ToString("N0") + " ฿";

        m_BuyHomeImage.sprite = newLocation.locationImage;

        m_ConfirmBuyBtn.interactable = PlayerManager.Instance.accountData.account[0].balance >= downPrice;
    }

    public void UpdateOwnedUI()
    {
        int _Index = PlayerManager.Instance.assetIndex(newLocation.locationType, State.AssetType.estate);
        PlayerAssetElement _Asset = PlayerManager.Instance.assetData.Asset[(int)State.AssetType.estate].element[_Index];

        m_OwnEstateName.text = newEstate.assetName;
        m_OwnEstateDistance.text = LocationManager.Instance.JobDistance(newEstate.location).ToString("N0") + " กม.";
        m_OwnEstateFixedCost.text = newEstate.fixedCost.ToString("N0") + " ฿";

        m_OwnHomeImage.sprite = newLocation.locationImage;

        if (_Asset.isRent)
        {
            // เช่าอยู่
            m_OwnEstateCancleRent.text = "ยกเลิกสัญญา";
            m_OwnEstateSellPrice.text = newEstate.rentPrice.ToString("N0") + " ฿";
            m_SellBtn.interactable = !_Asset.isUsing;
            m_UseBtn.interactable = !_Asset.isUsing && newLocation.type != State.Location.Land;

            m_CurrentLoanText.text = "N/A";
            m_MaxLoanText.text = "N/A";
            m_TermText.text = "(N/A)";
            
            m_InstallmentText.text = "N/A";
            m_InterestText.text = "N/A";
        }
        else
        {
            // เป็นเจ้าของ
            m_OwnEstateCancleRent.text = "ขาย";
            m_OwnEstateSellPrice.text = newEstate.price.ToString("N0") + " ฿";
            m_SellBtn.interactable = !_Asset.isUsing;
            m_UseBtn.interactable = !_Asset.isUsing && newLocation.type != State.Location.Land;

            int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element.FindIndex(item => item.deptID == _Asset.deptID);
            PlayerLiabilityElement newDept = PlayerManager.Instance.liabilityData.Liability[(int)State.LiabilityType.Loan].element[_index];
            m_CurrentLoanText.text = newDept.loanAmount.ToString("N0") + " ฿";
            m_MaxLoanText.text = "/" + newDept.maxAmount.ToString("N0") + " ฿";
            m_TermText.text = "(" + (newDept.maxTerm - newDept.term + 1).ToString("N0") + "/" + newDept.maxTerm.ToString("N0") + ")";
            
            float loanAmount = Mathf.RoundToInt((float)(decimal)newDept.loanAmount * 100f) / 100f;
            if (newDept.term > 1)
            {
                loanAmount = Mathf.RoundToInt((float)(decimal)newDept.loanAmount * 100f) / 100f;
            }
            float interest = loanAmount * (newDept.irPerMonth);

            m_InstallmentText.text = newDept.installment.ToString("N0") + " ฿";
            m_InterestText.text = interest.ToString("N0") + " ฿";

            m_LoanSlider.maxValue = (float)newDept.maxAmount;
            m_LoanSlider.maxValue = (float)newDept.loanAmount;
        }
    }

    private void CreateLocationElement()
    {
        for (int i = 1; i < LocationManager.Instance.location.Count; i++)
        {
            LocationElement newObj = Instantiate(locationPrefab, m_LocationParent);
            newObj.SetProperty(i);
        }
    }

    private void DestroyLocationPrefab()
    {
        for (int i = 0; i < m_LocationParent.childCount; i++)
        {
            m_LocationParent.GetChild(i).GetComponent<LocationElement>().DestroySelf();
        }
    }

    public void OnClickFadeBtn()
    {
        Hide();
    }

    public void OnClickBuyBtn()
    {
        m_NotOwnPanel.SetActive(false);

        if (newEstate.price > 0)
        {
            m_BuyPanel.SetActive(true);

            year = 20;
            OnClickChangeDownPer(20);
            ir = BankManager.Instance.currentIR;
        }
        else 
        {
            OnClickRentBtn();
        }

        UpdateBuyUI();
    }

    public void OnClickRentBtn()
    {
        if (!GameManager.Instance.isTutorial)
        {
            UIManager.Instance.Show(State.UIState.PaymentPage);
            UIPaymentPage patmentPage = UIManager.Instance.uIElement[(int)State.UIState.PaymentPage].GetComponent<UIPaymentPage>();
            patmentPage.SetPayment(newEstate.rentPrice, ()=>
                                {
                                    AssetManager.Instance.RentEstate(newEstate);
                                    AssetManager.Instance.UseAsset(newEstate.assetType, State.AssetType.estate);

                                    PlayerBalanceElement balance = new PlayerBalanceElement();
                                    balance.balanceName = newEstate.assetName;
                                    balance.amount = newEstate.rentPrice;
                                    m_PlayerManager.RecordBalance(balance, State.Expense.Home);
                                }, ()=>
                                {
                                    
                                });
        }
        else
        {
            AssetManager.Instance.RentEstate(newEstate);
            AssetManager.Instance.UseAsset(newEstate.assetType, State.AssetType.estate);
            UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopupAsync(State.PopupPage.Saving);
            Hide();
            UIManager.Instance.uIElement[(int)State.UIState.TutorialPage].GetComponent<UITutorialPage>().SetArrow(false);
        }
    }

    public void OnClickSellBtn()
    {
        int _Index = PlayerManager.Instance.assetIndex(newLocation.locationType, State.AssetType.estate);
        PlayerAssetElement _Asset = PlayerManager.Instance.assetData.Asset[(int)State.AssetType.estate].element[_Index];
        if (_Asset.isRent)
        {
            // ยกเลิกสัญญาเช่า
            AssetManager.Instance.CancelRentAsset(newEstate.assetType, State.AssetType.estate);
        }
        else
        {
            // ขาย
            AssetManager.Instance.SellAsset(newEstate.assetType, 
                                        State.AssetType.estate, 
                                        PlayerManager.Instance.accountData.account[0].accountID);
        }

        m_IsOwnPanel.SetActive(false);
        m_NotOwnPanel.SetActive(false);
        DestroyLocationPrefab();
        CreateLocationElement();
    }

    public void OnClickUseBtn()
    {
        Asset oldEstate = AssetManager.Instance.FindAsset(LocationManager.Instance.location[LocationManager.Instance.locationData.currentHomeLo].locationType, State.AssetType.estate);
        AssetManager.Instance.UnUseAsset(oldEstate.assetType, State.AssetType.estate);
        AssetManager.Instance.UseAsset(newEstate.assetType, State.AssetType.estate);
        UpdateInfo(newLocation);
        DestroyLocationPrefab();
        CreateLocationElement();
    }

    public void OnClickCancelBuyBtn()
    {
        m_BuyPanel.SetActive(false);
    }

    public void OnClickChangeDownPer(float _Value)
    {   
        downPercentage = _Value;
        if (downPercentage > 100)
        {
            downPercentage = 100;
        }
        else if (downPercentage < 0)
        {
            downPercentage = 0;
        }

        for (int i = 0; i < m_PerBtn.Length; i++)
        {
            m_PerBtn[i].SetActive(false);
        }

        m_PerBtn[(int)(downPercentage / 10)].SetActive(true);
        UpdateBuyUI();
    }

    public void OnClickChangeYear(float _Value)
    {   
        year += _Value;
        if (year > 30)
        {
            year = 30;
        }
        else if (year < 5)
        {
            year = 5;
        }
        UpdateBuyUI();
    }

    public void OnClickConfirmBuyBtn()
    {
        AssetManager.Instance.BuyEstate(newEstate, downPercentage, year);

        m_BuyPanel.SetActive(false);
        UpdateInfo(newLocation);
        DestroyLocationPrefab();
        CreateLocationElement();
    }
}
