﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIMallPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_TitleText;

    [Header("Prefabs")]
    [SerializeField] private ActionElement m_MallPrefab;
    [SerializeField] private Transform m_MallParent;

    [Header("ETC")]
    public State.Location locoState = State.Location.None;
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private List<Activity> actions;
    private List<Asset> assets;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroyElementPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(UIManager.Instance.previusState);
                                                });
    }

    public override void UpdateUI()
    {
        UpdateStore(locoState);
    }

    public void UpdateStore(State.Location _Type, string[] _Action = null)
    {
        actions = new List<Activity>();
        DestroyElementPrefab();
        if (_Type != State.Location.None)
        {
            for (int i = 0; i < _Action.Length; i++)
            {
                for (int j = 0; j < ActionManager.Instance.activity[(int)_Type].activity.Length; j++)
                {
                    if (_Action[i] == ActionManager.Instance.activity[(int)_Type].activity[j].activityType)
                    {
                        Activity newAction = ActionManager.Instance.activity[(int)_Type].activity[j];
                        actions.Add(newAction);
                        break;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < ActionManager.Instance.activity[(int)State.Location.Food].activity.Length; i++)
            {
                Activity newAction = ActionManager.Instance.activity[(int)State.Location.Food].activity[i];
                actions.Add(newAction);
            }
            for (int i = 0; i < ActionManager.Instance.activity[(int)State.Location.Store].activity.Length; i++)
            {
                Activity newAction = ActionManager.Instance.activity[(int)State.Location.Store].activity[i];
                actions.Add(newAction);
            }
        }

        if (actions != null)
        {
            for (int i = 0; i < actions.Count; i ++)
            {
                ActionElement newObj;
                newObj = Instantiate(m_MallPrefab, m_MallParent);
                newObj.SetAction(actions[i], (int)_Type);
            }
        }
    }

    private void DestroyElementPrefab()
    {
        for (int i = 0; i < m_MallParent.childCount; i++)
        {
            m_MallParent.GetChild(i).GetComponent<ActionElement>().DestroySelf();
        }
    }

    public void OnClickFadeBtn()
    {
        Hide();
    }
}
