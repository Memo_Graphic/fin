﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIPlannerPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_TravelTimeText;
    [SerializeField] private TextMeshProUGUI m_SleepTimeText;
    [SerializeField] private TextMeshProUGUI m_SleepUseText;
    [SerializeField] private TextMeshProUGUI m_WorkTimeText;
    [SerializeField] private TextMeshProUGUI m_WorkUseText;
    [SerializeField] private TextMeshProUGUI m_PartTimeTimeText;
    [SerializeField] private TextMeshProUGUI m_PartTimeUseText;
    [SerializeField] private TextMeshProUGUI m_EatTimeText;
    [SerializeField] private TextMeshProUGUI m_EatUseText;
    [SerializeField] private TextMeshProUGUI m_HomeNameText;
    [SerializeField] private TextMeshProUGUI m_HomeDistanceText;
    [SerializeField] private TextMeshProUGUI m_TravelNameText;
    [SerializeField] private TextMeshProUGUI m_TravelSpeedText;
    [SerializeField] private TextMeshProUGUI m_HealthText;
    [SerializeField] private TextMeshProUGUI m_HappyText;

    [SerializeField] private TextMeshProUGUI m_AllTimeText;
    [SerializeField] private TextMeshProUGUI m_AllHealtText;
    [SerializeField] private TextMeshProUGUI m_AllHappyText;


    [SerializeField] private TextMeshProUGUI m_SetActionNameText;
    [SerializeField] private TextMeshProUGUI m_SetActionTimeText;
    [SerializeField] private TextMeshProUGUI m_SetActionUseText;
    [SerializeField] private TextMeshProUGUI m_SetActionCashText;

    [Header("Image")]
    [SerializeField] private Image m_ActionImage;
    [SerializeField] private Image m_HomeImage;
    [SerializeField] private Image m_TravelImage;

    [Header("Slider")]
    [SerializeField] private Slider m_HappinessSlider;
    [SerializeField] private Slider m_HealthSlider;

    [Header("Button")]
    [SerializeField] private Button m_DoBtn;

    [Header("Prefabs")]
    [SerializeField] private GameObject m_SetSleep;
    [SerializeField] private GameObject m_SetAction;
    [SerializeField] private GameObject m_SelectAction;

    [Header("Prefabs")]
    [SerializeField] private PlannerElement plannerPrefab;
    [SerializeField] private Transform m_PlannerParent;

    [SerializeField] private ActionPlanElement actionPlannerPrefab;
    [SerializeField] private Transform m_ActionPlannerParent;

    [SerializeField] private ActionElement m_ActionPrefab;
    [SerializeField] private Transform m_ActionPanel;
    

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;
    private int sleepTimeInHour;
    private int sleepHealth;
    private int sleepHappy;

    private int workTimeInHour;
    private int workHealth;
    private int workHappy;

    private int partTimeInHour;
    private int partTimeHealth;
    private int partTimeHappy;

    public int setActionIndex;

    float AllTime;
    float AllHealth;
    float AllHappy;

    float AllSetActionTime;
    float AllSetActionHealth;
    float AllSetActionHappy;

    public float PartTimeIncome()
    {
        return partTimeInHour * 100f;
    }

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
        workTimeInHour = 8;
        workHealth = workTimeInHour * 15;
        workHappy = workTimeInHour * 10;

        partTimeInHour = 2;
        partTimeHealth = 0;
        partTimeHappy = 0;

        sleepTimeInHour = m_PlayerManager.playerData.sleepTimeInHour;
        sleepHealth = sleepTimeInHour * 5;
        sleepHappy = sleepTimeInHour * 4;

        UpdateUI();
        SetCloseAll();
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        StopCoroutine("Doing");
        sleepTimeInHour = m_PlayerManager.playerData.sleepTimeInHour;
        sleepHealth = sleepTimeInHour * 5;
        sleepHappy = sleepTimeInHour * 4;
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0f,
                                                0f,
                                                () =>
                                                {
                                                    // gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(State.UIState.MainPage);
                                                });
    }

    public override void UpdateUI()
    {
        UpdateSetActionZone();

        UpdateSleepTimeUI();
        UpdatePartTimeUI();
        UpdateWorkTimeUI();
        UpdateHomeUI();

        AllSetActionTime = 0;
        AllSetActionHealth = 0;
        AllSetActionHappy = 0;

        for (int i = 0; i < ActionManager.Instance.activityPlanner.Count; i++)
        {
            if (ActionManager.Instance.activityPlanner[i].type != State.Expense.None
            && ActionManager.Instance.activityPlanner[i].time > 0)
            {
                AllSetActionTime += ActionManager.Instance.activityPlanner[i].timeCost * ActionManager.Instance.activityPlanner[i].time;
                AllSetActionHealth += ActionManager.Instance.activityPlanner[i].health * ActionManager.Instance.activityPlanner[i].time;
                AllSetActionHappy += ActionManager.Instance.activityPlanner[i].happiness * ActionManager.Instance.activityPlanner[i].time;
            }
        }

        AllTime = (sleepTimeInHour * 60) + (m_PlayerManager.jobData.fullTimeJob.workTime) + /*(m_PlayerManager.jobData.partTimeJob.workTime) + */(LocationManager.Instance.currentDistance * 6) + AllSetActionTime;
        AllHealth = (sleepHealth + AllSetActionHealth) - (workHealth + partTimeHealth);
        AllHappy = (sleepHappy + AllSetActionHappy) - (workHappy + partTimeHappy);
        
        m_HappinessSlider.maxValue = (workHappy + partTimeHappy);
        m_HappinessSlider.value = PlayerManager.Instance.playerData.happiness + (sleepHappy) + AllSetActionHappy;
        m_HealthSlider.maxValue = (workHealth + partTimeHealth);
        m_HealthSlider.value = PlayerManager.Instance.playerData.health + (sleepHealth) + AllSetActionHealth;

        m_HappyText.text = (sleepHappy + AllSetActionHappy).ToString("N0") + "(+" + PlayerManager.Instance.playerData.happiness.ToString("N0") + ")";
        m_HealthText.text = (sleepHealth + AllSetActionHealth).ToString("N0");

        // m_HappyText.text = (sleepHappy + AllSetActionHappy).ToString("N0") + "(+" + PlayerManager.Instance.playerData.happiness.ToString("N0") + ")" + "/" + (workHappy + partTimeHappy).ToString("N0");
        // m_HealthText.text = (sleepHealth + AllSetActionHealth).ToString("N0") + "/" + (workHealth + partTimeHealth).ToString("N0");

        m_TravelTimeText.text = "ใช้เวลา " + (LocationManager.Instance.currentDistance * 6) + " นาที";

        if (AllTime > 1440)
        {
            m_AllTimeText.text = "เวลาไม่พอ ";
        }
        else
        {
            m_AllTimeText.text = "ใช้เวลา " + AllTime + " นาที (" + ((float)AllTime / 60f).ToString("N2") + " ช.ม.)";
        }

        
        if (AllHealth > 0)
        {
            m_AllHealtText.text = "ได้พลังกาย " + Mathf.Abs(AllHealth);
        }
        else
        {
            m_AllHealtText.text = "ใช้พลังกาย " + Mathf.Abs(AllHealth);
            if (Mathf.Abs(AllHealth) > PlayerManager.Instance.playerData.health)
            {
                m_AllHealtText.text = "ใช้พลังกาย " + Mathf.Abs(AllHealth) + " (พลังกายไม่พอ)";
            }
        }
        
        if (AllHappy > 0)
        {
            m_AllHappyText.text = "ได้พลังใจ " + Mathf.Abs(AllHappy);
        }
        else
        {
            m_AllHappyText.text = "ใช้พลังใจ " + Mathf.Abs(AllHappy);
            if (Mathf.Abs(AllHappy) > PlayerManager.Instance.playerData.happiness)
            {
                m_AllHappyText.text = "ใช้พลังใจ " + Mathf.Abs(AllHappy) + " (พลังใจไม่พอ)";
            }
        }

        // m_DoBtn.interactable = CanDo();
        m_DoBtn.interactable = true;
    }

    private void UpdateSleepTimeUI()
    {
        m_SleepTimeText.text = sleepTimeInHour.ToString("N0") + " ช.ม.";
        m_SleepUseText.text = "ฟื้นพลังกาย: " + (sleepHealth) + ", พลังใจ: " + (sleepHappy);
    }

    private void UpdateWorkTimeUI()
    {
        m_WorkTimeText.text = m_PlayerManager.jobData.fullTimeJob.workTimeHour.ToString("N0") + " ช.ม.";
        m_WorkUseText.text = "ใช้พลังกาย: " + (workHealth) + ", พลังใจ: " + (workHappy);
    }

    private void UpdatePartTimeUI()
    {
        m_PartTimeTimeText.text = m_PlayerManager.jobData.partTimeJob.workTimeHour.ToString("N0") + " ช.ม.";
        m_PartTimeUseText.text = "ใช้พลังกาย: " + (partTimeHealth) + ", พลังใจ: " + (partTimeHappy);
    }

    public void OnClickChangeSleepTime(int _Value)
    {   
        sleepTimeInHour += _Value;
        if (sleepTimeInHour > 18)
        {
            sleepTimeInHour = 18;
        }
        else if (sleepTimeInHour < 1)
        {
            sleepTimeInHour = 1;
        }
        sleepHealth = sleepTimeInHour * 5;
        sleepHappy = sleepTimeInHour * 4;
        UpdateSleepTimeValue();
        m_ActionPlannerParent.GetChild(0).GetComponent<ActionPlanElement>().UpdateUI();
        UpdateUI();
    }

    public void OnClickChangeWorkTime(int _Value)
    {   
        workTimeInHour += _Value;
        if (workTimeInHour > 10)
        {
            workTimeInHour = 10;
        }
        else if (workTimeInHour < 1)
        {
            workTimeInHour = 1;
        }
        workHealth = workTimeInHour * 15;
        workHappy = workTimeInHour * 10;
        UpdateUI();
    }

    public void OnClickChangePartTime(int _Value)
    {   
        partTimeInHour += _Value;
        if (partTimeInHour > 10)
        {
            partTimeInHour = 10;
        }
        else if (partTimeInHour < 0)
        {
            partTimeInHour = 0;
        }
        partTimeHealth = partTimeInHour * 5;
        partTimeHappy = partTimeInHour * 10;
        UpdateUI();
    }

    public void OnClickSetAction(int _Value)
    {
        ActionManager.Instance.activityPlanner[setActionIndex].time += _Value;
        if (ActionManager.Instance.activityPlanner[setActionIndex].time > 10)
        {
            ActionManager.Instance.activityPlanner[setActionIndex].time = 10;
        }
        else if (ActionManager.Instance.activityPlanner[setActionIndex].time < 0)
        {
            ActionManager.Instance.activityPlanner[setActionIndex].time = 0;
        }
        // UpdateSetAction(setActionIndex);
        UpdateUI();
    }

    public void OnClickDeleteAction()
    {
        ActionManager.Instance.activityPlanner.RemoveAt(setActionIndex);
        setActionIndex = -1;

        UpdateUI();
        SetCloseAll();
    }

    private void UpdateHomeUI()
    {
        m_HomeNameText.text = LocationManager.Instance.location[LocationManager.Instance.locationData.currentHomeLo].locationName;
        m_HomeImage.sprite = LocationManager.Instance.location[LocationManager.Instance.locationData.currentHomeLo].locationImage;
        m_HomeDistanceText.text = LocationManager.Instance.currentDistance.ToString("N0") + " ก.ม.";
    }

    public void OnClickHomeBtn()
    {
        UIManager.Instance.SwitchState(State.UIState.EstatePage);
    }

    public void OnClickTravelBtn()
    {
        // UIManager.Instance.SwitchState(State.UIState.EstatePage);
    }

    public void OnClickConfirmBtn()
    {
            Hide();
            UpdateSleepTimeValue();

            StartCoroutine("Doing");
    }

    public IEnumerator Doing()
    {
        StateManager.Instance.SwitchDayState(State.DayState.DailyEvent);

        yield return new WaitForSeconds(0.5f);

        PlayerManager.Instance.SleepTime();

        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < ActionManager.Instance.activityPlanner.Count; i++)
        {
            if (ActionManager.Instance.activityPlanner[i].type != State.Expense.None
            && ActionManager.Instance.activityPlanner[i].time > 0)
            {
                Debug.Log(ActionManager.Instance.activityPlanner[i].activityType);
                ActionManager.Instance.DoAction(ActionManager.Instance.activityPlanner[i].activityType, ActionManager.Instance.activityPlanner[i].type, ActionManager.Instance.activityPlanner[i].time);
            }
        }

        yield return new WaitForSeconds(0.5f);

        if (!PlayerManager.Instance.haveFullTime() || CalManager.Instance.isDayOff)
        {
            StateManager.Instance.SwitchDayState(State.DayState.Day);
        }
        else
        {
            StateManager.Instance.SwitchDayState(State.DayState.Day);
            JobManager.Instance.WorkFullTime();
        }

        yield return new WaitForSeconds(0.5f);

        StateManager.Instance.SwitchDayState(State.DayState.DayEnd);
    }

    public void OnClickAddBtn()
    {
        PlannerElement newObj;
        newObj = Instantiate(plannerPrefab, m_PlannerParent);
    }

    private void DestroyElementPrefab()
    {
        for (int i = 0; i < m_PlannerParent.childCount; i++)
        {
            m_PlannerParent.GetChild(i).GetComponent<PlannerElement>().DestroySelf();
        }
    }

    private void DestroyPlannerPrefab()
    {
        for (int i = 1; i < m_ActionPlannerParent.childCount; i++)
        {
            m_ActionPlannerParent.GetChild(i).GetComponent<ActionPlanElement>().DestroySelf();
        }
    }

    public void OnClickFadeBtn()
    {
        Hide();
    }

    private void UpdateSleepTimeValue()
    {
        m_PlayerManager.UpdateSleepTime(sleepTimeInHour);
    }

    private bool CanDo()
    {
        if (AllTime > 1440)
        {
            return false;
        }

        if (m_HappinessSlider.value < m_HappinessSlider.maxValue)
        {
            return false;
        }

        if (m_HealthSlider.value < m_HealthSlider.maxValue)
        {
            return false;
        }

        return true;
    }

    public void UpdateSetActionZone()
    {
        DestroyPlannerPrefab();
        for (int i = 0; i < ActionManager.Instance.activityPlanner.Count; i++)
        {
            ActionPlanElement newObj;
            newObj = Instantiate(actionPlannerPrefab, m_ActionPlannerParent);
            newObj.index = i;
            // newObj.uiPage = this;
            newObj.UpdateUI();
        }
    }

    public void SetCloseAll()
    {
        m_SetSleep.SetActive(false);
        m_SetAction.SetActive(false);
        m_SelectAction.SetActive(false);
    }

    public void SetSleep()
    {
        m_SetSleep.SetActive(true);
        m_SetAction.SetActive(false);
        m_SelectAction.SetActive(false);
    }

    public void SetAction()
    {
        m_SetSleep.SetActive(false);
        m_SetAction.SetActive(true);
        m_SelectAction.SetActive(false);
    }

    public void SelectAction(int _Index)
    {
        setActionIndex = _Index;
        m_SetSleep.SetActive(false);
        m_SetAction.SetActive(false);
        m_SelectAction.SetActive(true);
        OpenActionPanel("Food");
    }

    public void OpenActionPanel(string _ActionType)
    {
        State.Expense _actionType = (State.Expense)Enum.Parse(typeof(State.Expense), _ActionType);

        for (int i = 0; i < m_ActionPanel.transform.childCount; i++)
        {
            m_ActionPanel.transform.GetChild(i).GetComponent<ActionElement>().DestroySelf();
        }

        for (int i = 0; i < ActionManager.Instance.activity[(int)_actionType].activity.Length; i++)
        {
            int _Index = i;
            ActionElement newAtcion = Instantiate(m_ActionPrefab, m_ActionPanel.transform);

            newAtcion.SetActionSet(ActionManager.Instance.activity[(int)_actionType].activity[_Index],
                                (int)_actionType, ()=>
                                {
                                    int Jndex = -1;
                                    bool isAgain = false;
                                    for (int j = 0; j < ActionManager.Instance.activityPlanner.Count; j++)
                                    {
                                        Jndex = j;
                                        if (ActionManager.Instance.activityPlanner[Jndex].type == _actionType &&
                                        ActionManager.Instance.activityPlanner[Jndex].activityType == ActionManager.Instance.activity[(int)_actionType].activity[_Index].activityType)
                                        {
                                            isAgain = true;
                                            break;
                                        }
                                    }

                                    if (isAgain)
                                    {
                                        setActionIndex = Jndex;
                                        // UpdateSetAction(setActionIndex);
                                        UpdateUI();
                                        SetAction();
                                    }
                                    else
                                    {
                                        ActivityPlanner setAction = ActionManager.Instance.ConvertActivity(ActionManager.Instance.activity[(int)_actionType].activity[_Index].activityType, _actionType);
                                        ActionManager.Instance.activityPlanner[setActionIndex] = setAction;
                                        // UpdateSetAction(setActionIndex);
                                        UpdateUI();
                                        SetAction();
                                    }

                                });
        }
    }
}
