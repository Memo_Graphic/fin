﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIBillPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_TitleText;

    [Header("Prefabs")]
    [SerializeField] private BillElement m_BillPrefab;
    [SerializeField] private Transform m_BillParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {

    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroyElementPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(UIManager.Instance.previusState);
                                                });
    }

    public override void UpdateUI()
    {
        UpdateAsset();
    }

    private void UpdateAsset()
    {
        DestroyElementPrefab();
        for (int i = 0; i < m_PlayerManager.billData.bill.Count; i ++)
        {
            int _Index = i;
            BillElement newObj;
            newObj = Instantiate(m_BillPrefab, m_BillParent);
            newObj.ShowElement(m_PlayerManager.billData.bill[i],
                            _Index);
        }
    }

    private void DestroyElementPrefab()
    {
        for (int i = 0; i < m_BillParent.childCount; i++)
        {
            m_BillParent.GetChild(i).GetComponent<BillElement>().DestroySelf();
        }
    }

    public void OnClickConfirmBtn()
    {
        Hide();
    }
}
