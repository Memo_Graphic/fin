﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UITimePage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_TravelNameText;
    [SerializeField] private TextMeshProUGUI m_TravelSpeedText;
    [SerializeField] private TextMeshProUGUI m_TravelDistanceText;
    [SerializeField] private TextMeshProUGUI m_TravelFixedCostText;
    [SerializeField] private TextMeshProUGUI m_TravelHealthText;
    [SerializeField] private TextMeshProUGUI m_TravelHappyText;

    [Header("TMP - NotOwn")]
    [SerializeField] private Image m_NotOwnTravelImage;
    [SerializeField] private TextMeshProUGUI m_NotOwnTravelNameText;
    [SerializeField] private TextMeshProUGUI m_NotOwnTravelSpeedText;
    [SerializeField] private TextMeshProUGUI m_NotOwnFixedCostText;
    [SerializeField] private TextMeshProUGUI m_NotOwnCostText;
    [SerializeField] private TextMeshProUGUI m_NotOwnPriceText;
    [SerializeField] private TextMeshProUGUI m_NotOwnHealthText;
    [SerializeField] private TextMeshProUGUI m_NotOwnHappyText;
    [SerializeField] private TextMeshProUGUI m_NotOwnCoolText;

    [Header("TMP - Buy")]
    [SerializeField] private Image m_BuyTravelImage;
    [SerializeField] private TextMeshProUGUI m_BuyCarName;
    [SerializeField] private TextMeshProUGUI m_BuyCarDistance;
    [SerializeField] private TextMeshProUGUI m_BuyCarFixedCost;
    [SerializeField] private TextMeshProUGUI m_BuyCarPrice;
    [SerializeField] private TextMeshProUGUI m_BuyCarLoanPrice;
    [SerializeField] private TextMeshProUGUI m_BuyCarDownPrice;
    [SerializeField] private TextMeshProUGUI m_BuyCarIR;
    [SerializeField] private TextMeshProUGUI m_BuyCarYear;
    [SerializeField] private TextMeshProUGUI m_BuyCarLoanAmount;
    [SerializeField] private TextMeshProUGUI m_BuyCarInstallment;
    [SerializeField] private TextMeshProUGUI m_BuyCarLoanSum;
    [SerializeField] private TextMeshProUGUI m_BuyCarSum;
    [SerializeField] private TextMeshProUGUI m_BuyCarAllSum;

    [Header("Image")]
    [SerializeField] private Image m_TravelImage;

    [Header("GameObjects")]
    [SerializeField] private GameObject[] m_PerBtn;
    [SerializeField] private GameObject m_BuyPanel;
    [SerializeField] private GameObject m_NotOwnPanel;

    [Header("Button")]
    [SerializeField] private Button m_ConfirmBuyBtn;

    [Header("Prefabs")]
    [SerializeField] private TravelApplyElement m_TravelPrefab;
    [SerializeField] private Transform m_TravelParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private TravelBy travel;
    private Asset newCar;
    private float downPercentage;
    private float downPrice;
    private float loanAmount;
    private float ir;
    private float year;
    private float installment;
    

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
        UpdateUI();
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(State.UIState.MainPage);

                                                    if (StateManager.Instance.currentState == State.GameState.Story_11_1)
                                                    {
                                                        StateManager.Instance.NextState();
                                                    }
                                                });
    }

    public override void UpdateUI()
    {
        if (StateManager.Instance.currentState == State.GameState.Story_10_2)
        {
            
            StateManager.Instance.SwitchState(State.GameState.Story_11_0);
        }

        UpdateTravelUI();
    }

    public void UpdateTravelUI()
    {
        m_TravelNameText.text = LocationManager.Instance.currenTravel.travelName;
        if (LocationManager.Instance.currenTravel.travelName == "รถส่วนตัว")
        {
            m_TravelNameText.text += " " + AssetManager.Instance.FindAsset(LocationManager.Instance.currenTravel.carType, State.AssetType.car).assetName;
        }
        float TravelTime = LocationManager.Instance.currenTravel.MIN_PER_KM * LocationManager.Instance.currentDistance;
        m_TravelSpeedText.text = TravelTime.ToString("N0") + " นาที";
        m_TravelDistanceText.text = LocationManager.Instance.currentDistance.ToString("N0") + " กม.";
        m_TravelFixedCostText.text = LocationManager.Instance.currenTravel.costPerUse.ToString("N0") + " ฿";
        m_TravelHealthText.text = LocationManager.Instance.currenTravel.health.ToString("N0");
        m_TravelHappyText.text = LocationManager.Instance.currenTravel.happy.ToString("N0");

        m_TravelImage.sprite = LocationManager.Instance.currenTravel.travelBigImage;

        DestroyPrefab();
        CreateElement();
    }

    public void UpdateNotOwnUI(TravelBy _Travel, Asset _NewCar)
    {
        travel = _Travel;
        newCar = _NewCar;

        m_NotOwnTravelNameText.text = newCar.assetName;
        m_NotOwnTravelSpeedText .text =  travel.MIN_PER_KM.ToString("N0") + " นาที/กม.";
        m_NotOwnFixedCostText.text = newCar.fixedCost.ToString("N0") + " ฿";
        m_NotOwnCostText.text = travel.costPerUse.ToString("N0") + " ฿";
        m_NotOwnPriceText.text = newCar.price.ToString("N0") + " ฿";
        m_NotOwnHealthText.text = (travel.health * -1).ToString("N0");
        m_NotOwnHappyText.text = (travel.happy * -1).ToString("N0");
        // m_NotOwnCoolText.text = 

        m_NotOwnTravelImage.sprite = travel.travelBigImage;

        m_NotOwnPanel.SetActive(true);
    }

    public void UpdateBuyUI()
    {
        downPrice = newCar.price * downPercentage / 100f;
        loanAmount = newCar.price - downPrice;
        installment = (loanAmount + (loanAmount * (ir / 100f) * year)) / (year * CalManager.Instance.MONTH_IN_YEAR);
        float Allsum = installment * year * CalManager.Instance.MONTH_IN_YEAR;
        
        m_BuyCarName.text = newCar.assetName;
        m_BuyCarDistance.text = travel.MIN_PER_KM.ToString("N0") + " นาที/กม.";
        m_BuyCarFixedCost.text = travel.costPerUse.ToString("N0") + " ฿";
        m_BuyCarPrice.text = newCar.price.ToString("N0") + " ฿";

        m_BuyCarDownPrice.text = downPrice.ToString("N0") + " ฿";
        m_BuyCarLoanAmount.text = loanAmount.ToString("N0") + " ฿";

        m_BuyCarYear.text = year.ToString("N0") + " ปี";
        m_BuyCarInstallment.text = installment.ToString("N0") + " ฿";

        m_BuyCarLoanPrice.text = downPrice.ToString("N0") + " ฿";

        m_BuyCarIR.text = ir.ToString("N2") + "%/ปี";
        m_BuyCarLoanSum.text = loanAmount.ToString("N0") + " ฿";
        m_BuyCarSum.text = (Allsum - loanAmount).ToString("N0") + " ฿";
        m_BuyCarAllSum.text = Allsum.ToString("N0") + " ฿";

        m_BuyTravelImage.sprite = travel.travelImage;

        m_ConfirmBuyBtn.interactable = PlayerManager.Instance.accountData.account[0].balance >= downPrice;
    }

    private void CreateElement()
    {
        for (int i = 0; i < LocationManager.Instance.travelBy.Length; i++)
        {
            int index = i;
            TravelApplyElement newObj = Instantiate(m_TravelPrefab, m_TravelParent);
            newObj.SetTravel(LocationManager.Instance.travelBy[i], index);
        }
    }

    private void DestroyPrefab()
    {
        for (int i = 0; i < m_TravelParent.childCount; i++)
        {
            m_TravelParent.GetChild(i).GetComponent<TravelApplyElement>().DestroySelf();
        }
    }

    public void OnClickBuyBtn()
    {
        year = 20;
        OnClickChangeDownPer(20);
        ir = BankManager.Instance.currentIR;

        UpdateBuyUI();

        m_NotOwnPanel.SetActive(false);
        m_BuyPanel.SetActive(true);
    }

    public void OnClickConfirmBuyBtn()
    {
        AssetManager.Instance.BuyCar(newCar, downPercentage, year);

        m_BuyPanel.SetActive(false);
        UpdateUI();
    }

    public void OnClickChangeDownPer(float _Value)
    {   
        downPercentage = _Value;
        if (downPercentage > 100)
        {
            downPercentage = 100;
        }
        else if (downPercentage < 0)
        {
            downPercentage = 0;
        }

        for (int i = 0; i < m_PerBtn.Length; i++)
        {
            m_PerBtn[i].SetActive(false);
        }

        m_PerBtn[(int)(downPercentage / 10)].SetActive(true);
        UpdateBuyUI();
    }

    public void OnClickChangeYear(float _Value)
    {   
        year += _Value;
        if (year > 30)
        {
            year = 30;
        }
        else if (year < 5)
        {
            year = 5;
        }
        UpdateBuyUI();
    }

    public void OnClickConfirmBtn()
    {
        Hide();
    }
}
