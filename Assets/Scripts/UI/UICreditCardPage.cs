﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UICreditCardPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_UsedCreditText;
    [SerializeField] private TextMeshProUGUI m_EnoughCreditText;
    [SerializeField] private TextMeshProUGUI m_CalInterestText;
    [SerializeField] private TextMeshProUGUI m_IRText;

    [Header("Prefabs")]
    [SerializeField] private CreditFolder m_CreditPrefab;
    [SerializeField] private Transform m_CreditParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroyElementPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(UIManager.Instance.previusState);
                                                });
    }

    public override void UpdateUI()
    {
        UpdateStatement();
    }

    private void UpdateStatement()
    {
        DestroyElementPrefab();

        m_UsedCreditText.text = m_PlayerManager.liabilityData.creditCard[0].usedCredit.ToString("N2") + " ฿";
        m_EnoughCreditText.text = m_PlayerManager.liabilityData.creditCard[0].enoughCredit.ToString("N2") + " ฿";
        // m_CalInterestText.text = m_PlayerManager.liabilityData.creditCard[0].calInterest.ToString("N2") + "฿";
        m_CalInterestText.text = "วงเงิน " + m_PlayerManager.liabilityData.creditCard[0].maxCredit.ToString("N2") + " ฿";
        m_IRText.text = m_PlayerManager.liabilityData.creditCard[0].ir.ToString("N1") + "% /ปี";

        SpawnElementPrefab();
    }

    public void SpawnElementPrefab()
    {
        for (int i = 0; i < m_PlayerManager.liabilityData.creditCard[0].date.Count; i++)
        {
            int index = i;
            if (m_PlayerManager.liabilityData.creditCard[0].date[i].element.Count > 0)
            {
                CreditFolder creditFolder = Instantiate(m_CreditPrefab, m_CreditParent);
                creditFolder.ShowElement(index);
            }
        }
    }

    private void DestroyElementPrefab()
    {
        for (int i = 0; i < m_CreditParent.childCount; i++)
        {
            m_CreditParent.GetChild(i).GetComponent<CreditFolder>().DestroySelf();
        }
    }

    public void OnClickFadeBtn()
    {
        Hide();
    }
}
