﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UILotteryPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;

    [Header("Popup")]
    [SerializeField] private TextMeshProUGUI[] m_NumberText;
    [SerializeField] private TextMeshProUGUI m_AmountText;
    [SerializeField] private TextMeshProUGUI m_PriceText;
    
    [Header("GameObject")]
    [SerializeField] private GameObject m_PopupPage;
    [SerializeField] private GameObject m_HistoryPage;
    
    [Header("Prefabs")]
    [SerializeField] private LottoElement m_LottoPrefab;
    [SerializeField] private Transform m_LottoParent;
    [SerializeField] private LottoElement m_HistoryPrefab;
    [SerializeField] private Transform m_HistoryParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private int currentAccount;
    private bool isBuy;
    int number;
    int amount;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {

    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroyElementPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(State.UIState.MainPage);
                                                });
    }

    public override void UpdateUI()
    {
        DestroyElementPrefab();
        InstantiatePrefabs();
    }

    public void UpdatePopup()
    {
        m_PopupPage.SetActive(true);

        string numText = number.ToString("000000");
        for (int i = 0; i < m_NumberText.Length; i++)
        {
            m_NumberText[i].text = numText[i].ToString();
        }

        m_AmountText.text = amount.ToString("N0");
        m_PriceText.text = (amount * BankManager.Instance.lottoData.lottoPrice).ToString("N0") + " ฿";
    }

    public void UpdateHistory()
    {
        DestroyElementPrefab();
        InstantiateHistoryPrefabs();
    }

    public void OpenPopup(int _Number)
    {
        number = _Number;
        amount = 1;
        UpdatePopup();
    }

    public void OnClickBuyLotto()
    {
        PlayerManager.Instance.SubtractMoney(amount * BankManager.Instance.lottoData.lottoPrice, () => 
        {
            PlayerManager.Instance.BuyLotto(number, amount);
            m_PopupPage.SetActive(false);

            PlayerBalanceElement balance = new PlayerBalanceElement();
            balance.balanceName = "ฉลากกินแบ่งรัฐบาล";
            balance.amount = amount * BankManager.Instance.lottoData.lottoPrice;
            PlayerManager.Instance.RecordBalance(balance, State.Expense.ETC);
        });
    }

    public void OnClickBuyLottoCredit()
    {
        PlayerManager.Instance.UseCredit(0, amount * BankManager.Instance.lottoData.lottoPrice, "ฉลากกินแบ่งรัฐบาล", () => 
        {
            PlayerManager.Instance.BuyLotto(number, amount);
            m_PopupPage.SetActive(false);
        });
    }
    
    public void OnClickChangeActionTime(int _Value)
    {
        amount += _Value;
        if (amount > 99)
        {
            amount = 99;
        }
        else if (amount < 0)
        {
            amount = 0;
        }
        UpdatePopup();
    }

    public void OnClickLottoHistory()
    {
        m_HistoryPage.SetActive(true);
        UpdateHistory();
    }

    public void InstantiatePrefabs()
    {
        for (int i = 0; i < BankManager.Instance.lottoData.lottoNumber.Length; i ++)
        {
            LottoElement newObj;
            newObj = Instantiate(m_LottoPrefab, m_LottoParent);
            newObj.SetLotto(i);
        }
    }

    public void InstantiateHistoryPrefabs()
    {
        for (int i = m_PlayerManager.assetData.lottoHistory.Count - 1; i > 0; i --)
        {
            for (int j = 0; j < m_PlayerManager.assetData.lottoHistory[i].lottery.Count; j ++)
            {
                LottoElement newObj;
                newObj = Instantiate(m_HistoryPrefab, m_HistoryParent);
                newObj.SetLottoHistory(i, j);
            }
        }
    }

    private void DestroyElementPrefab()
    {
        for (int i = 0; i < m_LottoParent.childCount; i++)
        {
            m_LottoParent.GetChild(i).GetComponent<LottoElement>().DestroySelf();
        }
        for (int i = 0; i < m_HistoryParent.childCount; i++)
        {
            m_HistoryParent.GetChild(i).GetComponent<LottoElement>().DestroySelf();
        }
    }

    public void OnClickFadeBtn()
    {
        Hide();
    }
}