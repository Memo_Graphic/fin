﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIStatementPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_YearText;
    [SerializeField] private TextMeshProUGUI m_AmountText;

    [Header("Prefabs")]
    [SerializeField] private StatementElement m_StatementPrefab;
    [SerializeField] private Transform m_StatementParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private int currentMonth;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(UIManager.Instance.previusState);
                                                });
    }

    public override void UpdateUI()
    {
        currentMonth = CalManager.Instance.Month();
        UpdateStatement();
    }

    private void UpdateStatement()
    {
        DestroyElementPrefab();
        float total = 0;
        for (int i = 0; i < m_PlayerManager.balanceData.balanceElement[currentMonth - 1].balanceType.Count; i++)
        {
            if (m_PlayerManager.balanceData.balanceElement[currentMonth - 1].balanceType[i].totalCost > 0)
            {
                int _Index = i;
                StatementElement newObj;
                newObj = Instantiate(m_StatementPrefab, m_StatementParent);
                newObj.ShowElement(m_PlayerManager.balanceData.balanceElement[currentMonth - 1].balanceType[_Index].totalCost, 
                                _Index);

                total += m_PlayerManager.balanceData.balanceElement[currentMonth - 1].balanceType[_Index].totalCost;
            }
        }
        m_YearText.text = CalManager.Instance.Month(currentMonth * CalManager.Instance.DAY_IN_MONTH).ToString("00") 
                        + "/" + (CalManager.Instance.Year(currentMonth * CalManager.Instance.DAY_IN_MONTH) + 2000).ToString("0000");
        m_AmountText.text = total.ToString("N0") + " ฿";
    }

    private void DestroyElementPrefab()
    {
        for (int i = 0; i < m_StatementParent.childCount; i++)
        {
            m_StatementParent.GetChild(i).GetComponent<StatementElement>().DestroySelf();
        }
    }

    public void OnClickNext(int _Next)
    {
        currentMonth += _Next;
        if (currentMonth < 1)
        {
            currentMonth = 1;
        }
        else if (currentMonth >= CalManager.Instance.Month())
        {
            currentMonth = CalManager.Instance.Month();
        }
        UpdateStatement();
    }

    public void OnClickConfirmBtn()
    {
        Hide();
    }
}
