﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UISkillPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_EXPText;

    [Header("Prefabs")]
    [SerializeField] private UpSkillUIElement m_UpSkillPrefab;
    [SerializeField] private Transform m_UpSkillParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;

    private PlayerManager m_PlayerManager;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {

    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroySkillPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(UIManager.Instance.previusState);
                                                });
    }

    public override void UpdateUI()
    {
        UpdateEXP();
        DestroySkillPrefab();
        CreateSkillElement();
    }

    public void UpdateEXP()
    {
        // m_EXPText.text = "Skill EXP: " + m_PlayerManager.playerData.lvSkill.exp.ToString("N0");
    }

    private void CreateSkillElement()
    {
        UpSkillUIElement newObj = Instantiate(m_UpSkillPrefab, m_UpSkillParent);
        newObj.SetSkill(JobManager.Instance.upSkills, this);
    }

    private void DestroySkillPrefab()
    {
        for (int i = 0; i < m_UpSkillParent.childCount; i++)
        {
            m_UpSkillParent.GetChild(i).GetComponent<UpSkillUIElement>().DestroySelf();
        }
    }

    public void OnClickConfirmBtn()
    {
        Hide();
    }
}
