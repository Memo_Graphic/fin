﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
// using Utilities;

//[ExecuteAlways]
public class UIManager : Singleton<UIManager>
{
    [Header("Var")]
    public float scrWidth;
    public float scrHeight;
    public float scrRatio;

    [Header("Transforms")]
    [SerializeField] private RectTransform m_Canvas;

    [Header("State")]
    public State.UIState currentState;
    public State.UIState previusState;
    public UIElement[] uIElement;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // ScreenRatio();
    }

    public void Show(State.UIState _state)
    {
        if (uIElement[(int)_state] != null)
        {
            if (!uIElement[(int)_state].gameObject.activeSelf)
            {
                uIElement[(int)_state].gameObject.SetActive(true);
            }
            uIElement[(int)_state].Show();
        }
        else
        {
            // uIElement[(int)_state] = Instantiate(uIPrefabs[(int)_state], m_parent).GetComponent<UIElement>();
            // uIElement[(int)_state].Show();
        }
    }

    public void Hide(State.UIState _state)
    {
        if (!uIElement[(int)_state].gameObject.activeSelf)
        {
            uIElement[(int)_state].gameObject.SetActive(true);
        }
        uIElement[(int)_state].Hide();
    }

    public void HideAll()
    {
        for (int i = 0; i < uIElement.Length; i++)
        {
            if (i != (int)State.UIState.MainPage
            && i != (int)State.UIState.PopupPage)
            {
                if (uIElement[(int)i].gameObject.activeSelf)
                {
                    uIElement[(int)i].Hide();
                }
            }
        }
    }

    public void UpdateUI(State.UIState _state)
    {
        if (!uIElement[(int)_state].gameObject.activeSelf)
        {
            uIElement[(int)_state].gameObject.SetActive(true);
        }
        uIElement[(int)_state].UpdateUI();
    }

    public void UpdateCurrentUI()
    {
        UpdateUI(State.UIState.MainPage);
        UpdateUI(currentState);
    }

    public void SwitchState(State.UIState _state, bool _isUpdateState = true)
    {
        previusState = currentState;
        currentState = _state;
        if (_isUpdateState)
        {
            UpdateState();
        }
    }
    
    public void UpdateState()
    {
        switch (currentState)
        {
            case State.UIState.None:
                {
                    Debug.LogError("Fix UIState");
                }
                break;
            case State.UIState.MainPage:
                {
                    // Hide(previusState);
                    UpdateUI(currentState);
                }
                break;
            // case State.UIState.TimePage:
            //     {
            //         Show(State.UIState.TimePage);
            //     }
            //     break;
            default:
                {
                    Show(currentState);
                }
                break;
        }
    }

    private void ScreenRatio()
    {
        scrWidth = Screen.width;
        scrHeight = Screen.height;
        scrRatio = scrHeight / scrWidth;

        if (scrRatio > (1.78f))
        {
            m_Canvas.offsetMax = new Vector2(m_Canvas.offsetMax.x, -80f);
        }
        else
        {
            m_Canvas.offsetMax = new Vector2(m_Canvas.offsetMax.x, 0f);
        }
    }
}
