﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UITutorialPage : UIElement
{
    [Header("GameObjects")]
    [SerializeField] private GameObject m_ArrowObj;
    [SerializeField] private GameObject[] m_DetailObj;
    [SerializeField] private GameObject[] m_FadeObj;
    [SerializeField] private GameObject[] m_ArrowRefObj;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private int selectLocation;
    private Location location;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void Start()
    {
        m_PlayerManager = PlayerManager.Instance;
        UpdateUI();
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(State.UIState.MainPage);
                                                });
    }

    public override void UpdateUI()
    {
        SetArrow(false);

        for (int i = 0; i < m_FadeObj.Length; i++)
        {
            if (m_FadeObj[i])
            {
                m_FadeObj[i].SetActive(false);
            }
        }

        for (int i = 0; i < m_DetailObj.Length; i++)
        {
            if (m_DetailObj[i])
            {
                m_DetailObj[i].SetActive(false);
            }
        }

        // FadeObj((int)StateManager.Instance.currentState);

        if (StateManager.Instance.currentState <= State.GameState.Load)
        {

        }
        else if (StateManager.Instance.currentState == State.GameState.Story_1_0)
        {
            FadeObj(1);
            SetDetail(1);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_2_0)
        {

        }
        else if (StateManager.Instance.currentState == State.GameState.Story_3_0)
        {
            if (GameManager.Instance.gameLevel == State.Level.Easy)
            {
                FadeObj(2);
            }
            else if (GameManager.Instance.gameLevel == State.Level.Hard)
            {
                FadeObj(3);
            }
            SetDetail(3);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_4_0)
        {
            
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_5_0)
        {

        }
        else if (StateManager.Instance.currentState == State.GameState.Story_6_0)
        {
            FadeObj(6);
            SetDetail(6);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_7_0)
        {
            FadeObj(7);
            SetDetail(7);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_8_0)
        {
            FadeObj(8);
            SetDetail(8);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_9_0)
        {
            SetArrow(0);
            FadeObj(9);
            SetDetail(9);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_10_0)
        {
            SetArrow(1);
            SetDetail(10);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_10_1)
        {
            SetArrow(2);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_10_2)
        {
            SetArrow(3);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_11_0)
        {
            FadeObj(11);
            SetDetail(11);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_11_1)
        {
            SetArrow(4);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_12_0)
        {
            FadeObj(12);
            SetDetail(12);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_12_1)
        {
            SetArrow(7);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_12_2)
        {
            FadeObj(5);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_13_0)
        {
            FadeObj(13);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_13_1)
        {
            SetArrow(7);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_14_0)
        {
            FadeObj(14);
            SetDetail(14);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_15_0)
        {
            // SetArrow(8);
            FadeObj(15);
            SetDetail(15);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_16_0)
        {
            FadeObj(16);
            SetDetail(16);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_17_0)
        {
            FadeObj(17);
            SetDetail(17);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_18_0)
        {
            // SetArrow(10);
            FadeObj(18);
            SetDetail(18);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_19_0)
        {
            // SetArrow(11);
            FadeObj(19);
            SetDetail(19);
        }
        else if (StateManager.Instance.currentState == State.GameState.Story_20_0)
        {
            // SetArrow(9);
            FadeObj(20);
            SetDetail(20);
        }
        // else
        // {
        //     m_DetailObj.SetActive(true);
        //     m_DetailText.text = m_Detail[(int)StateManager.Instance.currentState];
        // }
    }

    public void SetDetail(int _Index)
    {
        m_DetailObj[_Index].SetActive(true);
    }

    public void FadeObj(int _Index)
    {
        for (int i = 0; i < m_FadeObj.Length; i++)
        {
            if (m_FadeObj[i])
            {
                m_FadeObj[i].SetActive(false);
            }
        }
        
        if (m_FadeObj[_Index])
        {
            m_FadeObj[_Index].SetActive(true);
        }
    }

    public void OnClickNext()
    {
        StateManager.Instance.NextState();
        UpdateUI();
    }

    public void SetArrow(bool _isShow)
    {
        m_ArrowObj.SetActive(_isShow);
    }

    public void SetArrow(GameObject _Obj)
    {
        m_ArrowObj.SetActive(true);
        if (m_ArrowObj.GetComponent<RectTransform>().position != _Obj.GetComponent<RectTransform>().position + new Vector3(0, 4, 0))
        {
            m_ArrowObj.GetComponent<RectTransform>().position = _Obj.GetComponent<RectTransform>().position;
            m_ArrowObj.GetComponent<RectTransform>().position += new Vector3(0, 4, 0);
        }
    }

    public void SetArrow(int _Index)
    {
        m_ArrowObj.SetActive(true);
        if (m_ArrowObj.GetComponent<RectTransform>().position != m_ArrowRefObj[_Index].GetComponent<RectTransform>().position + new Vector3(0, 4, 0))
        {
            m_ArrowObj.GetComponent<RectTransform>().position = m_ArrowRefObj[_Index].GetComponent<RectTransform>().position;
            m_ArrowObj.GetComponent<RectTransform>().position += new Vector3(0, 4, 0);
        }
    }

    public void Skip()
    {
        if (!GameManager.Instance.isTutorial)
        {
            Hide();
            return;
        }

        if (PlayerManager.Instance.jobData.fullTimeJob.jobID < 0)
        {
            JobManager.Instance.ApplyFullTime(JobManager.Instance.Job[(int)State.JobType.FullTime].jobs[0]);
        }

        if (PlayerManager.Instance.assetData.Asset[(int)State.AssetType.estate].element.Count == 0)
        {
            Location newLocation = LocationManager.Instance.location.Find(x => x.locationType == CalManager.Instance.startHome[(int)GameManager.Instance.gameLevel]);
            Asset newEstate = AssetManager.Instance.FindAsset(newLocation.locationType, State.AssetType.estate);
            AssetManager.Instance.RentEstate(newEstate);
            AssetManager.Instance.UseAsset(newEstate.assetType, State.AssetType.estate);

            UIManager.Instance.uIElement[(int)State.UIState.EstatePage].Hide();
        }

        if (PlayerManager.Instance.accountData.account[0].balance < CalManager.Instance.startMoney)
        {
            CalManager.Instance.SetStarterValue();
        }

        LocationManager.Instance.locationData.currentTravelIndex = 0;
        PlayerManager.Instance.playerData.sleepTimeInHour = 8;
            
        StateManager.Instance.SwitchState(State.GameState.Endless);

        UIManager.Instance.HideAll();
        UIManager.Instance.uIElement[(int)State.UIState.MainPage].GetComponent<UIMainPage>().OnClickCloseAll();
        UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().CloseAll();

        if (CalManager.Instance.realDay < 1)
        {
            // StartCoroutine(UIManager.Instance.uIElement[(int)State.UIState.MainPage].GetComponent<UIMainPage>().Doing());
            UIManager.Instance.uIElement[(int)State.UIState.MainPage].GetComponent<UIMainPage>().OnClickGOBtn();
        }
        else
        {
            UIManager.Instance.uIElement[(int)State.UIState.MainPage].GetComponent<UIMainPage>().OnClickHomeBtn();
        }
    }
}
