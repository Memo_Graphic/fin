﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIOpenPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_TitleText;
    [SerializeField] private GameObject m_ContinueBtn;


    public CanvasGroup canvasGroup;

    private List<Activity> action;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void Start()
    {
        UpdateUI();
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
    }

    public override void Hide()
    {

    }

    public override void UpdateUI()
    {
        m_ContinueBtn.SetActive(SaveManager.Instance.IsHaveGameData());
    }

    public void OnClickNewGame(string _SceneName)
    {
        SaveManager.Instance.IS_LOAD = false;
        StartCoroutine(LoadAsyncScene(_SceneName));
    }

    public void OnClickContinue(string _SceneName)
    {
        SaveManager.Instance.IS_LOAD = true;
        StartCoroutine(LoadAsyncScene(_SceneName));
    }

    IEnumerator LoadAsyncScene(string _SceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(_SceneName);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
