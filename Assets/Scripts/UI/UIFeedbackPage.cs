﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIFeedbackPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_DistanceText;

    [Header("Image")]
    [SerializeField] private Image m_FeedbackImage;
    [SerializeField] private Image m_BGImage;

    [Header("Sprite")]
    [SerializeField] private Sprite[] m_FeedbackSprite;

    [Header("Color")]
    [SerializeField] private Color[] m_Color;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
        // m_FadeBtn.onClick.AddListener(() =>
        // {
        //     OnClickFadeBtn();
        // });
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0f,
                                                0f,
                                                () =>
                                                {
                                                    // UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().EndTravel();
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(State.UIState.MainPage);
                                                });
    }

    public override void UpdateUI()
    {
        UpdateImage();
        UpdateDistance();
        StartCoroutine(Traveling());
    }

    public void UpdateDistance()
    {
        m_DistanceText.text = StateManager.Instance.currentDayState + " " + LocationManager.Instance.locationData.distance.ToString() + " KM";
    }

    public void UpdateImage()
    {
        m_FeedbackImage.sprite = m_FeedbackSprite[(int)StateManager.Instance.currentDayState];
        m_BGImage.color = m_Color[(int)StateManager.Instance.currentDayState];
    }

    public void OnClickSktpBtn()
    {
        Hide();
    }

    private void OnClickFadeBtn()
    {
        Hide();
    }

    IEnumerator Traveling(float _time = 2f)
    {
        yield return new WaitForSeconds(_time);
        Hide();
    }
}