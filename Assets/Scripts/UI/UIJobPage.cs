﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIJobPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_TitleText;

    [Header("Prefabs")]
    [SerializeField] private JobApplyElement m_ApplyJobPrefab;
    [SerializeField] private Transform m_ApplyJobParent;

    [Header("PartTime")]
    [SerializeField] private TextMeshProUGUI m_PartTimeNameText;
    [SerializeField] private TextMeshProUGUI m_PartTimeSalaryText;
    [SerializeField] private TextMeshProUGUI m_PartTimeWorkTimeText;
    [SerializeField] private Button m_PartTimeBtn;

    [Header("ETC")]
    public bool isFullTime = true;
    public CanvasGroup canvasGroup;

    private PlayerManager m_PlayerManager;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {

    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroyApplyJobPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(UIManager.Instance.previusState);
                                                });
    }

    public override void UpdateUI()
    {
        if (isFullTime)
        {
            m_TitleText.text = "Apply Full-Time Job";
        }
        else
        {
            m_TitleText.text = "Apply Part-Time Job";
        }
        UpdatePartTime();
        TogglePartTimeBtn();
        DestroyApplyJobPrefab();
        CreateJobElement(isFullTime);
    }

    private void CreateJobElement(bool _IsFullTime)
    {
        if (_IsFullTime)
        {
            for (int j = 0; j < JobManager.Instance.Job[(int)State.JobType.FullTime].jobs.Length; j++)
            {
                JobApplyElement newObj = Instantiate(m_ApplyJobPrefab, m_ApplyJobParent);
                newObj.SetJob(JobManager.Instance.Job[(int)State.JobType.FullTime].jobs[j], _IsFullTime);
            }
        }
        else
        {
            for (int j = 0; j < JobManager.Instance.Job[(int)State.JobType.PartTime].jobs.Length; j++)
            {
                JobApplyElement newObj = Instantiate(m_ApplyJobPrefab, m_ApplyJobParent);
                newObj.SetJob(JobManager.Instance.Job[(int)State.JobType.PartTime].jobs[j], _IsFullTime);
            }
        }
    }

    private void DestroyApplyJobPrefab()
    {
        for (int i = 0; i < m_ApplyJobParent.childCount; i++)
        {
            m_ApplyJobParent.GetChild(i).GetComponent<JobApplyElement>().DestroySelf();
        }
    }

    public void OnClickConfirmBtn()
    {
        Hide();
    }

    public void UpdatePartTime()
    {
        if (!isFullTime)
        {
            if (m_PlayerManager.havePartTime())
            {
                m_PartTimeNameText.text = "ชื่องาน: " + m_PlayerManager.jobData.partTimeJob.jobName;
                m_PartTimeSalaryText.text = "ค่าตอบแทน: " + m_PlayerManager.jobData.partTimeJob.salary.ToString("N0");
                m_PartTimeWorkTimeText.text = "ระยะเวลา: " + CalManager.Instance.TextMinTime(m_PlayerManager.jobData.partTimeJob.workTime, "HH");
            }
            else
            {

            }
        }
    }

    public void TogglePartTimeBtn()
    {
        if (m_PartTimeBtn)
        {
            m_PartTimeBtn.interactable = m_PlayerManager.havePartTime();
        }
    }

    public void OnClickPartTime()
    {
        JobManager.Instance.WorkPartTime();
        UpdateUI();
    }
}
