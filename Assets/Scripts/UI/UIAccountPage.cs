﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIAccountPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_AmountText;
    [SerializeField] private TextMeshProUGUI m_IRText;
    [SerializeField] private TextMeshProUGUI m_IRCalText;
    [SerializeField] private TextMeshProUGUI m_CashText;
    [SerializeField] private TextMeshProUGUI m_CalAmountText;
    [SerializeField] private TextMeshProUGUI m_IsDepositText;

    [Header("Image")]
    [SerializeField] private Image m_ArrowImg;
    [SerializeField] private Image m_BGImg;

    [Header("Button")]
    [SerializeField] private Button m_DoneBtn;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private int currentAccount;
    private bool isDeposit;
    private float amount;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {

    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(State.UIState.MainPage);
                                                });
    }

    public override void UpdateUI()
    {
        UpdateAccount();
    }

    public void UpdateAccount(int _Account = 1)
    {
        amount = 0;
        isDeposit = true;
        currentAccount = _Account;
        m_NameText.text = PlayerManager.Instance.accountData.account[currentAccount].accountName;
        m_AmountText.text = PlayerManager.Instance.accountData.account[currentAccount].balance.ToString("N2") + " ฿";
        m_IRText.text = BankManager.Instance.allAccount.accounts[m_PlayerManager.accountData.account[currentAccount].accountID].interestRate.ToString("N1") + "% /ปี";
        m_IRCalText.text = "ดอกเบี้ยสะสม " + PlayerManager.Instance.accountData.account[currentAccount].accruedExpenses.ToString("N2") + " ฿";;
        m_CashText.text = PlayerManager.Instance.accountData.account[0].balance.ToString("N2") + " ฿";
        UpdateCalculator();
    }

    private void UpdateCalculator()
    {
        if (isDeposit)
        {
            m_IsDepositText.text = "ฝากเงิน";
            m_CalAmountText.text = amount.ToString("N0") + " ฿";
            m_DoneBtn.interactable = amount <= PlayerManager.Instance.accountData.account[0].balance;
            m_ArrowImg.transform.localEulerAngles = new Vector3(0, 0, 0);
            m_ArrowImg.color = GameManager.Instance.green;
            m_BGImg.color = GameManager.Instance.green;
        }
        else
        {
            m_IsDepositText.text = "ถอนเงิน";
            m_CalAmountText.text = amount.ToString("N0") + " ฿";
            m_DoneBtn.interactable = amount <= PlayerManager.Instance.accountData.account[currentAccount].balance;
            m_ArrowImg.transform.localEulerAngles = new Vector3(0, 0, 180);
            m_ArrowImg.color = GameManager.Instance.red;
            m_BGImg.color = GameManager.Instance.red;
        }
    }

    public void OnClickCalculator()
    {
        isDeposit = !isDeposit;
        UpdateCalculator();
    }

    public void OnClickCalBtn(string BTN)
    {
        switch(BTN) 
        {
        case "1":
            amount = (Mathf.Floor(amount) * 10) + 1;
            break;
        case "2":
            amount = (Mathf.Floor(amount) * 10) + 2;
            break;
        case "3":
            amount = (Mathf.Floor(amount) * 10) + 3;
            break;
        case "4":
            amount = (Mathf.Floor(amount) * 10) + 4;
            break;
        case "5":
            amount = (Mathf.Floor(amount) * 10) + 5;
            break;
        case "6":
            amount = (Mathf.Floor(amount) * 10) + 6;
            break;
        case "7":
            amount = (Mathf.Floor(amount) * 10) + 7;
            break;
        case "8":
            amount = (Mathf.Floor(amount) * 10) + 8;
            break;
        case "9":
            amount = (Mathf.Floor(amount) * 10) + 9;
            break;
        case "0":
            amount = (Mathf.Floor(amount) * 10) + 0;
            break;
        case "00":
            amount = (Mathf.Floor(amount) * 10) * 10;
            break;
        case ".":
            
            break;
        case "<":
            amount = Mathf.Floor(amount / 10);
            break;
        case "AC":
            amount = 0;
            break;
        case "DONE":
            OnClickCalDone();
            break;
        default:
            // code block
            break;
        }

        if (isDeposit)
        {
            if (amount > PlayerManager.Instance.accountData.account[0].balance)
            {
                amount = Mathf.Floor((float)PlayerManager.Instance.accountData.account[0].balance);
            }
        }
        else
        {
            if (amount > PlayerManager.Instance.accountData.account[currentAccount].balance)
            {
                amount = Mathf.Floor((float)PlayerManager.Instance.accountData.account[currentAccount].balance);
            }
        }

        UpdateCalculator();
    }

    private void OnClickCalDone()
    {
        if (isDeposit)
        {
            PlayerManager.Instance.TransferMoney(0, currentAccount, amount);
        }
        else
        {
            PlayerManager.Instance.TransferMoney(currentAccount, 0, amount);
        }
        amount = 0;
        UpdateAccount(currentAccount);
    }

    public void OnClickFadeBtn()
    {
        Hide();
    }
}
