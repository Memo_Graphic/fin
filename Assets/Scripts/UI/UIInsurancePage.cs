﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIInsurancePage : UIElement
{
    [Header("TMP - Life")]
    [SerializeField] private GameObject m_haveLife;
    [SerializeField] private TextMeshProUGUI m_LifeNameText;
    [SerializeField] private TextMeshProUGUI m_LifeLimitText;
    [SerializeField] private TextMeshProUGUI m_LifePriceText;
    [SerializeField] private TextMeshProUGUI m_LifeDateText;

    [Header("TMP - Health")]
    [SerializeField] private GameObject m_haveHealth;
    [SerializeField] private TextMeshProUGUI m_HealthNameText;
    [SerializeField] private TextMeshProUGUI m_HealthLimitText;
    [SerializeField] private TextMeshProUGUI m_HealthPriceText;
    [SerializeField] private TextMeshProUGUI m_HealthDateText;


    [Header("TMP - Accident")]
    [SerializeField] private GameObject m_haveAccident;
    [SerializeField] private TextMeshProUGUI m_AccidentNameText;
    [SerializeField] private TextMeshProUGUI m_AccidentLimitText;
    [SerializeField] private TextMeshProUGUI m_AccidentPriceText;
    [SerializeField] private TextMeshProUGUI m_AccidentDateText;

    [Header("GameObjects")]
    [SerializeField] private GameObject m_LifeObj;
    [SerializeField] private GameObject m_HealthObj;
    [SerializeField] private GameObject m_AccidentObj;

    [Header("Prefabs")]
    [SerializeField] private InsuranceUIElement m_InsurancePrefab;
    [SerializeField] private Transform m_LifeInsuranceParent;
    [SerializeField] private Transform m_HealthInsuranceParent;
    [SerializeField] private Transform m_AccidentInsuranceParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroyElementPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(UIManager.Instance.previusState);
                                                });
    }

    public override void UpdateUI()
    {
        m_LifeObj.SetActive(false);
        m_HealthObj.SetActive(false);
        m_AccidentObj.SetActive(false);
        UpdateInsuranceUI();
    }

    private void UpdateInsuranceUI()
    {
        DestroyElementPrefab();

        if (m_PlayerManager.assetData.insurances[(int)State.Insurance.LifeInsurance].element.type != State.Insurance.None)
        {
            m_haveLife.SetActive(true);
            m_LifeNameText.text = m_PlayerManager.assetData.insurances[(int)State.Insurance.LifeInsurance].element.insuranceName;
            m_LifeLimitText.text = "วงเงินคุ้มครอง " + m_PlayerManager.assetData.insurances[(int)State.Insurance.LifeInsurance].element.protectionLimit.ToString("N0") + " ฿";
            m_LifePriceText.text = m_PlayerManager.assetData.insurances[(int)State.Insurance.LifeInsurance].element.price.ToString("N0") + " ฿";
            m_LifeDateText.text = "เวนคืน " + m_PlayerManager.assetData.insurances[(int)State.Insurance.LifeInsurance].element.totalPrice.ToString("N0") + " ฿";
        }
        else
        {
            m_haveLife.SetActive(false);
        }

        if (m_PlayerManager.assetData.insurances[(int)State.Insurance.HealthInsurance].element.type != State.Insurance.None)
        {
            m_haveHealth.SetActive(true);
            m_HealthNameText.text = m_PlayerManager.assetData.insurances[(int)State.Insurance.HealthInsurance].element.insuranceName;
            m_HealthLimitText.text = "วงเงินคุ้มครอง " + m_PlayerManager.assetData.insurances[(int)State.Insurance.HealthInsurance].element.protectionLimit.ToString("N0") + " ฿/ปี";
            m_HealthPriceText.text = m_PlayerManager.assetData.insurances[(int)State.Insurance.HealthInsurance].element.price.ToString("N0") + " ฿";
            int deadline = m_PlayerManager.assetData.insurances[(int)State.Insurance.HealthInsurance].element.expiredDate;
            m_HealthDateText.text = "หมดอายุใน " + CalManager.Instance.DateTime("DD/MM/YYYY", deadline);
        }
        else
        {
            m_haveHealth.SetActive(false);
        }

        if (m_PlayerManager.assetData.insurances[(int)State.Insurance.AccidentInsurance].element.type != State.Insurance.None)
        {
            m_haveAccident.SetActive(true);
            m_AccidentNameText.text = m_PlayerManager.assetData.insurances[(int)State.Insurance.AccidentInsurance].element.insuranceName;
            m_AccidentLimitText.text = "วงเงินคุ้มครอง " + m_PlayerManager.assetData.insurances[(int)State.Insurance.AccidentInsurance].element.protectionLimit.ToString("N0") + " ฿/ครั้ง";
            m_AccidentPriceText.text = m_PlayerManager.assetData.insurances[(int)State.Insurance.AccidentInsurance].element.price.ToString("N0") + " ฿";
            int deadline = m_PlayerManager.assetData.insurances[(int)State.Insurance.AccidentInsurance].element.expiredDate;
            m_AccidentDateText.text = "หมดอายุใน " + CalManager.Instance.DateTime("DD/MM/YYYY", deadline);
        }
        else
        {
            m_haveAccident.SetActive(false);
        }
    }

    public void OnClickLife(bool _IsShow)
    {
        m_LifeObj.SetActive(_IsShow);
        if (_IsShow)
        {
            SpawnInsuranceElementPrefab(State.Insurance.LifeInsurance, m_LifeInsuranceParent);
        }
        else
        {
            UpdateInsuranceUI();
        }
    }

    public void OnClickHealth(bool _IsShow)
    {
        m_HealthObj.SetActive(_IsShow);
        if (_IsShow)
        {
            SpawnInsuranceElementPrefab(State.Insurance.HealthInsurance, m_HealthInsuranceParent);
        }
        else
        {
            UpdateInsuranceUI();
        }
    }

    public void OnClickAccident(bool _IsShow)
    {
        m_AccidentObj.SetActive(_IsShow);
        if (_IsShow)
        {
            SpawnInsuranceElementPrefab(State.Insurance.AccidentInsurance, m_AccidentInsuranceParent);
        }
        else
        {
            UpdateInsuranceUI();
        }
    }

    public void SpawnInsuranceElementPrefab(State.Insurance _Type, Transform _Parent)
    {
        for (int i = 0; i < AssetManager.Instance.insurances[(int)_Type].element.Length; i++)
        {
            int index = i;
            InsuranceUIElement insurance = Instantiate(m_InsurancePrefab, _Parent);
            insurance.ShowElement(index, _Type);
        }
    }

    private void DestroyElementPrefab()
    {
        for (int i = 0; i < m_LifeInsuranceParent.childCount; i++)
        {
            m_LifeInsuranceParent.GetChild(i).GetComponent<InsuranceUIElement>().DestroySelf();
        }
        for (int i = 0; i < m_HealthInsuranceParent.childCount; i++)
        {
            m_HealthInsuranceParent.GetChild(i).GetComponent<InsuranceUIElement>().DestroySelf();
        }
        for (int i = 0; i < m_AccidentInsuranceParent.childCount; i++)
        {
            m_AccidentInsuranceParent.GetChild(i).GetComponent<InsuranceUIElement>().DestroySelf();
        }
    }

    public void OnClickFadeBtn()
    {
        if (m_LifeObj.activeSelf)
        {
            OnClickLife(false);
        }
        else if (m_HealthObj.activeSelf)
        {
            OnClickHealth(false);
        }
        else if (m_AccidentObj.activeSelf)
        {
            OnClickAccident(false);
        }
        else
        {
            Hide();
        }
    }

    public void OnClickSellLife()
    {

    }
}
