﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UICreateCharPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_LifespanText;
    [SerializeField] private TextMeshProUGUI m_LevelText;
    [SerializeField] private TMP_InputField m_PlayerName;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private int selectLocation;
    private Location location;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void Start()
    {
        m_PlayerManager = PlayerManager.Instance;
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(State.UIState.MainPage);
                                                });
    }

    public override void UpdateUI()
    {
        m_PlayerManager = PlayerManager.Instance;
        m_LifespanText.text = m_PlayerManager.playerData.lifespan.ToString("N0") + " ปี";
        if (GameManager.Instance.gameLevel == State.Level.Hard)
        {
            m_LevelText.text = "ยาก";
        }
        else if (GameManager.Instance.gameLevel == State.Level.Easy)
        {
            m_LevelText.text = "ง่าย";
        }
    }

    void SetPlayerName()
    {
        m_PlayerManager.playerData.playerName = m_PlayerName.text;
        if (m_PlayerName.text == "")
        {
            m_PlayerManager.playerData.playerName = "นิรนาม";
        }
    }

    public void OnClickGender(string _Gender)
    {
        m_PlayerManager.playerData.playerGender = _Gender;
    }
    
    public void OnClickChangeActionTime(int _Value)
    {
        m_PlayerManager.playerData.lifespan += _Value;

        if (m_PlayerManager.playerData.lifespan > 100)
        {
            m_PlayerManager.playerData.lifespan = 100;
        }
        else if (m_PlayerManager.playerData.lifespan < 65)
        {
            m_PlayerManager.playerData.lifespan = 65;
        }

        UpdateUI();
    }
    
    public void OnClickChangeLevel(int _Value)
    {
        GameManager.Instance.gameLevel += _Value;
        
        if (GameManager.Instance.gameLevel > State.Level.Hard)
        {
            GameManager.Instance.gameLevel = State.Level.Hard;
        }
        else if (GameManager.Instance.gameLevel < State.Level.Easy)
        {
            GameManager.Instance.gameLevel = State.Level.Easy;
        }

        UpdateUI();
    }

    public void OnClickCreate()
    {
        SetPlayerName();
        Hide();
        StateManager.Instance.NextState();
        UIManager.Instance.Show(State.UIState.EstatePage);
        UIManager.Instance.Show(State.UIState.TutorialPage);
    }
}
