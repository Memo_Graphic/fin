﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIMapPage : UIElement
{
    [Header("Prefabs")]
    [SerializeField] private LocationElement locationPrefab;
    [SerializeField] private Transform m_LocationParent;

    [Header("Transforms")]
    public List<Transform> m_AllPivot;
    public Transform m_PinNext;
    public Transform m_PinCurrent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
        CreateLocationElement();
    }

    void Update()
    {
        m_PinNext.position = m_AllPivot[LocationManager.Instance.locationData.viewLocation - 1].position;
        m_PinCurrent.position = m_AllPivot[LocationManager.Instance.locationData.currentLocation - 1].position;
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    // DestroyLocationPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(State.UIState.MainPage);
                                                });
    }

    public override void UpdateUI()
    {
        // CreateLocationElement();
    }
    
    private void CreateLocationElement()
    {
        m_AllPivot = new List<Transform>();
        for (int i = 1; i < LocationManager.Instance.location.Count; i++)
        {
            LocationElement newObj = Instantiate(locationPrefab, m_LocationParent);
            newObj.SetProperty(i);
            m_AllPivot.Add(newObj.m_Pivot);
        }
    }

    private void DestroyLocationPrefab()
    {
        for (int i = 0; i < m_LocationParent.childCount; i++)
        {
            m_LocationParent.GetChild(i).GetComponent<LocationElement>().DestroySelf();
        }
    }

    private void OnClickFadeBtn()
    {
        Hide();
    }
}
