﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIProfilePage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_AgeText;
    [SerializeField] private TextMeshProUGUI m_CoolText;
    [SerializeField] private TextMeshProUGUI m_Cool2Text;
    [SerializeField] private TextMeshProUGUI m_JobHrText;
    [SerializeField] private TextMeshProUGUI m_JobSalaryText;
    [SerializeField] private TextMeshProUGUI m_JobNextSalaryText;
    [SerializeField] private TextMeshProUGUI m_JobKPIText;
    [SerializeField] private TextMeshProUGUI m_JobHealthText;
    [SerializeField] private TextMeshProUGUI m_JobHappyText;
    [SerializeField] private TextMeshProUGUI m_JobSpeedText;
    [SerializeField] private TextMeshProUGUI m_JobNextSpeedText;
    [SerializeField] private TextMeshProUGUI m_JobSkillText;
    [SerializeField] private TextMeshProUGUI m_GoalText;
    [SerializeField] private TextMeshProUGUI m_GoalHintText;
    [SerializeField] private TextMeshProUGUI m_DeptLVText;
    [SerializeField] private TextMeshProUGUI m_LiabilityText;
    [SerializeField] private TextMeshProUGUI m_IncomeText;
    [SerializeField] private TextMeshProUGUI m_MonthText;
    [SerializeField] private TextMeshProUGUI m_AssetText;
    [SerializeField] private TextMeshProUGUI m_CostText;

    [Header("Slider")]
    [SerializeField] private Slider m_KPISlider;
    [SerializeField] private Slider m_SkillSlider;
    [SerializeField] private Slider m_GoalSlider;

    [Header("GameObject")]
    [SerializeField] private GameObject m_StarPanel;
    [SerializeField] private Image m_ProfileImage;
    [SerializeField] private Sprite[] m_ProfileSprt;

    [Header("Prefabs")]
    [SerializeField] private StarUIElement m_Prefab;
    [SerializeField] private Transform m_Parent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;
    private float sleepTime;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {

    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(UIManager.Instance.previusState);
                                                });
    }

    public override void UpdateUI()
    {
        m_ProfileImage.sprite = m_PlayerManager.playerData.playerGender == "Male" ? m_ProfileSprt[0] : m_ProfileSprt[1];
        
        m_NameText.text = m_PlayerManager.playerData.playerName;
        m_AgeText.text = m_PlayerManager.playerData.age.ToString("N0") + " ปี";
        m_CoolText.text = m_PlayerManager.NetCool.ToString("N0");
        m_Cool2Text.text = m_PlayerManager.NetCool.ToString("N0");
        m_JobHrText.text = m_PlayerManager.jobData.fullTimeJob.workTimeHour.ToString("N0") + " ชม.";
        m_JobSalaryText.text = "เงินเดือน " + m_PlayerManager.jobData.fullTimeJob.salary.ToString("N0") + " ฿";

        Job nextJob = JobManager.Instance.Job[(int)State.JobType.FullTime].jobs[m_PlayerManager.jobData.fullTimeJob.jobID + 1];
        m_JobNextSalaryText.text = nextJob.startSalary.ToString("N0") + " ฿";
        m_JobKPIText.text = m_PlayerManager.jobData.completedTask.ToString("N0") + "/" + m_PlayerManager.jobData.fullTimeJob.kpi.ToString("N0");
        m_JobHealthText.text = m_PlayerManager.jobData.fullTimeJob.useHealth.ToString("N0");
        m_JobHappyText.text = m_PlayerManager.jobData.fullTimeJob.useHappy.ToString("N0");
        m_JobSpeedText.text = m_PlayerManager.playerData.lvSkill.JobSpeed.ToString("N0");
        m_JobNextSpeedText.text = (m_PlayerManager.playerData.lvSkill.JobSpeed + JobManager.Instance.upSkills.getSpeed).ToString("N0");
        m_JobSkillText.text = m_PlayerManager.playerData.lvSkill.currentEXP.ToString("N0") + "/" + JobManager.Instance.upSkills.maxExp.ToString("N0");

        m_KPISlider.maxValue = m_PlayerManager.jobData.fullTimeJob.kpi;
        m_KPISlider.value = m_PlayerManager.jobData.completedTask;

        m_SkillSlider.maxValue = JobManager.Instance.upSkills.maxExp;
        m_SkillSlider.value = m_PlayerManager.playerData.lvSkill.currentEXP;

        float Liability = 0;
        for (int i = 0; i < m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element.Count; i++)
        {
            Liability += (float)m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[i].installment;
        }
        for (int i = 0; i < m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element.Count; i++)
        {
            Liability += (float)m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element[i].installment;
        }

        float dept = Mathf.FloorToInt(Liability / m_PlayerManager.jobData.fullTimeJob.salary * 100);
        if (dept > 100)
        {
            dept = 100;
        }

        float Asset = 0;
        for (int i = 0; i < m_PlayerManager.accountData.account.Count; i++)
        {
            Asset += (float)m_PlayerManager.accountData.account[i].balance;
        }

        float Cost = ActionManager.Instance.dailyCost * CalManager.Instance.DAY_IN_MONTH;
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element.Count; i++)
        {
            Cost += m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element[i].rentPrice;
            Cost += m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element[i].fixedCost;
        }
        for (int i = 0; i < m_PlayerManager.liabilityData.Liability.Length; i++)
        {
            for (int j = 0; j < m_PlayerManager.liabilityData.Liability[i].element.Count; j++)
            {
                Cost += (float)m_PlayerManager.liabilityData.Liability[i].element[j].installment;
            }
        }
        
        float Goal = Cost * CalManager.Instance.RETIRE_MONTH;

        int Month = Mathf.FloorToInt(Asset / Cost);
        if (Month < 0)
        {
            Month = 0;
        }

        float NetWorth = (float)m_PlayerManager.netWorth;
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element.Count; i++)
        {
            NetWorth -= m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element[i].price;
        }
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)State.AssetType.car].element.Count; i++)
        {
            NetWorth -= m_PlayerManager.assetData.Asset[(int)State.AssetType.car].element[i].price;
        }
        if (NetWorth < 0)
        {
            NetWorth = 0;
        }

        m_GoalText.text = NetWorth.ToString("N0") + " ฿/" + Goal.ToString("N0") + " ฿";
        m_GoalHintText.text = "ค่าใช้จ่ายต่อเดือน " + Cost.ToString("N0") + " ฿ x จำนวนเดือนชีวิตหลังเกษียน " + CalManager.Instance.RETIRE_MONTH.ToString("N0") + " เดือน";
        m_GoalSlider.maxValue = Goal;
        m_GoalSlider.value = NetWorth;

        m_LiabilityText.text = Liability.ToString("N0") + " ฿";
        m_IncomeText.text = m_PlayerManager.jobData.fullTimeJob.salary.ToString("N0") + " ฿";
        m_DeptLVText.text = dept.ToString("N0") + "%";
        m_AssetText.text = Asset.ToString("N0") + " ฿";
        m_CostText.text = Cost.ToString("N0") + " ฿";
        m_MonthText.text = Month.ToString("N0");
    }

    public void OnClickStarBtn()
    {
        m_StarPanel.SetActive(true);
        DestroyElementPrefab();
        SpawnElementPrefab();
    }

    public void OnClickConfirmBtn()
    {
        Hide();
    }

    public void SpawnElementPrefab()
    {
        for (int i = 0; i < m_PlayerManager.missionData.saving.Length; i++)
        {
            StarUIElement prefab = Instantiate(m_Prefab, m_Parent);
            prefab.ShowElement(m_PlayerManager.missionData.saving[i].missionName, 
                m_PlayerManager.missionData.saving[i].missionStar, 
                m_PlayerManager.missionData.saving[i].isCompleted);
        }
        for (int i = 0; i < m_PlayerManager.missionData.wealth.Length; i++)
        {
            StarUIElement prefab = Instantiate(m_Prefab, m_Parent);
            prefab.ShowElement(m_PlayerManager.missionData.wealth[i].missionName, 
                m_PlayerManager.missionData.wealth[i].missionStar, 
                m_PlayerManager.missionData.wealth[i].isCompleted);
        }
        for (int i = 0; i < m_PlayerManager.missionData.invest.Length; i++)
        {
            StarUIElement prefab = Instantiate(m_Prefab, m_Parent);
            prefab.ShowElement(m_PlayerManager.missionData.invest[i].missionName, 
                m_PlayerManager.missionData.invest[i].missionStar, 
                m_PlayerManager.missionData.invest[i].isCompleted);
        }
    }

    private void DestroyElementPrefab()
    {
        for (int i = 1; i < m_Parent.childCount; i++)
        {
            m_Parent.GetChild(i).GetComponent<StarUIElement>().DestroySelf();
        }
    }
}
