﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIStorePage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_TitleText;

    [Header("Prefabs")]
    [SerializeField] private ActionElement m_StorePrefab;
    [SerializeField] private Transform m_StoreParent;

    [Header("ETC")]
    public State.Location locoState = State.Location.None;
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private List<Activity> action;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroyElementPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(UIManager.Instance.previusState);
                                                });
    }

    public override void UpdateUI()
    {
        UpdateStore(locoState);
    }

    public void UpdateStore(State.Location _Type, string[] _Action = null)
    {
        action = new List<Activity>();
        DestroyElementPrefab();
        if (_Type != State.Location.None && _Action != null)
        {
            for (int i = 0; i < _Action.Length; i++)
            {
                for (int j = 0; j < ActionManager.Instance.activity[(int)_Type].activity.Length; j++)
                {
                    if (_Action[i] == ActionManager.Instance.activity[(int)_Type].activity[j].activityType)
                    {
                        Activity newAction = ActionManager.Instance.activity[(int)_Type].activity[j];
                        action.Add(newAction);
                        break;
                    }
                }
            }
        }
        else
        {
            // _Type = State.Expense.Item;
            for (int i = 0; i < ActionManager.Instance.activity[(int)State.Expense.Item].activity.Length; i++)
            {
                Activity newAction = ActionManager.Instance.activity[(int)State.Expense.Item].activity[i];
                action.Add(newAction);
            }
            if (action != null)
            {
                for (int i = 0; i < action.Count; i ++)
                {
                    ActionElement newObj;
                    newObj = Instantiate(m_StorePrefab, m_StoreParent);
                    newObj.SetAction(action[i], (int)State.Expense.Item);
                }
            }
            action = new List<Activity>();
        }

        if (action != null)
        {
            for (int i = 0; i < action.Count; i ++)
            {
                ActionElement newObj;
                newObj = Instantiate(m_StorePrefab, m_StoreParent);
                newObj.SetAction(action[i], (int)_Type);
            }
        }
    }

    private void DestroyElementPrefab()
    {
        for (int i = 0; i < m_StoreParent.childCount; i++)
        {
            m_StoreParent.GetChild(i).GetComponent<ActionElement>().DestroySelf();
        }
    }

    public void OnClickFadeBtn()
    {
        Hide();
    }
}
