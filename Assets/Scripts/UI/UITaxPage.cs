﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UITaxPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_IncomeText;
    [SerializeField] private TextMeshProUGUI m_Income2Text;
    [SerializeField] private TextMeshProUGUI m_DiscountText;
    [SerializeField] private TextMeshProUGUI m_Discount2Text;
    [SerializeField] private TextMeshProUGUI m_NetIncomeText;
    [SerializeField] private TextMeshProUGUI m_CalTaxText;

    [Header("GameObjects")]
    [SerializeField] private GameObject m_IncomeObj;
    [SerializeField] private GameObject m_DeductionObj;

    [Header("Prefabs")]
    [SerializeField] private TaxUIElement m_CreditPrefab;
    [SerializeField] private Transform m_CreditParent;
    [SerializeField] private TaxDeductionUIElement m_DeductionPrefab;
    [SerializeField] private Transform m_DeductionParent;
    [SerializeField] private Transform m_IncomeParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {
    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroyElementPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(UIManager.Instance.previusState);
                                                });
    }

    public override void UpdateUI()
    {
        m_PlayerManager.CalculateTax();
        OnClickDeduction(false);
        OnClickIncome(false);
        UpdateTaxUI();
    }

    private void UpdateTaxUI()
    {
        DestroyElementPrefab();

        m_IncomeText.text = m_PlayerManager.taxData.totalIncome.ToString("N0") + " ฿";
        m_Income2Text.text = m_PlayerManager.taxData.totalIncome.ToString("N0") + " ฿";
        m_DiscountText.text = m_PlayerManager.taxData.totalDeduction.ToString("N0") + " ฿";
        m_Discount2Text.text = m_PlayerManager.taxData.totalDeduction.ToString("N0") + " ฿";
        m_NetIncomeText.text = m_PlayerManager.taxData.netIncome.ToString("N0") + " ฿";
        m_CalTaxText.text = m_PlayerManager.taxData.totalTax.ToString("N0") + " ฿";

        SpawnTaxElementPrefab();
    }

    public void OnClickIncome(bool _IsShow)
    {
        m_IncomeObj.SetActive(_IsShow);
        if (_IsShow)
        {
            SpawnIncomeElementPrefab();
        }
        else
        {
            UpdateTaxUI();
        }
    }

    public void OnClickDeduction(bool _IsShow)
    {
        m_DeductionObj.SetActive(_IsShow);
        if (_IsShow)
        {
            SpawnDeductionElementPrefab();
        }
        else
        {
            UpdateTaxUI();
        }
    }

    public void SpawnTaxElementPrefab()
    {
        for (int i = 1; i < m_PlayerManager.taxData.tax.Length; i++)
        {
            // if (i == 1 || m_PlayerManager.taxData.tax[i].taxFill > 0)
            // {
                int index = i;
                TaxUIElement creditFolder = Instantiate(m_CreditPrefab, m_CreditParent);
                creditFolder.ShowElement(index);
            // }
        }
    }

    public void SpawnIncomeElementPrefab()
    {
        for (int i = 0; i < m_PlayerManager.taxData.income.Length; i++)
        {
            int index = i;
            TaxDeductionUIElement deductionUIElement = Instantiate(m_DeductionPrefab, m_IncomeParent);
            deductionUIElement.ShowIncomeElement(index);
        }
    }

    public void SpawnDeductionElementPrefab()
    {
        for (int i = 0; i < m_PlayerManager.taxData.deduction.Length; i++)
        {
            int index = i;
            TaxDeductionUIElement deductionUIElement = Instantiate(m_DeductionPrefab, m_DeductionParent);
            deductionUIElement.ShowElement(index);
        }
    }

    private void DestroyElementPrefab()
    {
        for (int i = 1; i < m_CreditParent.childCount; i++)
        {
            m_CreditParent.GetChild(i).GetComponent<TaxUIElement>().DestroySelf();
        }
        for (int i = 0; i < m_IncomeParent.childCount; i++)
        {
            m_IncomeParent.GetChild(i).GetComponent<TaxDeductionUIElement>().DestroySelf();
        }
        for (int i = 0; i < m_DeductionParent.childCount; i++)
        {
            m_DeductionParent.GetChild(i).GetComponent<TaxDeductionUIElement>().DestroySelf();
        }
    }

    public void OnClickFadeBtn()
    {
        if (m_IncomeObj.activeSelf)
        {
            OnClickIncome(false);
        }
        else if (m_DeductionObj.activeSelf)
        {
            OnClickDeduction(false);
        }
        else
        {
            Hide();
        }
    }
}
