﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIElement : MonoBehaviour
{
    public virtual void Show(){}
    public virtual void Show(bool isForce, int index){}
    public virtual void Hide(){}
    public virtual void Hide(System.Action callback){}
    public virtual void UpdateUI(){}
    public virtual void UpdateUIPerYear(){}
    public virtual void OnClickOpenPageBtn(string uiState)
    {
        UIManager.Instance.SwitchState((State.UIState)Enum.Parse(typeof(State.UIState), uiState));
    }
}