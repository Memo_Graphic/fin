﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UILoanPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_IRText;
    [SerializeField] private TextMeshProUGUI m_AmountText;
    [SerializeField] private TextMeshProUGUI m_Amount2Text;
    [SerializeField] private TextMeshProUGUI m_YearText;
    [SerializeField] private TextMeshProUGUI m_InstallmentText;
    [SerializeField] private TextMeshProUGUI m_SumText;
    [SerializeField] private TextMeshProUGUI m_AllSumText;

    [Header("GameObject")]
    [SerializeField] private GameObject m_LoanObj;
    
    [Header("Prefabs")]
    [SerializeField] private LoanElement m_ElementPrefab;
    [SerializeField] private Transform m_ElementParent;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    private int currentAccount;
    private bool isBuy;
    private int year;
    private float loanAmount;
    private float installment;

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {

    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UpdateUI();
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    DestroyElementPrefab();
                                                    gameObject.SetActive(false);
                                                    UIManager.Instance.SwitchState(State.UIState.MainPage);
                                                });
    }

    public override void UpdateUI()
    {
        DestroyElementPrefab();
        InstantiatePrefabs();
    }

    public void UpdateLoanUI()
    {
        installment = loanAmount / ((1f - (1f / Mathf.Pow(1 + (BankManager.Instance.currentIR / 100f / CalManager.Instance.MONTH_IN_YEAR), year * CalManager.Instance.MONTH_IN_YEAR))) / (BankManager.Instance.currentIR / 100f / CalManager.Instance.MONTH_IN_YEAR));

        float Allsum = installment * year * CalManager.Instance.MONTH_IN_YEAR;

        m_IRText.text = "อัตราดอกเบี้ย " + BankManager.Instance.currentIR.ToString("N2") + "% /ปี";
        m_AmountText.text = loanAmount.ToString("N0") + " ฿";
        m_Amount2Text.text = loanAmount.ToString("N0") + " ฿";
        m_YearText.text = year.ToString("N0") + " ปี";

        m_InstallmentText.text = installment.ToString("N0") + " ฿";
        m_SumText.text = (Allsum - loanAmount).ToString("N0") + " ฿";
        m_AllSumText.text = Allsum.ToString("N0") + " ฿";
    }

    public void InstantiatePrefabs()
    {
        for (int i = 0; i < m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element.Count; i ++)
        {
            LoanElement newObj;
            newObj = Instantiate(m_ElementPrefab, m_ElementParent);
            newObj.SetLoan(i);
        }

        LoanElement noneObj;
        noneObj = Instantiate(m_ElementPrefab, m_ElementParent);
        noneObj.SetLoan(-1);
    }

    private void DestroyElementPrefab()
    {
        for (int i = 0; i < m_ElementParent.childCount; i++)
        {
            m_ElementParent.GetChild(i).GetComponent<LoanElement>().DestroySelf();
        }
    }
    
    public void OpenLoanPage()
    {
        year = 1;
        OnClickAmount(5000);
        m_LoanObj.SetActive(true);
    }

    public void OnClickAmount(float _Value)
    {   
        loanAmount = _Value;
        UpdateLoanUI();
    }

    public void OnClickChangeYear(int _Value)
    {   
        year += _Value;
        if (year > 5)
        {
            year = 5;
        }
        else if (year < 1)
        {
            year = 1;
        }
        UpdateLoanUI();
    }

    public void OnClickLoan()
    {
        m_PlayerManager.ApplyMoney(loanAmount, ()=> 
        {
            PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
            newLiability.name = "สินเชื่อส่วนบุคคล";
            newLiability.type = State.LiabilityType.Personal;
            newLiability.expenseType = State.Expense.ETC;
            newLiability.notPaid = false;
            newLiability.irPerMonth = BankManager.Instance.currentIR / 100f / CalManager.Instance.MONTH_IN_YEAR;
            newLiability.maxTerm = year * CalManager.Instance.MONTH_IN_YEAR;
            newLiability.term = newLiability.maxTerm;
            newLiability.downPayment = 0;
            newLiability.loanAmount = loanAmount - newLiability.downPayment;
            newLiability.maxAmount = newLiability.loanAmount;
            newLiability.installment = newLiability.loanAmount / ((1f - (1f / Mathf.Pow(1 + (newLiability.irPerMonth), newLiability.maxTerm))) / (newLiability.irPerMonth));
            
            BankManager.Instance.currentDeptID ++;
            newLiability.deptID = BankManager.Instance.currentDeptID;
            BankManager.Instance.AddLiability(newLiability);
        
            m_LoanObj.SetActive(false);
            UpdateUI();
        });
    }

    public void OnClickFadeBtn()
    {
        Hide();
    }
}