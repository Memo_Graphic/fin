﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using System;
using System.Collections;
using System.Collections.Generic;

public class UIPaymentPage : UIElement
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_Text;
    [SerializeField] private TextMeshProUGUI m_TextAcc0;
    [SerializeField] private TextMeshProUGUI m_TextCredit0;

    [Header("TMP - Details")]
    [SerializeField] private Image m_LogoImg;
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_HealthText;
    [SerializeField] private TextMeshProUGUI m_HappyText;
    [SerializeField] private TextMeshProUGUI m_CoolText;
    [SerializeField] private TextMeshProUGUI m_TimeText;
    [SerializeField] private TextMeshProUGUI m_PriceText;

    [Header("ETC")]
    public CanvasGroup canvasGroup;
    private PlayerManager m_PlayerManager;

    public float amount;
    public Activity action;
    public Action callbackYes;
    public Action callbackNo;


    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        m_PlayerManager = PlayerManager.Instance;
    }

    void Start()
    {

    }

    void Update()
    {
        
    }

    public override void Show()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                true,
                                                true,
                                                true,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    //
                                                });
    }

    public override void Hide()
    {
        UIUtilities.Instance.ToggleCanvasGroup(canvasGroup,
                                                Easing.Type.Linear,
                                                false,
                                                false,
                                                false,
                                                0.1f,
                                                0f,
                                                () =>
                                                {
                                                    gameObject.SetActive(false);
                                                });
    }

    public override void UpdateUI()
    {
        // m_Text.text = "Payment: " + amount + " Bath";
        // m_TextAcc0.text = m_PlayerManager.accountData.account[0].accountName + ": " + m_PlayerManager.accountData.account[0].balance.ToString("N0") + " ฿";
        // m_TextCredit0.text = "Credit Card: " + m_PlayerManager.liabilityData.creditCard[0].enoughCredit.ToString("N0") + " ฿";

        if (action.activityType != "Activity00")
        {
            m_NameText.text = "ชำระเงิน";
            m_HealthText.text = action.health.ToString("N0");
            m_HappyText.text = action.happiness.ToString("N0");
            m_CoolText.text = action.cool.ToString("N0");
            m_TimeText.text = CalManager.Instance.TextMinTime(action.timeCost, "HH");
            m_PriceText.text = action.moneyCost.ToString("N0") + " ฿";

            m_LogoImg.sprite = action.activityImage;
        }
    }

    public void SetPayment(float _Amount, Action _CallbackYes = null, Action _CallbackNo = null)
    {
        action = new Activity();
        amount = _Amount;
        callbackYes = _CallbackYes;
        callbackNo = _CallbackNo;

        UpdateUI();
    }

    public void SetPayment(Activity _Action, Action _CallbackYes = null, Action _CallbackNo = null)
    {
        action = new Activity();
        action = _Action;
        amount = action.moneyCost;
        callbackYes = _CallbackYes;
        callbackNo = _CallbackNo;

        UpdateUI();
    }

    public void Payment(int _accountID)
    {
        m_PlayerManager.SubtractMoney(_accountID, amount, ()=>
                                        {
                                            Hide();
                                            UpdateUI();
                                            callbackYes();
                                        }, ()=> callbackNo());
    }

    public void UseCredit(int _cardIndex)
    {
        m_PlayerManager.UseCredit(_cardIndex, amount, action.activity, ()=>
                                {
                                    Hide();
                                    UpdateUI();
                                    callbackYes();
                                }, ()=> callbackNo());
    }

    public void OnClickFadeBtn()
    {
        Hide();
    }
}