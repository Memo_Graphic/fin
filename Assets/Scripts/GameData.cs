[System.Serializable]
public class GameData
{
    public PlayerData playerData;
    public JobData jobData;
    public BalanceData balanceData;
    public AccountData accountData;
    public TaxData taxData;
    public BillData billData;
    public AssetData assetData;
    public LiabilityData liabilityData;
    public MissionData missionData;

    public HospitalData hospitalData;
    public LocationData locationData;
    public ScamData scamData;
    public LottoData lottoData;

    public State.Level gameLevel;
    public State.GameState currentState;
    public State.DayState currentDayState;

    public int realDay;
    public bool useCredit;

    public AssetElement[] Asset;
    public InsuranceElement[] insurances;
    public ActivityPlanner[] activityPlanner;
}
