﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateManager : Singleton<StateManager>
{
    public State.GameState currentState;
    public State.DayState currentDayState;
    public GameObject[] testShowObj;
    
    
    void Start()
    {
        
    }

    public void NextState()
    {
        SwitchState(currentState + 1);
    }

    public void SwitchState(State.GameState _state, bool _isUpdateState = true)
    {
        currentState = _state;
        if (_isUpdateState)
        {
            UpdateState();
        }
    }

    public void SwitchDayState(State.DayState _state, bool _isUpdateState = true)
    {
        currentDayState = _state;
        if (_isUpdateState)
        {
            UpdateDayState();
        }
    }

    public void UpdateState()
    {
        switch (currentState)
        {
            case State.GameState.Endless:
                {
                    CalManager.Instance.StartGame();
                    UIManager.Instance.Hide(State.UIState.TutorialPage);
                    Debug.Log("GameState Endless");
                }
                break;
            default:
                {
                    UIManager.Instance.UpdateUI(State.UIState.TutorialPage);
                    Debug.Log(currentState);
                }
                break;
        }
    }

    public void UpdateDayState()
    {
        switch (currentDayState)
        {
            case State.DayState.None:
                {
                    Debug.Log("Fix it dude");
                }
                break;
            case State.DayState.DailyEvent:
                {
                    CalManager.Instance.DailyEvent();
                }
                break;
            case State.DayState.DayStart:
                {
                    CalManager.Instance.DayStart();
                }
                break;
            case State.DayState.Day:
                {
                    CalManager.Instance.DayState();
                }
                break;
            case State.DayState.DayEnd:
                {
                    CalManager.Instance.DayEnd();
                }
                break;
            case State.DayState.Night:
                {
                    CalManager.Instance.Night();
                }
                break;
            default:
                {
                    if (currentDayState > State.DayState.Night)
                    {
                        SwitchDayState(State.DayState.DailyEvent);
                    }
                }
                break;
        }
        ShowScene();
    }

    private void ShowScene()
    {
        for (int i = 0; i < testShowObj.Length; i++)
        {
           testShowObj[i].SetActive(false);
        }
        
        // if (currentDayState == State.DayState.DayEnd)
        // {
        //     UIManager.Instance.SwitchState(State.UIState.MapPage);
        // }
        // else
        // {
        //     UIManager.Instance.uIElement[(int)State.UIState.MapPage].Hide();
        // }

        testShowObj[(int)currentDayState].SetActive(true);
        UIManager.Instance.uIElement[(int)State.UIState.MainPage].UpdateUI();
    }
}