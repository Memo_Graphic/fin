﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hellmade.Sound;

[System.Serializable]
public struct AudioControl
{
    public string audioName;
    public AudioClip audioClip;
}

public class SoundManager : Singleton<SoundManager>
{
    public bool isBGMOn = true;
    public bool isSFXOn = true;
    public float volumeMusic;
    public float volumeSound;
    public float volumeUISound;
    [SerializeField] private AudioControl m_BGM;
    [SerializeField] private AudioControl[] m_SoundFx;
    [SerializeField] private AudioControl[] m_SoundUI;

    void Awake()
    {

    }

    void Start()
    {
        SetVolume();

        if (isBGMOn)
            PlayMusic();
    }

    public void SetVolume()
    {
        EazySoundManager.GlobalMusicVolume = volumeMusic;
        EazySoundManager.GlobalSoundsVolume = volumeSound;
        EazySoundManager.GlobalUISoundsVolume = volumeUISound;
    }

    public int PlayMusic()
    {
        AudioControl audio = m_BGM;
        return EazySoundManager.PlayMusic(audio.audioClip, volumeMusic, true, false);
    }
    public void PlaySoundX(int _index)
    {
        PlaySound(_index);
    }
    public int PlaySound(int _index)
    {
        AudioControl audio = m_SoundFx[_index];
        return EazySoundManager.PlaySound(audio.audioClip, volumeSound);
    }
    public void PlayUISoundX(int _index)
    {
        PlayUISound(_index);
    }
    public int PlayUISound(int _index)
    {
        AudioControl audio = m_SoundUI[_index];
        return EazySoundManager.PlayUISound(audio.audioClip, volumeUISound);
    }

    public void StopMusic(float _duration)
    {
        EazySoundManager.StopAllMusic(_duration);
    }
    public void StopSound()
    {
        EazySoundManager.StopAllSounds();
    }
    public void StopUISound()
    {
        EazySoundManager.StopAllUISounds();
    }

    public void StopAll(float _duration)
    {
        EazySoundManager.StopAll(_duration);
    }
}