﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct JobData
{
    [UnityEngine.Header("Current Job")]
    public float currenTask;
    public float fullTimeTask;
    public float completedTask;
    public PlayerJobElement fullTimeJob;
    public PlayerJobElement partTimeJob;
}

[System.Serializable]
public class PlayerJobElement
{
    public int jobID = 0;
    public int dailyWorked = 0;
    public int dailyFinish = 0;
    public string jobName
    {
        get 
        {
            return JobManager.Instance.Job[(int)type].jobs[jobID].jobName;
        }
    }
    public int location
    {
        get 
        {
            return JobManager.Instance.Job[(int)type].jobs[jobID].location;
        }
    }
    public int workTime
    {
        get 
        {
            if (jobID >= 0)
                return (int)(JobManager.Instance.Job[(int)type].jobs[jobID].workTime * CalManager.Instance.MIN_IN_HOUR);
            else
                return 0;
        }
    }
    public int workTimeHour
    {
        get 
        {
            if (jobID >= 0)
                return (int)(JobManager.Instance.Job[(int)type].jobs[jobID].workTime);
            else
                return 0;
        }
    }
    public State.JobType type;
    public State.Skill skill;
    public float salary;
    public float useHealth
        {
        get 
        {
            if (jobID >= 0)
            {

                return (int)(JobManager.Instance.useHealth + (jobID));
            }
            else
                return 0;
        }
    }
    public float useHappy
        {
        get 
        {
            if (jobID >= 0)
            {
                return (int)(JobManager.Instance.useHappy + (jobID * 2));
            }
            else
                return 0;
        }
    }
    public int workpoint
        {
        get 
        {
            if (jobID >= 0)
            {
                return (int)(JobManager.Instance.workpoint + (jobID * 10));
            }
            else
                return 0;
        }
    }
    public int kpi
        {
        get 
        {
            if (jobID >= 0)
                return (int)(JobManager.Instance.kpi + (jobID * 5));
            else
                return 0;
        }
    }
}

[System.Serializable]
public class JobManagerElement
{
    public string typeName = "typeName";
    public float exp;
    public Job[] jobs;
}

[System.Serializable]
public class Job
{
    [Header("Job")]
    public string jobName = "jobName";
    public State.JobType type;
    public string locationType;
    public int location
    {
        get 
        {
            for (int i = 0; i < LocationManager.Instance.location.Count; i++)
            {
                int index = i;
                if (locationType == LocationManager.Instance.location[index].locationType)
                {
                    return index;
                }
            }
            Debug.LogWarning("Asset: " + jobName + " location Type Error");
            return 0;
        }
    }
    public float workTime;
    public float startSalary;
}