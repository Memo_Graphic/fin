using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UpSkillUIElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_CostText;
    [SerializeField] private TextMeshProUGUI m_TimeText;
    [SerializeField] private TextMeshProUGUI m_ExpText;

    [Header("Buttons")]
    [SerializeField] private Button m_LearnBtn;


    private UpSkillElement m_Skill;
    private UISkillPage m_UI;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSkill(UpSkillElement _Skill, UISkillPage _UI)
    {
        m_Skill = _Skill;
        m_UI = _UI;
        UpdateUI();
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }

    private void UpdateUI()
    {
        // m_NameText.text = "ชื่อคอร์ส: " + m_Skill.skillName;
        // m_CostText.text = "ค่าใช้จ่าย: " + m_Skill.cost.ToString("N0") + " ฿";
        // m_TimeText.text = "ระยะเวลาเรียน: " + CalManager.Instance.TextMinTime(m_Skill.learnTime, "HH");
        // m_ExpText.text = "ได้รับความรู้: " + m_Skill.exp;
    }

    public void OnClickLearnBtn()
    {
        // UIManager.Instance.Show(State.UIState.PaymentPage);
        // UIPaymentPage patmentPage = UIManager.Instance.uIElement[(int)State.UIState.PaymentPage].GetComponent<UIPaymentPage>();
        // patmentPage.GetComponent<UIPaymentPage>().SetPayment(m_Skill.cost, ()=>
        //                                                     {
        //                                                         PlayerManager.Instance.SubtractTime(m_Skill.learnTime);

        //                                                         PlayerBalanceElement balance = new PlayerBalanceElement();
        //                                                         balance.balanceName = m_Skill.skillName;
        //                                                         balance.amount = m_Skill.cost;
        //                                                         PlayerManager.Instance.RecordBalance(balance, State.Expense.UpSkill);
                                                                
        //                                                         JobManager.Instance.UpSkill(m_Skill.exp);

        //                                                         UIManager.Instance.UpdateUI(State.UIState.MainPage);
        //                                                         m_UI.UpdateEXP();
        //                                                     }, ()=>
        //                                                     {
                                                                
        //                                                     });
    }
}
