using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SkillUIElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_ExpText;


    private string skill = "skillName";
    private float exp;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSkill(string _Skill, float _EXP)
    {
        skill = _Skill;
        exp = _EXP;

        UpdateUI();
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }

    private void UpdateUI()
    {
        m_NameText.text = skill;
        m_ExpText.text = "EXP: " + exp.ToString();
    }
}

