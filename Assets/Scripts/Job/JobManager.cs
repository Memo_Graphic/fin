﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JobManager : Singleton<JobManager>
{
    public State.JobType allType;
    public float salaryCost;
    public float useHealth;
    public float useHappy;
    public int workpoint;
    public int kpi;
    public JobManagerElement[] Job;
    public UpSkillElement upSkills;
    
    private PlayerManager m_PlayerManager;
    

    // Start is called before the first frame update
    void Start()
    {
        m_PlayerManager = PlayerManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public PlayerJobElement ConvertJob(Job _Job)
    {
        for (int i = 0; i < Job[(int)State.JobType.FullTime].jobs.Length; i++)
        {
            if (_Job.jobName == Job[(int)State.JobType.FullTime].jobs[i].jobName)
            {
                int index = i;
                PlayerJobElement newJob = new PlayerJobElement();
                newJob.jobID = index;
                newJob.type = _Job.type;
                newJob.salary = _Job.startSalary;

                return newJob;
            }
        }
        Debug.LogWarning("Fixed Jobs");
        return null;
    }

    public PlayerJobElement ConvertPartTime(Job _Job)
    {
        for (int i = 0; i < Job[(int)State.JobType.PartTime].jobs.Length; i++)
        {
            if (_Job.jobName == Job[(int)State.JobType.PartTime].jobs[i].jobName)
            {
                int index = i;
                PlayerJobElement newJob = new PlayerJobElement();
                newJob.jobID = index;
                newJob.type = _Job.type;
                newJob.salary = _Job.startSalary;

                return newJob;
            }
        }
        Debug.LogWarning("Fixed Jobs");
        return null;
    }

    public void UpWorkExp(int _Index, float _Value)
    {
        Job[_Index].exp += _Value;
    }

    public void ApplyFullTime(Job _Job)
    {
        PlayerJobElement m_Job = ConvertJob(_Job);
        m_PlayerManager.jobData.fullTimeJob = m_Job;

        Debug.Log("Apply FullTime " + m_Job.jobName);

        LocationManager.Instance.SetJobLocation(m_PlayerManager.jobData.fullTimeJob.location);
    }

    public void ResignFullTime()
    {
        m_PlayerManager.jobData.fullTimeJob.jobID = -1;
        m_PlayerManager.jobData.fullTimeJob.type = State.JobType.FullTime;
        m_PlayerManager.jobData.fullTimeJob.salary = 0;
        LocationManager.Instance.SetJobLocation(0);

        Debug.Log("Resign FullTime");
    }

    public void ApplyPartTime(Job _Job)
    {
        PlayerJobElement m_Job = ConvertPartTime(_Job);
        m_PlayerManager.jobData.partTimeJob = m_Job;

        Debug.Log("Apply PartTime " + m_Job.jobName);
    }

    public void ResignPartTime()
    {
        m_PlayerManager.jobData.partTimeJob.jobID = -1;
        m_PlayerManager.jobData.partTimeJob.type = State.JobType.PartTime;
        m_PlayerManager.jobData.partTimeJob.salary = 0;
        LocationManager.Instance.SetJobLocation(0);

        Debug.Log("Resign PartTime");
    }

    public void LearnSkill()
    {
        if (StateManager.Instance.currentState == State.GameState.Endless)
        {
            if (m_PlayerManager.playerData.Time.minute >= upSkills.learnTime
            && upSkills.useHealth <= m_PlayerManager.playerData.health
            && upSkills.useHealth <= m_PlayerManager.playerData.happiness)
            {
                float income = Mathf.Floor(m_PlayerManager.jobData.fullTimeJob.salary / 50);

                m_PlayerManager.SubtractTime(upSkills.learnTime);
                m_PlayerManager.ApplyHealth(upSkills.useHealth * -1);
                m_PlayerManager.ApplyHappiness(upSkills.useHealth * -1);

                m_PlayerManager.playerData.lvSkill.currentEXP += upSkills.getExp;

                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.GetSkill);
            }
            else if (m_PlayerManager.playerData.Time.minute < upSkills.learnTime)
            {
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.TimeLimit);
            }
            else if (upSkills.useHealth > m_PlayerManager.playerData.health
            || upSkills.useHealth > m_PlayerManager.playerData.happiness)
            {
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.PowerLimit);
            }
        }
        else
        {
            float income = Mathf.Floor(m_PlayerManager.jobData.fullTimeJob.salary / 50);

            m_PlayerManager.SubtractTime(upSkills.learnTime);
            m_PlayerManager.ApplyHealth(upSkills.useHealth * -1);
            m_PlayerManager.ApplyHappiness(upSkills.useHealth * -1);

            m_PlayerManager.playerData.lvSkill.currentEXP += upSkills.getExp;
            
            StateManager.Instance.NextState();
        }

        if (m_PlayerManager.playerData.lvSkill.currentEXP >= upSkills.maxExp)
        {
            upSkills.maxExp += Mathf.RoundToInt(upSkills.maxExp * 0.2f);
            m_PlayerManager.playerData.lvSkill.JobSpeed += upSkills.getSpeed;
            m_PlayerManager.playerData.lvSkill.currentEXP = 0;
        }
    }

    public void WorkPartTime()
    {
        if (StateManager.Instance.currentState == State.GameState.Endless)
        {
            if (m_PlayerManager.playerData.Time.minute >= CalManager.Instance.MIN_IN_HOUR
            && m_PlayerManager.jobData.fullTimeJob.useHealth <= m_PlayerManager.playerData.health
            && m_PlayerManager.jobData.fullTimeJob.useHappy <= m_PlayerManager.playerData.happiness)
            {
                float income = Mathf.Floor(m_PlayerManager.jobData.fullTimeJob.salary / 50);
                m_PlayerManager.SubtractTime(CalManager.Instance.MIN_IN_HOUR);
                m_PlayerManager.ApplyHealth(m_PlayerManager.jobData.fullTimeJob.useHealth * -1);
                m_PlayerManager.ApplyHappiness(m_PlayerManager.jobData.fullTimeJob.useHappy * -1);
                
                m_PlayerManager.ApplyMoney(income);
                m_PlayerManager.ApplyTaxIncome(income, 1);
            }
            else if (m_PlayerManager.playerData.Time.minute < CalManager.Instance.MIN_IN_HOUR)
            {
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.TimeLimit);
            }
            else if (m_PlayerManager.jobData.fullTimeJob.useHealth > m_PlayerManager.playerData.health
            || m_PlayerManager.jobData.fullTimeJob.useHappy > m_PlayerManager.playerData.happiness)
            {
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.PowerLimit);
            } 
        }
        else
        {
            float income = Mathf.Floor(m_PlayerManager.jobData.fullTimeJob.salary / 50);
            m_PlayerManager.SubtractTime(CalManager.Instance.MIN_IN_HOUR);
            m_PlayerManager.ApplyHealth(m_PlayerManager.jobData.fullTimeJob.useHealth * -1);
            m_PlayerManager.ApplyHappiness(m_PlayerManager.jobData.fullTimeJob.useHappy * -1);
            
            m_PlayerManager.ApplyMoney(income);
            m_PlayerManager.ApplyTaxIncome(income, 1);

            StateManager.Instance.NextState();
        }
    }

    public void WorkFullTime()
    {
        Activity action = ActionManager.Instance.FindActivity("WorkNormal", State.Expense.Work);
        UpWorkExp((int)m_PlayerManager.jobData.fullTimeJob.type, m_PlayerManager.jobData.fullTimeJob.workTimeHour);

        LocationManager.Instance.NextTo((int)LocationManager.Instance.locationData.currentJobLo);
        m_PlayerManager.SubtractTime(m_PlayerManager.jobData.fullTimeJob.workTime);

        for (int i = 0; i < m_PlayerManager.jobData.fullTimeJob.workTimeHour; i ++)
        {
            ActionManager.Instance.DoAction("WorkNormal", State.Expense.Work);
        }
    }
}
