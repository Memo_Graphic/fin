using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class JobApplyElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private TextMeshProUGUI m_SalaryText;
    [SerializeField] private TextMeshProUGUI m_WorkTimeText;
    [SerializeField] private TextMeshProUGUI m_JobText;
    [SerializeField] private TextMeshProUGUI m_SkillText;
    [SerializeField] private TextMeshProUGUI m_AgeText;
    

    [Header("Button")]
    [SerializeField] private Button m_ComfirmBtn;


    private Job m_Job;
    private bool isFullTime = true;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetJob(Job _Job, bool _IsFullTime)
    {
        m_Job = _Job;
        isFullTime = _IsFullTime;

        // m_ComfirmBtn.onClick.AddListener(() =>
        // {
        //     ApplyJob();
        // });

        UpdateUI();
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }

    private void UpdateUI()
    {
        m_NameText.text = "ชื่องาน: " + m_Job.jobName;
        if (isFullTime)
        {
            m_SalaryText.text = "เงินเดือน: " + m_Job.startSalary.ToString("N0");
        }
        else
        {
            m_SalaryText.text = "ค่าตอบแทน: " + m_Job.startSalary.ToString("N0");
        }
        m_WorkTimeText.text = "ระยะเวลา: " + CalManager.Instance.TextMinTime((int)m_Job.workTime * CalManager.Instance.MIN_IN_HOUR, "HH");

        SetComfirmBtn();
    }

    private void SetComfirmBtn()
    {
        m_ComfirmBtn.onClick.RemoveAllListeners();
        m_ComfirmBtn.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "สมัครงาน";

        if (isFullTime)
        {
            if (PlayerManager.Instance.haveFullTime())
            {
                m_ComfirmBtn.interactable = false;
                m_ComfirmBtn.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "ไม่ผ่านเกณฑ์";
                if (PlayerManager.Instance.haveFullTime(m_Job.jobName))
                {
                    m_ComfirmBtn.interactable = true;
                    m_ComfirmBtn.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "ลาออก";

                    m_ComfirmBtn.onClick.AddListener(() =>
                    {
                        JobManager.Instance.ResignFullTime();
                        UIManager.Instance.UpdateCurrentUI();
                    });
                }
            }
            else
            {
                m_ComfirmBtn.onClick.AddListener(() =>
                {
                    JobManager.Instance.ApplyFullTime(m_Job);
                    UIManager.Instance.UpdateCurrentUI();
                });
            }
        }
        else
        {
            if (PlayerManager.Instance.havePartTime())
            {
                m_ComfirmBtn.interactable = false;
                m_ComfirmBtn.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "ไม่ผ่านเกณฑ์";
                if (PlayerManager.Instance.havePartTime(m_Job.jobName))
                {
                    m_ComfirmBtn.interactable = true;
                    m_ComfirmBtn.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "ลาออก";

                    m_ComfirmBtn.onClick.AddListener(() =>
                    {
                        JobManager.Instance.ResignPartTime();
                        UIManager.Instance.UpdateCurrentUI();
                    });
                }
            }
            else
            {
                m_ComfirmBtn.onClick.AddListener(() =>
                {
                    JobManager.Instance.ApplyPartTime(m_Job);
                    UIManager.Instance.UpdateCurrentUI();
                });
            }
        }
    }
}
