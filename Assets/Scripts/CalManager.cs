﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalManager : Singleton<CalManager>
{
    [UnityEngine.Header("Fixed Variable")]
    public int MIN_IN_DAY;
    public int MIN_IN_HOUR;
    // public int DAY_IN_REAL;
    public int DAY_START;
    public int DAY_IN_WEEK;
    public int WEEK_IN_MONTH;
    public int MONTH_IN_YEAR;
    public int DAY_IN_MONTH
    {
        get
        {
            return DAY_IN_WEEK * WEEK_IN_MONTH;
        }
    }
    public int DAY_IN_YEAR
    {
        get
        {
            return DAY_IN_WEEK * WEEK_IN_MONTH * MONTH_IN_YEAR;
        }
    }
    public int RETIRE_YEAR
    {
        get
        {
            return PlayerManager.Instance.playerData.lifespan - PlayerManager.Instance.playerData.retire;
        }
    }
    public int RETIRE_MONTH
    {
        get
        {
            return RETIRE_YEAR * MONTH_IN_YEAR;
        }
    }
    public int SCAM_SPAWN_DATE;
    public int SCAM_LENGHT_DATE;
    public int SCAM_MISSING_TIME;
    public int HOSPITAL_SPAWN_DATE;
    public int HOSPITAL_MISSING_TIME;
    // public int REALDAY_IN_YEAR
    // {
    //     get
    //     {
    //         return DAY_IN_REAL * DAY_IN_WEEK * WEEK_IN_MONTH * MONTH_IN_YEAR;
    //     }
    // }

    [UnityEngine.Header("DateTime")]
    public int realDay;
    public int daySalary = 4;
    public int[] dayOff;
    public int Day(int _RealDay = 0)
    {
        if(_RealDay == 0)
        {
            _RealDay = realDay;
        }

        if (_RealDay % DAY_IN_WEEK != 0)
        {
            return (_RealDay % DAY_IN_WEEK) + (DAY_IN_WEEK * (Week(_RealDay) - 1));
        }
        else
        {
            return DAY_IN_WEEK + (DAY_IN_WEEK * (Week(_RealDay) - 1));
        }
    }
    public int Week(int _RealDay = 0)
    {
        if(_RealDay == 0)
        {
            _RealDay = realDay;
        }

        int allWeek = Mathf.CeilToInt((float)_RealDay / (float)DAY_IN_WEEK);
        if (allWeek % WEEK_IN_MONTH != 0)
        {
            return allWeek % WEEK_IN_MONTH;
        }
        else
        {
            return WEEK_IN_MONTH;
        }
    }
    public int Month(int _RealDay = 0)
    {
        if(_RealDay == 0)
        {
            _RealDay = realDay;
        }

        int allMonth = Mathf.CeilToInt((float)_RealDay / (float)DAY_IN_MONTH);
        if (allMonth % MONTH_IN_YEAR != 0)
        {
            return allMonth % MONTH_IN_YEAR;
        }
        else
        {
            return MONTH_IN_YEAR;
        }
    }
    public int Year(int _RealDay = 0)
    {
        if(_RealDay == 0)
        {
            _RealDay = realDay;
        }

        return Mathf.CeilToInt((float)_RealDay / (float)DAY_IN_YEAR) + 20;
    }
    public bool isDayOff
    {
        get
        {
            for (int i = 0; i < dayOff.Length; i++)
            {
                if (Day() == dayOff[i])
                {
                    return true;
                }
            }
            return false;
        }
    }

    public string DateTime(string type = "", int _RealDay = 0)
    {
        if (_RealDay == 0)
        {
            _RealDay = realDay;
        }
        string dateTime = "";

        if (type == "" || type == "DD/MM/YYYY")
        {
            dateTime = Day(_RealDay).ToString("00") + "/" + Month(_RealDay).ToString("00") + "/" + (Year(_RealDay) + 2000).ToString("0000");
        }
        else if (type == "" || type == "DD/MM/YY")
        {
            dateTime = Day(_RealDay).ToString("00") + "/" + Month(_RealDay).ToString("00") + "/" + Year(_RealDay).ToString("00");
        }
        else if (type == "MM/YYYY")
        {
            dateTime = Month(_RealDay).ToString("00") + "/" + (Year(_RealDay) + 2000).ToString("0000");
        }
        else
        {
            dateTime = Day(_RealDay).ToString("00") + "/" + Month(_RealDay).ToString("00") + "/" + (Year(_RealDay) + 2000).ToString("0000");
        }

        return dateTime;
    }
    
    public string TextMinTime(int _Min, string type)
    {
        string strTime = "";
        int currenTime = _Min;

        if (type == "" || type == "MM")
        {
            strTime = currenTime.ToString() + " นาที";
        }
        else if (type == "HH:MM")
        {
            int hour = (int)Mathf.Floor((float)currenTime / (float)CalManager.Instance.MIN_IN_HOUR);
            int minute = currenTime - (hour * CalManager.Instance.MIN_IN_HOUR);
            strTime = hour.ToString("00") + " ชม. " + minute.ToString("00") + " น.";
        }
        else if (type == "HHMM")
        {
            int hour = (int)Mathf.Floor((float)currenTime / (float)CalManager.Instance.MIN_IN_HOUR);
            int minute = currenTime - (hour * CalManager.Instance.MIN_IN_HOUR);
            strTime = hour.ToString("00") + ":" + minute.ToString("00") + " ชม.";
        }
        else if (type == "HH")
        {
            int hour = (int)Mathf.Floor((float)currenTime / (float)CalManager.Instance.MIN_IN_HOUR);
            int minute = currenTime - (hour * CalManager.Instance.MIN_IN_HOUR);
            if (hour > 0 && minute > 0)
            {
                strTime = hour.ToString("N0") + " ชม. " + minute.ToString("N0") + " น.";
            }
            else if (hour > 0 && minute <= 0)
            {
                strTime = hour.ToString("N0") + " ชั่วโมง";
            }
            else if (hour <= 0)
            {
                strTime = minute.ToString("N0") + " นาที";
            }
        }
        else if (type == "HH2")
        {
            int hour = (int)Mathf.Floor((float)currenTime / (float)CalManager.Instance.MIN_IN_HOUR);
            int minute = currenTime - (hour * CalManager.Instance.MIN_IN_HOUR);
            if (hour > 0)
            {
                strTime = hour.ToString("N0") + " ชม.";
            }
            else if (hour <= 0)
            {
                strTime = minute.ToString("N0") + " น.";
            }
        }

        return strTime;
    }

    [UnityEngine.Header("StarterValue")]
    public float startMoney = 5000f;
    public string[] startHome = {"Asset00", "Asset01"};
    public string startCard = "Card00";


    private PlayerManager m_PlayerManager;
    private StateManager m_StateManager;

    
    void Start()
    {
        m_PlayerManager = PlayerManager.Instance;
        m_StateManager = StateManager.Instance;

        if (SaveManager.Instance.IS_LOAD && SaveManager.Instance.IsHaveGameData())
        {
            SaveManager.Instance.LoadGameData();
        }
        else
        {
            if (!GameManager.Instance.isTutorial && m_StateManager.currentState == State.GameState.Load)
            {
                // LoadGame();
                FirstGameStart();
            }
        }
    }

    private void FirstGameStart()
    {
        if (realDay <= DAY_START)
        {
            // StartGame();
            GameManager.Instance.isTutorial = true;
            UIManager.Instance.Show(State.UIState.CreateCharPage);
        }
        else
        {
            
        }
    }

    public void StartGame()
    {
        GameManager.Instance.isTutorial = false;
        UIManager.Instance.Hide(State.UIState.TutorialPage);
        // GameStart();
    }

    public void SetStarterValue()
    {
        // StartGame();

        // Set Money
        m_PlayerManager.accountData.account[0].balance = startMoney;

        // Set Job
        // JobManager.Instance.ApplyFullTime(JobManager.Instance.Job[(int)State.JobType.FullTime].jobs[0]);

        // Set Home
        // AssetManager.Instance.RentEstate(AssetManager.Instance.FindAsset(startHome, State.AssetType.estate));
        // AssetManager.Instance.UseAsset(startHome, State.AssetType.estate);

        // AssetManager.Instance.AddAsset("Car01", State.AssetType.car);

        BankManager.Instance.AddCreditCard(startCard);
    }

    private void GameStart()
    {
        NextDay();
        Daily();
        m_StateManager.SwitchDayState(State.DayState.Night);
    }

    private void NextDay()
    {
        realDay += 1;

        m_PlayerManager.playerData.Time.minute = MIN_IN_DAY;
        
        if (Day() == 1)
        {
            WeeklyEvent();
            if (Week() == 1)
            {
                MonthlyEvent();
                if (Month() == 1)
                {
                    YearlyEvent();
                }
            }
        }
        else if (Day() == DAY_IN_WEEK + 1)
        {
            WeeklyEvent();
        }
    }

    public void DailyEvent()
    {
        //ช่วงเริ่มวัน
        NextDay();
        Daily();
        LocationManager.Instance.MoveTo((int)LocationManager.Instance.locationData.currentHomeLo);
    }

    public void Daily()
    {
        AssetGrowth();
        AssetStockGrowth();
    }

    public void DayStart()
    {
        //ช่วงเช้าของวัน
    }

    public void DayState()
    {
        //ระหว่างวัน //ทำงาน
    }

    public void DayEnd()
    {
        //หลังเลิกงาน
        CalSalary();

        if (ActionManager.Instance.hospitalData.currentHospital == State.Hospital.None)
        {
            if (m_PlayerManager.jobData.fullTimeJob.dailyFinish > 0)
            {
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.TaskComplete);
            }
            else
            {
                if (!GameManager.Instance.isTutorial && m_PlayerManager.jobData.fullTimeJob.dailyWorked 
                < m_PlayerManager.jobData.fullTimeJob.workTimeHour * m_PlayerManager.playerData.lvSkill.JobSpeed * 0.3f)
                {
                    UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.TaskSlow);
                }
            }
        }
        
        if (Day() == 1 && Week() == 1)
        {
            if (Month() == 1 || Month() == 6)
            {
                PayAccruedExpenses();
            }
        }

        BankManager.Instance.GenerateLotto();
    }

    public void Night()
    {
        //หลังกลับบ้าน
        BillDeadline();
        CalInsurance();
        CalTax();
        CalBill();
        Scamming();
        // GenUIBill();

        CalCreditInterest();
        CalDepositInterest();
        AssetStockGrowth();

        LocationManager.Instance.MoveTo((int)LocationManager.Instance.locationData.currentHomeLo);
        m_PlayerManager.playerData.health = 0;
        m_PlayerManager.playerData.happiness = 0;

        if (StateManager.Instance.currentState == State.GameState.Endless)
        {
            SaveManager.Instance.SaveGameData();
        }
    }

    private void WeeklyEvent()
    {
        // Test ระบบดอกเบี้ยเงินฝากแบบ รายสัปดาห์
        // PayAccruedExpenses();
    }

    private void MonthlyEvent()
    {
        BalanceTypeElement mBalance = new BalanceTypeElement();
        mBalance.balanceType = new List<BalanceElement>();
        for (int i = 0; i < (int)State.Expense.LENGTH; i ++)
        {
            BalanceElement newBalanceElement = new BalanceElement();
            newBalanceElement.expense = new List<PlayerBalanceElement>();
            newBalanceElement.expenseName = ((State.Expense)i).ToString();
            mBalance.balanceType.Add(newBalanceElement);
        }
        m_PlayerManager.balanceData.balanceElement.Add(mBalance);
    }

    private void YearlyEvent()
    {
        m_PlayerManager.playerData.age += 1;
        
        if (realDay > 1)
        {
            // AssetPersonalGrowth();
        }
    }

    private void PayAccruedExpenses()
    {
        // for (int i = 1; i < m_PlayerManager.accountData.account.Count; i ++)
        // {
        //     if (m_PlayerManager.accountData.account[i].accruedExpenses != 0)
        //     {
        //         UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.GetInterest);
        //         m_PlayerManager.ApplyMoney(i, (float)m_PlayerManager.accountData.account[i].accruedExpenses,
        //                                     ()=> 
        //                                     {
        //                                         m_PlayerManager.accountData.account[i].accruedExpenses = 0;
        //                                     });
        //     }
        // }

        if (m_PlayerManager.accountData.account[1].accruedExpenses != 0)
        {
            UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.GetInterest);
            m_PlayerManager.ApplyMoney(1, (float)m_PlayerManager.accountData.account[1].accruedExpenses,
                                        ()=> 
                                        {
                                            m_PlayerManager.accountData.account[1].accruedExpenses = 0;
                                        });
        }
    }

    private void CalSalary()
    {
        if (m_PlayerManager.haveFullTime() && Day() == daySalary)
        {
            m_PlayerManager.ApplyMoney(m_PlayerManager.jobData.fullTimeJob.salary - JobManager.Instance.salaryCost);
            m_PlayerManager.ApplyTaxIncome(m_PlayerManager.jobData.fullTimeJob.salary);
            
            UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.GetSalaly);
        }
    }

    private void CalTax()
    {
        m_PlayerManager.CalculateTax();
    }

    public void CalTaxBill()
    {
        Debug.Log("Tax Bill");
        // Debug.Log(m_PlayerManager.taxData.totalTax);
        // Debug.Log(m_PlayerManager.taxData.netIncome);
        // Debug.Log(m_PlayerManager.taxData.totalIncome);
        // Debug.Log(m_PlayerManager.taxData.totalDeduction);
        
        PlayerBillElement newBill = new PlayerBillElement();
        newBill.billName = "ภาษี";
        newBill.billName += " (" + DateTime("MM/YYYY") + ")";
        newBill.amount = m_PlayerManager.taxData.totalTax;
        newBill.expenseType = State.Expense.ETC;
        newBill.billType = State.BillType.Tax;

        newBill.deadline = realDay + (DAY_IN_MONTH - 1);
        m_PlayerManager.billData.bill.Add(newBill);

        m_PlayerManager.ResetTax();
    }

    private void CalBill()
    {
        if (Day() == 1 && Week() == 1 && realDay > 1)
        {
            // Fixed cost from Asset
            CalFixedCostBill();

            // Rent Bill
            CalRentBill();

            // Liab bill
            CalLiabilityBill();

            if (Month() == 1)
            {
                // Tax Bill
                CalTaxBill();
            }
        }
    }

    private void BillDeadline()
    {
        for (int i = m_PlayerManager.billData.bill.Count - 1; i >= 0; i --)
        {
            if (m_PlayerManager.billData.bill[i].deadline < realDay)
            {
                Debug.Log("End Payment: " + i + " | " + m_PlayerManager.billData.bill[i].billName);
                m_PlayerManager.billData.bill[i].CallbackNo();
                
                // if (m_PlayerManager.billData.bill[i].CallbackNo != null)
                // {
                //     m_PlayerManager.billData.bill[i].CallbackNo();
                // }
                // PlayerManager.Instance.billData.bill.RemoveAt(i);
            }
        }
    }

    private void GenUIBill()
    {
        // if (Day() == 1 && Week() == 1 && realDay > 1)
        // {
            if (m_PlayerManager.billData.bill.Count > 0)
            {
                UIManager.Instance.SwitchState(State.UIState.BillPage);
            }
        // }
    }

    private void CalFixedCostBill()
    {
        // Estate
        for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element.Count; i++)
        {
            int index = i;
            if (m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element[index].fixedCost > 0)
            {
                PlayerBillElement newBill = new PlayerBillElement();
                newBill.billName = "ค่าสาธารณูปโภค";
                // newBill.billName += m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element[index].assetName;
                newBill.billName += " (" + DateTime("MM/YYYY") + ")";
                newBill.amount = m_PlayerManager.assetData.Asset[(int)State.AssetType.estate].element[index].fixedCost;
                newBill.expenseType = State.Expense.HomeFixedCost;
                newBill.billType = State.BillType.FixedCost;
                newBill.index = index;

                newBill.deadline = realDay + (DAY_IN_MONTH - 1);

                // newBill.CallbackYes = delegate
                // {
                //     if (newBill.deptID > 0)
                //     {
                //         int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element.FindIndex(item => item.deptID == newBill.deptID);
                //         BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Shot);
                //     }

                //     Debug.Log("Paid " + newBill.billName);
                //     m_PlayerManager.billData.bill.Remove(newBill);
                // };
                // newBill.CallbackNo = delegate
                // {
                //     Debug.LogWarning("Not Paid Bill fixedCost");
                //     newBill.deadline += DAY_IN_MONTH;
                //     newBill.penalty += 100f;

                //     PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
                //     newLiability.name = newBill.billName;
                //     newLiability.type = State.LiabilityType.Shot;
                //     newLiability.expenseType = newBill.expenseType;
                //     newLiability.loanAmount = newBill.amount;
                    
                //     BankManager.Instance.currentDeptID ++;
                //     newLiability.deptID = BankManager.Instance.currentDeptID;
                //     newBill.deptID = BankManager.Instance.currentDeptID;
                //     BankManager.Instance.AddLiability(newLiability);
                // };

                if (newBill.amount != 0)
                {
                    m_PlayerManager.billData.bill.Add(newBill);
                }
            }
        }

        // Car
        if (Year() == 1)
        {
            for (int i = 0; i < m_PlayerManager.assetData.Asset[(int)State.AssetType.car].element.Count; i++)
            {
                int index = i;
                if (m_PlayerManager.assetData.Asset[(int)State.AssetType.car].element[index].fixedCost > 0)
                {
                    PlayerBillElement newBill = new PlayerBillElement();
                    newBill.billName = "ค่าบำรุงรักษาและ พรบ.";
                    newBill.billName += " (" + DateTime("MM/YYYY") + ")";
                    newBill.expenseType = State.Expense.CarFixedCost;
                    newBill.billType = State.BillType.FixedCost;
                    newBill.index = index;
                    newBill.amount = m_PlayerManager.assetData.Asset[(int)State.AssetType.car].element[index].fixedCost;

                    newBill.deadline = realDay + (DAY_IN_MONTH - 1);

                    // newBill.CallbackYes = delegate
                    // {
                    //     if (newBill.deptID > 0)
                    //     {
                    //         int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element.FindIndex(item => item.deptID == newBill.deptID);
                    //         BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Shot);
                    //     }

                    //     Debug.Log("Paid " + newBill.billName);
                    //     m_PlayerManager.billData.bill.Remove(newBill);
                    // };
                    // newBill.CallbackNo = delegate
                    // {
                    //     Debug.LogWarning("Not Paid Bill fixedCost");
                    //     newBill.deadline += DAY_IN_MONTH;
                    //     newBill.penalty += 100f;

                    //     PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
                    //     newLiability.name = newBill.billName;
                    //     newLiability.type = State.LiabilityType.Shot;
                    //     newLiability.expenseType = newBill.expenseType;
                    //     newLiability.loanAmount = newBill.amount;
                        
                    //     BankManager.Instance.currentDeptID ++;
                    //     newLiability.deptID = BankManager.Instance.currentDeptID;
                    //     newBill.deptID = BankManager.Instance.currentDeptID;
                    //     BankManager.Instance.AddLiability(newLiability);
                    // };

                    if (newBill.amount != 0)
                    {
                        m_PlayerManager.billData.bill.Add(newBill);
                    }
                }
            }
        }
    }

    private void CalRentBill()
    {
        if (BankManager.Instance.haveRent())
        {
            for (int i = 0; i < m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Rent].element.Count; i++)
            {
                int index = i;
                PlayerBillElement newBill = new PlayerBillElement();
                newBill.billName = "ค่าเช่า ";
                newBill.billName += m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Rent].element[index].name;
                newBill.billName += " (" + DateTime("MM/YYYY") + ")";
                newBill.amount = (float)m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Rent].element[index].installment;
                newBill.expenseType = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Rent].element[index].expenseType;
                newBill.billType = State.BillType.RentCost;
                newBill.index = index;

                newBill.deadline = realDay + (DAY_IN_MONTH - 1);

                // newBill.CallbackYes = delegate
                // {
                //     if (newBill.penalty == 0)
                //     {
                //         m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Rent].element[index].term++;
                //     }

                //     if (newBill.deptID > 0)
                //     {
                //         int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element.FindIndex(item => item.deptID == newBill.deptID);
                //         BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Shot);
                //     }

                //     Debug.Log("Paid Rent");
                //     m_PlayerManager.billData.bill.Remove(newBill);
                // };
                // newBill.CallbackNo = delegate
                // {
                //     if (newBill.penalty == 0)
                //     {
                //         m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Rent].element[index].term++;
                //     }

                //     Debug.LogWarning("Not Paid Rent");
                //     newBill.deadline += DAY_IN_MONTH;
                //     newBill.penalty += 100f;

                //     PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
                //     newLiability.name = newBill.billName;
                //     newLiability.type = State.LiabilityType.Shot;
                //     newLiability.expenseType = newBill.expenseType;
                //     newLiability.loanAmount = newBill.amount;
                    
                //     BankManager.Instance.currentDeptID ++;
                //     newLiability.deptID = BankManager.Instance.currentDeptID;
                //     newBill.deptID = BankManager.Instance.currentDeptID;
                //     BankManager.Instance.AddLiability(newLiability);
                // };

                if (newBill.amount != 0)
                {
                    m_PlayerManager.billData.bill.Add(newBill);
                }
            }
        }
    }

    private void CalLiabilityBill()
    {
        if (BankManager.Instance.haveLiability(State.LiabilityType.Loan))
        {
            for (int i = 0; i < m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element.Count; i++)
            {
                int index = i;
                PlayerBillElement newBill = new PlayerBillElement();
                PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
                newLiability = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[index];

                newBill.billName = "ค่างวด" + newLiability.name;
                newBill.billName += " (" + DateTime("MM/YYYY") + ")";
                newBill.expenseType = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[index].expenseType;
                newBill.billType = State.BillType.Loan;
                newBill.index = index;

                newBill.deadline = realDay + (DAY_IN_MONTH - 1);
                newBill.irPerMonth = newLiability.irPerMonth;
                
                float maxAmount = Mathf.RoundToInt((float)(decimal)newLiability.maxAmount * 100f) / 100f;
                float loanAmount = Mathf.RoundToInt((float)(decimal)newLiability.loanAmount * 100f) / 100f;
                float installment = Mathf.RoundToInt((float)(decimal)newLiability.installment * 100f) / 100f;

                if (newBill.expenseType == State.Expense.Home)
                {
                    if (newLiability.term > 1)
                    {
                        newBill.interest = loanAmount * (newBill.irPerMonth);
                        newBill.amount = installment - newBill.interest;
                    }
                    else
                    {
                        newBill.interest = loanAmount * (newBill.irPerMonth);
                        newBill.amount = loanAmount - newBill.interest;
                    }
                }
                else if (newBill.expenseType == State.Expense.Car)
                {
                    if (newLiability.term > 1)
                    {
                        newBill.amount = (maxAmount / (float)newLiability.maxTerm);
                        newBill.interest = installment - newBill.amount;
                    }
                    else
                    {
                        newBill.amount = loanAmount;
                        newBill.interest = installment - newBill.amount;
                    }
                }

                // newBill.CallbackYes = delegate
                // {
                //     m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[index].term --;
                //     m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[index].loanAmount -= (newBill.amount);

                //     if (newBill.expenseType == State.Expense.Home)
                //     {
                //         PlayerManager.Instance.taxData.deduction[(int)State.Deduction.DeptEstate].deductionRec += newBill.interest;
                //     }

                //     Debug.Log("Paid Loan " + newBill.amount + " Balance " + m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[index].loanAmount);
                //     m_PlayerManager.billData.bill.Remove(newBill);
                    
                //     if (m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[index].term <= 0)
                //     {
                //         BankManager.Instance.RemoveLiability(index, State.LiabilityType.Loan);
                //     }
                // };
                // เกินกำหนด
                // newBill.CallbackNo = delegate
                // {
                //     // !!! not pay
                //     // newBill.deadline = realDay + 1;
                //     newBill.penalty += (newBill.interest * (newBill.irPerMonth));
                //     Debug.LogWarning("Not Pay Loan");
                // };

                if (newBill.amount != 0)
                {
                    m_PlayerManager.billData.bill.Add(newBill);
                }
            }
        }

        
        if (BankManager.Instance.haveLiability(State.LiabilityType.Personal))
        {
            for (int i = 0; i < m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element.Count; i++)
            {
                int index = i;
                PlayerBillElement newBill = new PlayerBillElement();
                PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
                newLiability = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element[index];

                newBill.billName = "ค่างวด" + newLiability.name;
                newBill.billName += " (" + DateTime("MM/YYYY") + ")";
                newBill.expenseType = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element[index].expenseType;
                newBill.billType = State.BillType.Personal;
                newBill.index = index;

                newBill.deadline = realDay + (DAY_IN_MONTH - 1);
                newBill.irPerMonth = newLiability.irPerMonth;
                
                float loanAmount = Mathf.RoundToInt((float)(decimal)newLiability.loanAmount * 100f) / 100f;
                float installment = Mathf.RoundToInt((float)(decimal)newLiability.installment * 100f) / 100f;

                if (newLiability.term > 1)
                {
                    newBill.interest = loanAmount * (newBill.irPerMonth);
                    newBill.amount = installment - newBill.interest;
                }
                else
                {
                    newBill.interest = loanAmount * (newBill.irPerMonth);
                    newBill.amount = loanAmount - newBill.interest;
                }

                // newBill.CallbackYes = delegate
                // {
                //     m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element[index].term --;
                //     m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element[index].loanAmount -= (newBill.amount);

                //     Debug.Log("Paid Loan " + newBill.amount + " Balance " + m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element[index].loanAmount);
                //     m_PlayerManager.billData.bill.Remove(newBill);
                    
                //     if (m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element[index].term <= 0)
                //     {
                //         BankManager.Instance.RemoveLiability(index, State.LiabilityType.Personal);
                //     }
                // };
                // เกินกำหนด
                // newBill.CallbackNo = delegate
                // {
                //     // !!! not pay
                //     // newBill.deadline = realDay + 1;
                //     newBill.penalty += (newBill.interest * (newBill.irPerMonth));
                //     Debug.LogWarning("Not Pay Loan");
                // };

                if (newBill.amount != 0)
                {
                    m_PlayerManager.billData.bill.Add(newBill);
                }
            }
        }
    }

    private void CalCreditCardBill()
    {
        for (int i = 0; i < m_PlayerManager.liabilityData.creditCard.Count; i ++)
        {
            int _Index = i;
            float Used = m_PlayerManager.liabilityData.creditCard[_Index].calUsed;
            float newDept = m_PlayerManager.liabilityData.creditCard[_Index].dept;
            float ir = m_PlayerManager.liabilityData.creditCard[_Index].deptInterest;
            float irDept = m_PlayerManager.liabilityData.creditCard[_Index].deptInterest;

            if (Used > 0 || newDept > 0)
            {
                PlayerBillElement newBill = new PlayerBillElement();
                newBill.billName = "ใบแจ้งยอด" + m_PlayerManager.liabilityData.creditCard[_Index].cardName;
                newBill.billName += " (" + DateTime("MM/YYYY") + ")";
                newBill.expenseType = State.Expense.None;
                newBill.billType = State.BillType.CreditCard;
                newBill.index = _Index;

                newBill.interest = m_PlayerManager.liabilityData.creditCard[_Index].interest;
                newBill.deadline = realDay + (DAY_IN_MONTH - 1);
                newBill.newDept = newDept;
                newBill.irDept = irDept;
                newBill.used = Used;
                newBill.amount = newBill.used;
                newBill.ir = ir;

                newBill.creditDept = newBill.newDept + newBill.irDept;
                newBill.canMn = true;

                // newBill.CallbackYes = delegate
                // {
                //     m_PlayerManager.liabilityData.creditCard[_Index].usedCredit -= newBill.used;
                //     m_PlayerManager.liabilityData.creditCard[_Index].deptUsed -= newBill.used;

                //     m_PlayerManager.liabilityData.creditCard[_Index].usedCredit -= newBill.newDept;
                //     m_PlayerManager.liabilityData.creditCard[_Index].dept -= newBill.newDept;

                //     m_PlayerManager.liabilityData.creditCard[_Index].deptInterest -= newBill.ir;
                //     m_PlayerManager.liabilityData.creditCard[_Index].deptInterest -= newBill.irDept;
                //     m_PlayerManager.liabilityData.creditCard[_Index].deptInterest = 0;
                //     m_PlayerManager.liabilityData.creditCard[_Index].interest = 0;

                //     if (m_PlayerManager.liabilityData.creditCard[_Index].deptID > 0)
                //     {
                //         int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element.FindIndex(item => 
                //             item.deptID == m_PlayerManager.liabilityData.creditCard[_Index].deptID);
                //         BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Shot);
                //         m_PlayerManager.liabilityData.creditCard[_Index].deptID = 0;
                //     }
                    
                //     m_PlayerManager.billData.bill.Remove(newBill);
                // };

                int lastBill = m_PlayerManager.billData.bill.Count;
                // newBill.CallbackNo = delegate
                // {
                //     if (newBill.amount != newBill.used || newBill.creditDept != (newBill.newDept + newBill.irDept))
                //     {
                //         m_PlayerManager.liabilityData.creditCard[_Index].usedCredit -= (newBill.used * 0.1f);
                //         m_PlayerManager.liabilityData.creditCard[_Index].deptUsed -= (newBill.used * 0.1f);

                //         m_PlayerManager.liabilityData.creditCard[_Index].usedCredit -= (newBill.newDept * 0.1f);
                //         m_PlayerManager.liabilityData.creditCard[_Index].dept -= (newBill.newDept * 0.1f);

                //         m_PlayerManager.liabilityData.creditCard[_Index].deptInterest -= (newBill.ir * 0.1f);
                //         m_PlayerManager.liabilityData.creditCard[_Index].deptInterest -= (newBill.irDept * 0.1f);
                //     }
                    
                //     if (m_PlayerManager.liabilityData.creditCard[_Index].deptID > 0)
                //     {
                //         int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element.FindIndex(item => 
                //             item.deptID == m_PlayerManager.liabilityData.creditCard[_Index].deptID);
                //         BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Shot);
                //         m_PlayerManager.liabilityData.creditCard[_Index].deptID = 0;
                //     }

                //     if (newBill.creditDept + newBill.amount > 0)
                //     {
                //         PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
                //         newLiability.name = "หนี้บัตรเครดิต";
                //         newLiability.type = State.LiabilityType.Shot;
                //         newLiability.expenseType = newBill.expenseType;
                //         newLiability.loanAmount = newBill.creditDept + newBill.amount;
                        
                //         BankManager.Instance.currentDeptID ++;
                //         newLiability.deptID = BankManager.Instance.currentDeptID;
                //         m_PlayerManager.liabilityData.creditCard[_Index].deptID = BankManager.Instance.currentDeptID;

                //         BankManager.Instance.AddLiability(newLiability);
                //     }

                //     m_PlayerManager.liabilityData.creditCard[_Index].dept += m_PlayerManager.liabilityData.creditCard[_Index].deptUsed;
                //     m_PlayerManager.liabilityData.creditCard[_Index].deptUsed = 0;

                //     Debug.LogWarning("Pay minimum Credit");
                //     m_PlayerManager.billData.bill.Remove(newBill);
                // };
                    
                if (m_PlayerManager.liabilityData.creditCard[_Index].deptID > 0)
                {
                    int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element.FindIndex(item => 
                        item.deptID == m_PlayerManager.liabilityData.creditCard[_Index].deptID);
                    BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Shot);
                    m_PlayerManager.liabilityData.creditCard[_Index].deptID = 0;
                }

                if (newBill.creditDept > 0)
                {
                    PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
                    newLiability.name = "หนี้บัตรเครดิต";
                    newLiability.type = State.LiabilityType.Shot;
                    newLiability.expenseType = newBill.expenseType;
                    newLiability.loanAmount = newBill.creditDept;
                    
                    BankManager.Instance.currentDeptID ++;
                    newLiability.deptID = BankManager.Instance.currentDeptID;
                    m_PlayerManager.liabilityData.creditCard[_Index].deptID = BankManager.Instance.currentDeptID;

                    BankManager.Instance.AddLiability(newLiability);
                }

                m_PlayerManager.billData.bill.Add(newBill);

                m_PlayerManager.liabilityData.creditCard[_Index].deptUsed += Used;
                m_PlayerManager.liabilityData.creditCard[_Index].deptInterest += m_PlayerManager.liabilityData.creditCard[_Index].interest;
                m_PlayerManager.liabilityData.creditCard[_Index].interest = m_PlayerManager.liabilityData.creditCard[_Index].calInterest;
                m_PlayerManager.liabilityData.creditCard[_Index].calInterest = 0;

                for (int j = 0; j < m_PlayerManager.liabilityData.creditCard[_Index].date.Count; j++)
                {
                    m_PlayerManager.liabilityData.creditCard[_Index].date[j].element = new List<PlayerCreditCardElement>();
                }
            }
        }
    }

    private void CalInsurance()
    {
        if (m_PlayerManager.assetData.insurances[(int)State.Insurance.LifeInsurance].element.expiredDate == realDay)
        {
            CalInsurance(State.Insurance.LifeInsurance);
        }
        if (m_PlayerManager.assetData.insurances[(int)State.Insurance.HealthInsurance].element.expiredDate == realDay)
        {
            CalInsurance(State.Insurance.HealthInsurance);
        }
        if (m_PlayerManager.assetData.insurances[(int)State.Insurance.AccidentInsurance].element.expiredDate == realDay)
        {
            CalInsurance(State.Insurance.AccidentInsurance);
        }
    }

    private void CalInsurance(State.Insurance _Type)
    {
        PlayerBillElement newBill = new PlayerBillElement();
        newBill.billName = "บิล" + m_PlayerManager.assetData.insurances[(int)_Type].typeName;
        newBill.billName += " (" + DateTime("MM/YYYY") + ")";
        newBill.amount = m_PlayerManager.assetData.insurances[(int)_Type].element.price;
        newBill.expenseType = State.Expense.ETC;
        newBill.insuranceType = _Type;
        newBill.billType = State.BillType.Insurance;
        // newBill.index = _Index;

        newBill.deadline = realDay + (DAY_IN_MONTH - 1);

        // newBill.CallbackYes = delegate
        // {
        //     Debug.Log("Paid Bill " + m_PlayerManager.assetData.insurances[(int)_Type].typeName);
        //     m_PlayerManager.assetData.insurances[(int)_Type].element.expiredDate = m_PlayerManager.assetData.insurances[(int)_Type].element.expiredDate + CalManager.Instance.DAY_IN_YEAR;
        //     m_PlayerManager.assetData.insurances[(int)_Type].element.protectionLimit = m_PlayerManager.assetData.insurances[(int)_Type].element.protectionMaxLimit;
        //     m_PlayerManager.billData.bill.Remove(newBill);
        // };
        // newBill.CallbackNo = delegate
        // {
        //     Debug.LogWarning("Not Paid Bill " + m_PlayerManager.assetData.insurances[(int)_Type].typeName);
        //     AssetManager.Instance.RemoveInsurance(_Type);
        //     m_PlayerManager.billData.bill.Remove(newBill);
        // };

        if (newBill.amount != 0)
        {
            m_PlayerManager.billData.bill.Add(newBill);
        }
    }

    private void CalDepositInterest()
    {
        for (int i = 1; i < m_PlayerManager.accountData.account.Count; i ++)
        {
            float value = (float)m_PlayerManager.accountData.account[i].balance * (BankManager.Instance.allAccount.accounts[m_PlayerManager.accountData.account[i].accountID].interestRate / 100f / (float)DAY_IN_YEAR);
            value = Mathf.RoundToInt(value * 100f) / 100f;
            m_PlayerManager.accountData.account[i].accruedExpenses += value;
        }
    }

    private void CalCreditInterest()
    {
        for (int i = 0; i < m_PlayerManager.liabilityData.creditCard.Count; i ++)
        {
            float dept = m_PlayerManager.liabilityData.creditCard[i].dept + m_PlayerManager.liabilityData.creditCard[i].deptUsed + m_PlayerManager.liabilityData.creditCard[i].deptInterest;
            float value = (float)dept * (BankManager.Instance.allCreditCard[m_PlayerManager.liabilityData.creditCard[i].cardID].ir / 100f / (float)DAY_IN_YEAR);
            value = Mathf.RoundToInt(value * 100f) / 100f;

            float value2 = (float)m_PlayerManager.liabilityData.creditCard[i].calUsed * (BankManager.Instance.allCreditCard[m_PlayerManager.liabilityData.creditCard[i].cardID].ir / 100f / (float)DAY_IN_YEAR);
            value2 = Mathf.RoundToInt(value2 * 100f) / 100f;

            m_PlayerManager.liabilityData.creditCard[i].interest += value;
            m_PlayerManager.liabilityData.creditCard[i].calInterest += value2;
        }

        if (Day() == DAY_IN_MONTH && realDay > 1)
        {
            // Credit Card bill
            CalCreditCardBill();
        }
    }

    private void AssetStockGrowth()
    {
        for (int j = 0; j < AssetManager.Instance.Asset[(int)State.AssetType.stock].element.Length; j++)
        {
            float growth = Random.Range(-3, 4) * AssetManager.Instance.Asset[(int)State.AssetType.stock].element[j].price * AssetManager.Instance.Asset[(int)State.AssetType.stock].element[j].growthRate / 100;
            AssetManager.Instance.Asset[(int)State.AssetType.stock].element[j].pPrice = AssetManager.Instance.Asset[(int)State.AssetType.stock].element[j].price;
            AssetManager.Instance.Asset[(int)State.AssetType.stock].element[j].price += growth;
            AssetManager.Instance.Asset[(int)State.AssetType.stock].element[j].price = Mathf.Floor(AssetManager.Instance.Asset[(int)State.AssetType.stock].element[j].price * 100) / 100;
        }
    }

    private void AssetGrowth()
    {
        for (int j = 0; j < AssetManager.Instance.Asset[(int)State.AssetType.fund].element.Length; j++)
        {
            float growth = Random.Range(-2, 4) * AssetManager.Instance.Asset[(int)State.AssetType.fund].element[j].price * AssetManager.Instance.Asset[(int)State.AssetType.fund].element[j].growthRate / 100;
            AssetManager.Instance.Asset[(int)State.AssetType.fund].element[j].pPrice = AssetManager.Instance.Asset[(int)State.AssetType.fund].element[j].price;
            AssetManager.Instance.Asset[(int)State.AssetType.fund].element[j].price += growth;
            AssetManager.Instance.Asset[(int)State.AssetType.fund].element[j].price = Mathf.Floor(AssetManager.Instance.Asset[(int)State.AssetType.fund].element[j].price * 100) / 100;
        }
        for (int j = 0; j < AssetManager.Instance.Asset[(int)State.AssetType.gold].element.Length; j++)
        {
            float growth = Random.Range(-2, 3) * AssetManager.Instance.Asset[(int)State.AssetType.gold].element[j].price * AssetManager.Instance.Asset[(int)State.AssetType.gold].element[j].growthRate / 100;
            AssetManager.Instance.Asset[(int)State.AssetType.gold].element[j].pPrice = AssetManager.Instance.Asset[(int)State.AssetType.gold].element[j].price;
            AssetManager.Instance.Asset[(int)State.AssetType.gold].element[j].price += growth;
            AssetManager.Instance.Asset[(int)State.AssetType.gold].element[j].price = Mathf.Floor(AssetManager.Instance.Asset[(int)State.AssetType.gold].element[j].price * 100) / 100;
        }
    }

    private void AssetPersonalGrowth()
    {
        for (int j = 0; j < AssetManager.Instance.Asset[(int)State.AssetType.car].element.Length; j++)
        {
            float growth = Random.Range(-5, 2) * AssetManager.Instance.Asset[(int)State.AssetType.car].element[j].price * AssetManager.Instance.Asset[(int)State.AssetType.car].element[j].growthRate / 100;
            AssetManager.Instance.Asset[(int)State.AssetType.car].element[j].pPrice = AssetManager.Instance.Asset[(int)State.AssetType.car].element[j].price;
            AssetManager.Instance.Asset[(int)State.AssetType.car].element[j].price += growth;
            AssetManager.Instance.Asset[(int)State.AssetType.car].element[j].price = Mathf.Floor(AssetManager.Instance.Asset[(int)State.AssetType.car].element[j].price * 100) / 100;
        }
        for (int j = 0; j < AssetManager.Instance.Asset[(int)State.AssetType.estate].element.Length; j++)
        {
            float growth = Random.Range(-4, 6) * AssetManager.Instance.Asset[(int)State.AssetType.estate].element[j].price * AssetManager.Instance.Asset[(int)State.AssetType.estate].element[j].growthRate / 100;
            AssetManager.Instance.Asset[(int)State.AssetType.estate].element[j].pPrice = AssetManager.Instance.Asset[(int)State.AssetType.estate].element[j].price;
            AssetManager.Instance.Asset[(int)State.AssetType.estate].element[j].price += growth;
            AssetManager.Instance.Asset[(int)State.AssetType.estate].element[j].price = Mathf.Floor(AssetManager.Instance.Asset[(int)State.AssetType.estate].element[j].price * 100) / 100;
        }
    }

    private void Scamming()
    {
        if (BankManager.Instance.scamData.scamDate == 0 && Day() == SCAM_SPAWN_DATE)
        {
            if (BankManager.Instance.scamData.scamMissing >= SCAM_MISSING_TIME)
            {
                BankManager.Instance.scamData.scamMissing = 0;
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.Scamming);
            }
            else
            {
                if (Random.Range(0, 100) <= 20)
                {
                    BankManager.Instance.scamData.scamMissing = 0;
                    UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.Scamming);
                }
                else
                {
                    BankManager.Instance.scamData.scamMissing++;
                }
            }
        }
        else if (BankManager.Instance.scamData.scamDate > 0 && BankManager.Instance.scamData.scamDate == realDay)
        {
            bool isWin = false;
            if (BankManager.Instance.scamData.isFirstTime)
            {
                isWin = true;
                BankManager.Instance.scamData.isFirstTime = false;
            }
            else
            {
                int winRate = 10;
                if (BankManager.Instance.scamData.scamPriceSubmit < 10000)
                {
                    winRate = 100;
                }
                else if (BankManager.Instance.scamData.scamPriceSubmit < 50000)
                {
                    winRate = 30;
                }
                else if (BankManager.Instance.scamData.scamPriceSubmit < 100000)
                {
                    winRate = 15;
                }
                else
                {
                    winRate = 10;
                }
                isWin = Random.Range(0, 100) <= winRate;
            }

            if (isWin)
            {
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.ScamWin);
                BankManager.Instance.OnScamWin();
            }
            else
            {
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.ScamLose);
                BankManager.Instance.OnScamLose();
            }
        }
    }

    public void HospitalEvent()
    {
        if (ActionManager.Instance.hospitalData.hospitalExitDate == 0 && Day() == HOSPITAL_SPAWN_DATE && realDay > DAY_IN_MONTH)
        {
            if (ActionManager.Instance.hospitalData.hospitalMissing >= HOSPITAL_MISSING_TIME)
            {
                ActionManager.Instance.hospitalData.hospitalMissing = 0;
                SetHospitalPrice(State.Hospital.Health);
            }
            else
            {
                if (Random.Range(0, 100) <= 10)
                {
                    ActionManager.Instance.hospitalData.hospitalMissing = 0;
                    SetHospitalPrice(State.Hospital.Accident);
                }
                else
                {
                    if (Random.Range(0, 100) <= 20)
                    {
                        ActionManager.Instance.hospitalData.hospitalMissing = 0;
                        SetHospitalPrice(State.Hospital.Health);
                    }
                    else
                    {
                        ActionManager.Instance.hospitalData.hospitalMissing++;
                    }
                }
            }
        }
    }

    public void SetHospitalPrice(State.Hospital _Type)
    {
        ActionManager.Instance.hospitalData.currentHospital = _Type;

        float ratio = Random.Range(8, 12) / 10f;
        ActionManager.Instance.hospitalData.hospitalPrice = ratio * ActionManager.Instance.hospitalData.hospitalDefaultPrice[(int)_Type];

        if (_Type == State.Hospital.Health)
        {
            ActionManager.Instance.hospitalData.hospitalPrice *= m_PlayerManager.playerData.age;
            ActionManager.Instance.hospitalData.hospitalInsurance = m_PlayerManager.assetData.insurances[(int)State.Insurance.HealthInsurance].element.protectionLimit;
        }
        else if (_Type == State.Hospital.Accident)
        {
            ActionManager.Instance.hospitalData.hospitalInsurance = m_PlayerManager.assetData.insurances[(int)State.Insurance.AccidentInsurance].element.protectionLimit;
        }

        if (ActionManager.Instance.hospitalData.hospitalInsurance > ActionManager.Instance.hospitalData.hospitalPrice)
        {
            ActionManager.Instance.hospitalData.hospitalInsurance = ActionManager.Instance.hospitalData.hospitalPrice;
        }

        ActionManager.Instance.hospitalData.hospitalExitDate = realDay;
    }

    public void SkipDay()
    {

    }
}