﻿using System;
using System.Collections;
using UnityEngine;

namespace Utilities
{
    public class UIUtilities : Singleton<UIUtilities>
    {
        void Awake(){}
        
        #region Toggle Canvas Group
        /// <summary>
        /// Ex.
        /// UIUtilities.Instance.ToggleCanvasGroup(m_CG,
        ///                                         Easing.Type.Linear,
        ///                                         true,
        ///                                         true,
        ///                                         true,
        ///                                         0f);
        /// </summary>
        /// <param name="cg">Your canvas group</param>
        /// <param name="easingType">Easing Type</param>
        /// <param name="_isFadeIn">Fade in or out</param>
        /// <param name="_isInteractable">Start from 0 or 1</param>
        /// <param name="_blocksRaycasts">Block clicking</param>
        /// <param name="_delay">Delay before start fade</param>
        /// <param name="callback">Your own function after fade</param>
        public void ToggleCanvasGroup(CanvasGroup cg,
                                        Easing.Type easingType,
                                        bool _isFadeIn = true,
                                        bool _isInteractable = false,
                                        bool _blocksRaycasts = true,
                                        float _duration = 1f,
                                        float _delay = 0f,
                                        Action callback = null)
        {
            cg.interactable = _isInteractable;
            cg.blocksRaycasts = _blocksRaycasts;

            EZLerp.Instance.Lerp(
                Easing.Type.Linear,
                result => cg.alpha = result,
                _isFadeIn ? 0 : 1,
                _isFadeIn ? 1 : 0,
                _duration,
                0,
                () =>
                {
                    cg.alpha = _isFadeIn ? 1 : 0;
                    if (callback != null)
                        callback();
                });
        }
        #endregion

        public void SetCG(CanvasGroup _cg, bool _isActive)
        {
            _cg.alpha = _isActive ? 1 : 0;
            _cg.interactable = _isActive ? true : false;
            _cg.blocksRaycasts = _isActive ? true : false;
        }
    }
}