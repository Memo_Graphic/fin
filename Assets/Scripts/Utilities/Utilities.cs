﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public class Utilities : MonoBehaviour
    {
        # region Get Digit Length
        public static int Length(float _number)
        {
            _number = Math.Abs(_number);
            int length = 1;
            while ((_number /= 10) >= 1)
                length++;
            return length;
        }

        public static float GetLength(float _number)
        {
            return Mathf.Floor(Mathf.Log10(_number) + 1);
        }

        #endregion

        # region Set Digit to zero

        public static float SetZeroDigit(float _number)
        {
            if (Length(_number) >= 4)
            {
                _number /= 1000;
                _number = Mathf.FloorToInt(_number);
                _number *= 1000;
            }
            else if (Length(_number) == 3)
            {
                _number /= 100;
                _number = Mathf.FloorToInt(_number);
                _number *= 100;
            }
            else if (Length(_number) == 2)
            {
                _number /= 10;
                _number = Mathf.FloorToInt(_number);
                _number *= 10;
            }
            return _number;
        }

        public static string SetK_M(float _number)
        {
            string sub = "";
            float zeroCounter = 0;
            if (Length(_number) >= 1)
            {
                zeroCounter += Length(_number) - 1;
                if (zeroCounter == 3)
                {
                    sub = _number.ToString().Substring(0, 1);
                    sub = sub + "K";
                }
                else if (zeroCounter == 4)
                {
                    sub = _number.ToString().Substring(0, 2);
                    sub = sub + "K";
                }
                else if (zeroCounter == 5)
                {
                    sub = _number.ToString().Substring(0, 3);
                    sub = sub + "K";
                }
                else if (zeroCounter >= 6)
                {
                    sub = _number.ToString().Substring(0, 1);
                    sub = sub + "M";
                }
                else
                {
                    sub = _number.ToString("N0");
                }
            }
            return sub;
        }

        public static string SetK_M_withPoint(float _number, int _place, int minBase = 0)
        {
            string sub = "";
            float zeroCounter = 0;
            if (Length(_number) >= 1)
            {
                zeroCounter += Length(_number) - 1;
                if (minBase < 3 && zeroCounter == 3)
                {
                    sub = _number.ToString().Substring(0, 1) + "." + _number.ToString().Substring(1, _place);
                    sub = sub + "K";
                }
                else if (minBase < 4 && zeroCounter == 4)
                {
                    sub = _number.ToString().Substring(0, 2) + "." + _number.ToString().Substring(2, _place);
                    sub = sub + "K";
                }
                else if (minBase < 5 && zeroCounter == 5)
                {
                    sub = _number.ToString().Substring(0, 3) + "." + _number.ToString().Substring(3, _place);
                    sub = sub + "K";
                }
                else if (minBase < 6 && zeroCounter >= 6)
                {
                    sub = _number.ToString().Substring(0, 1) + "." + _number.ToString().Substring(1, _place);
                    sub = sub + "M";
                }
                else
                {
                    sub = _number.ToString("N" + _place);
                }
                if (_place == 0)
                    sub = sub.Replace(".", "");
            }
            return sub;
        }

        public static string ThreePointNumber(float _number)
        {
            string num = "";
            if (_number < 10)
            {
                num += Mathf.FloorToInt(_number);
                num += ".";
                num += Mathf.RoundToInt(Mathf.FloorToInt(_number*10)%10);
                num += Mathf.RoundToInt(_number*100)%10;
            }
            else if (_number < 100)
            {
                num += Mathf.FloorToInt(_number/10);
                num += Mathf.RoundToInt(_number%10);
                num += ".";
                num += Mathf.RoundToInt((_number*10)%10);
            }
            else if (_number < 1000)
            {
                num = Mathf.RoundToInt(_number).ToString("N0");
            }
            else if (_number < 10000)
            {
                num = SetK_M_withPoint(_number, 1);
            }
            return num;
        }
        #endregion

    }
}