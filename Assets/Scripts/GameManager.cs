﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public bool isTutorial;
    public State.Level gameLevel;
    public GameObject EventSystem;

    [Header("Color")]
    public Color green;
    public Color red;
    public Color sleepTime;
    public Color actionTime;


    // Start is called before the first frame update
    void Start()
    {
        SaveManager.Instance.IS_ONLINE_SAVE = isTutorial;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleEventSystem(bool isTrue)
    {
        EventSystem.SetActive(isTrue);
    }

    public void ToggleEventSystem()
    {
        EventSystem.SetActive(!EventSystem.activeSelf);
    }
}
