using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct BillData
{
    public List<PlayerBillElement> bill;
}

[System.Serializable]
public class PlayerBillElement
{
    public string billName = "billName";
    public State.Expense expenseType;
    public State.BillType billType;
    public State.Insurance insuranceType;
    public bool canMn;
    public float amount;
    public float interest;
    public float penalty;
    public float creditDept;
    public float newDept;
    public float used;
    public float ir;
    public float irDept;
    public float irPerMonth; //ดอกเบี้ยต่อเดือน
    public int deadline; // ชำระหนี้ก่อนวันนี้
    public int deptID = 0;
    public int index = 0;
    public int overdue = 0;
    // public System.Action CallbackYes;
    // public System.Action CallbackNo;
    
    public void CallbackYes()
    {
        PlayerManager m_PlayerManager = PlayerManager.Instance;

        if (billType == State.BillType.FixedCost)
        {
            if (deptID > 0)
            {
                int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element.FindIndex(item => item.deptID == deptID);
                BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Shot);
            }

            Debug.Log("Paid: " + billName);
        }
        else if (billType == State.BillType.RentCost)
        {
            if (penalty == 0)
            {
                m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Rent].element[index].term ++;
            }

            if (deptID > 0)
            {
                int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element.FindIndex(item => item.deptID == deptID);
                BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Shot);
            }

            Debug.Log("Paid Rent: " + billName);
        }
        else if (billType == State.BillType.Loan)
        {
            m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[index].term --;
            m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[index].loanAmount -= (amount);

            if (expenseType == State.Expense.Home)
            {
                PlayerManager.Instance.taxData.deduction[(int)State.Deduction.DeptEstate].deductionRec += interest;
            }

            if (m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[index].term <= 0)
            {
                BankManager.Instance.RemoveLiability(index, State.LiabilityType.Loan);
            }

            Debug.Log("Paid Loan: " + amount + " Balance " + m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Loan].element[index].loanAmount);
        }
        else if (billType == State.BillType.Personal)
        {
            m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element[index].term --;
            m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element[index].loanAmount -= (amount);
            
            if (m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element[index].term <= 0)
            {
                BankManager.Instance.RemoveLiability(index, State.LiabilityType.Personal);
            }

            Debug.Log("Paid Loan: " + amount + " Balance " + m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Personal].element[index].loanAmount);
        }
        else if (billType == State.BillType.CreditCard)
        {
            m_PlayerManager.liabilityData.creditCard[index].usedCredit -= used;
            m_PlayerManager.liabilityData.creditCard[index].deptUsed -= used;

            m_PlayerManager.liabilityData.creditCard[index].usedCredit -= newDept;
            m_PlayerManager.liabilityData.creditCard[index].dept -= newDept;

            m_PlayerManager.liabilityData.creditCard[index].deptInterest -= ir;
            m_PlayerManager.liabilityData.creditCard[index].deptInterest -= irDept;
            m_PlayerManager.liabilityData.creditCard[index].deptInterest = 0;
            m_PlayerManager.liabilityData.creditCard[index].interest = 0;

            if (m_PlayerManager.liabilityData.creditCard[index].deptID > 0)
            {
                int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element.FindIndex(item => 
                    item.deptID == m_PlayerManager.liabilityData.creditCard[index].deptID);
                BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Shot);
                m_PlayerManager.liabilityData.creditCard[index].deptID = 0;
            }

            Debug.Log("Paid Credit Bill ");
        }
        else if (billType == State.BillType.Insurance)
        {
            m_PlayerManager.assetData.insurances[(int)insuranceType].element.expiredDate = m_PlayerManager.assetData.insurances[(int)insuranceType].element.expiredDate + CalManager.Instance.DAY_IN_YEAR;
            m_PlayerManager.assetData.insurances[(int)insuranceType].element.protectionLimit = m_PlayerManager.assetData.insurances[(int)insuranceType].element.protectionMaxLimit;
           
            Debug.Log("Paid Bill: " + m_PlayerManager.assetData.insurances[(int)insuranceType].typeName);
        }
        else if (billType == State.BillType.Tax)
        {
            Debug.Log("Paid Tax Bill ");
        }
        else if (billType == State.BillType.None)
        {

        }

        m_PlayerManager.billData.bill.Remove(this);
    }

    public void CallbackNo()
    {
        PlayerManager m_PlayerManager = PlayerManager.Instance;

        if (billType == State.BillType.FixedCost)
        {
            if (penalty == 0)
            {
                PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
                newLiability.name = billName;
                newLiability.type = State.LiabilityType.Shot;
                newLiability.expenseType = expenseType;
                newLiability.loanAmount = amount;
                
                BankManager.Instance.currentDeptID ++;
                newLiability.deptID = BankManager.Instance.currentDeptID;
                deptID = BankManager.Instance.currentDeptID;
                BankManager.Instance.AddLiability(newLiability);
            }

            deadline += CalManager.Instance.DAY_IN_MONTH;
            penalty += 200f;
            overdue++;
            Debug.LogWarning("Not Paid Bill fixedCost");
        }
        else if (billType == State.BillType.RentCost)
        {
            if (penalty == 0)
            {
                m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Rent].element[index].term++;

                PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
                newLiability.name = billName;
                newLiability.type = State.LiabilityType.Shot;
                newLiability.expenseType = expenseType;
                newLiability.loanAmount = amount;
                
                BankManager.Instance.currentDeptID ++;
                newLiability.deptID = BankManager.Instance.currentDeptID;
                deptID = BankManager.Instance.currentDeptID;
                BankManager.Instance.AddLiability(newLiability);
            }

            deadline += CalManager.Instance.DAY_IN_MONTH;
            penalty += 500f;
            overdue++;
            Debug.LogWarning("Not Paid Rent");
        }
        else if (billType == State.BillType.Loan)
        {
            penalty += (interest * (irPerMonth));
            overdue++;
            Debug.LogWarning("Not Pay Loan");
        }
        else if (billType == State.BillType.Personal)
        {
            penalty += (interest * (irPerMonth));
            overdue++;
            Debug.LogWarning("Not Pay Loan");

        }
        else if (billType == State.BillType.CreditCard)
        {
            if (amount != used || creditDept != (newDept + irDept))
            {
                m_PlayerManager.liabilityData.creditCard[index].usedCredit -= (used * 0.1f);
                m_PlayerManager.liabilityData.creditCard[index].deptUsed -= (used * 0.1f);

                m_PlayerManager.liabilityData.creditCard[index].usedCredit -= (newDept * 0.1f);
                m_PlayerManager.liabilityData.creditCard[index].dept -= (newDept * 0.1f);

                m_PlayerManager.liabilityData.creditCard[index].deptInterest -= (ir * 0.1f);
                m_PlayerManager.liabilityData.creditCard[index].deptInterest -= (irDept * 0.1f);
            }
            
            if (m_PlayerManager.liabilityData.creditCard[index].deptID > 0)
            {
                int _index = m_PlayerManager.liabilityData.Liability[(int)State.LiabilityType.Shot].element.FindIndex(item => 
                    item.deptID == m_PlayerManager.liabilityData.creditCard[index].deptID);
                BankManager.Instance.RemoveLiability(_index, State.LiabilityType.Shot);
                m_PlayerManager.liabilityData.creditCard[index].deptID = 0;
            }

            if (creditDept + amount > 0)
            {
                PlayerLiabilityElement newLiability = new PlayerLiabilityElement();
                newLiability.name = "หนี้บัตรเครดิต";
                newLiability.type = State.LiabilityType.Shot;
                newLiability.expenseType = expenseType;
                newLiability.loanAmount = creditDept + amount;
                
                BankManager.Instance.currentDeptID ++;
                newLiability.deptID = BankManager.Instance.currentDeptID;
                m_PlayerManager.liabilityData.creditCard[index].deptID = BankManager.Instance.currentDeptID;

                BankManager.Instance.AddLiability(newLiability);
            }

            m_PlayerManager.liabilityData.creditCard[index].dept += m_PlayerManager.liabilityData.creditCard[index].deptUsed;
            m_PlayerManager.liabilityData.creditCard[index].deptUsed = 0;

            Debug.LogWarning("Pay minimum Credit");
            m_PlayerManager.billData.bill.Remove(this);
        }
        else if (billType == State.BillType.Insurance)
        {
            Debug.LogWarning("Not Paid Bill " + m_PlayerManager.assetData.insurances[(int)insuranceType].typeName);
            AssetManager.Instance.RemoveInsurance(insuranceType);
            m_PlayerManager.billData.bill.Remove(this);
        }
        else if (billType == State.BillType.Tax)
        {
            Debug.LogWarning("Not Paid Tax Bill");
            deadline += CalManager.Instance.DAY_IN_MONTH;
            penalty += 500f;
            overdue++;
        }
        else if (billType == State.BillType.None)
        {

        }
    }
}

public class Bill : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
