using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : Singleton<PlayerManager>
{
    public PlayerData playerData;
    public JobData jobData;
    public BalanceData balanceData;

    [Header("Asset and Liability")]
    public AccountData accountData;
    public TaxData taxData;
    public BillData billData;
    public AssetData assetData;
    public LiabilityData liabilityData;

    
    [Header("Mission")]
    public MissionData missionData;
    
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string TextTime(string type = "MM")
    {
        string strTime = "";
        // float currenTime = CalManager.Instance.HOUR_IN_DAY - playerData.time;
        
        int currenTime = playerData.Time.minute;

        if (type == "" || type == "MM")
        {
            strTime = currenTime.ToString() + " นาที";
        }
        else if (type == "HH:MM")
        {
            int hour = (int)Mathf.Floor((float)currenTime / (float)CalManager.Instance.MIN_IN_HOUR);
            int minute = currenTime - (hour * CalManager.Instance.MIN_IN_HOUR);
            strTime = hour.ToString("00") + ":" + minute.ToString("00");
        }

        return strTime;
    }

    public bool CanDoAction(Activity _Action)
    {
        if (_Action.happiness < 0 && playerData.happiness < (_Action.happiness * -1f))
        {
            return false;
        }

        if (_Action.health < 0 && playerData.health < (_Action.health * -1f))
        {
            return false;
        }

        return true;
    }

    // ----- Job

    public bool haveFullTime()
    {
        bool haveFullTime;
        haveFullTime = jobData.fullTimeJob.jobID >= 0;

        return haveFullTime;
    }

    public bool haveFullTime(string _FullTimeName)
    {
        bool haveFullTime;
        haveFullTime = jobData.fullTimeJob.jobName == _FullTimeName;

        return haveFullTime;
    }

    public bool havePartTime()
    {
        bool havePartTime;
        havePartTime = jobData.partTimeJob.jobID >= 0;

        return havePartTime;
    }

    public bool havePartTime(string _PartTimeName)
    {
        bool havePartTime;
        havePartTime = jobData.partTimeJob.jobName == _PartTimeName;

        return havePartTime;
    }

    // ----- Asset

    public bool haveThisAsset(string _AssetType, State.AssetType _Type)
    {
        bool haveThisAsset = false;

        foreach(var _Asset in assetData.Asset[(int)_Type].element)
        {
            if (_Asset.assetType == _AssetType)
            {
                haveThisAsset = true;
                break;
            }
        }

        return haveThisAsset;
    }

    public int indexThisAsset(string _AssetType, State.AssetType _Type)
    {
        int index = -1;

        for (int i = 0; i < assetData.Asset[(int)_Type].element.Count; i++)
        {
            if (assetData.Asset[(int)_Type].element[i].assetType == _AssetType)
            {
                index = assetData.Asset[(int)_Type].element[i].assetID;
                break;
            }
        }

        return index;
    }

    public bool rentedThisAsset(string _AssetType, State.AssetType _Type)
    {
        bool haveRentEstate = false;

        foreach(var _Asset in assetData.Asset[(int)_Type].element)
        {
            if (_Asset.isRent && _Asset.assetType == _AssetType)
            {
                haveRentEstate = true;
                break;
            }
        }

        return haveRentEstate;
    }

    public bool haveRentEstate()
    {
        bool haveRentEstate = false;

        foreach(var _Asset in assetData.Asset[(int)State.AssetType.estate].element)
        {
            if (_Asset.isRent)
            {
                haveRentEstate = true;
                break;
            }
        }

        return haveRentEstate;
    }

    public bool IsUseAsset(string _AssetType, State.AssetType _Type)
    {
        bool isUseAsset = false;

        foreach(var _Asset in assetData.Asset[(int)_Type].element)
        {
            if (_Asset.assetType == _AssetType)
            {
                isUseAsset = _Asset.isUsing;
                break;
            }
        }

        return isUseAsset;
    }

    public int assetIndex(string _AssetType, State.AssetType _Type)
    {
        for (int i = 0; i < assetData.Asset[(int)_Type].element.Count; i++)
        {
            int _Index = i; 
            if (assetData.Asset[(int)_Type].element[i].assetType == _AssetType)
            {
                return _Index;
            }
        }

        return -1;
    }

    public double netWorth
    {
        get
        {
            return AssetValue - LiabilityValue;
        }
    }

    public float AssetValue
    {
        get 
        {
            float asset = (float)accountData.account[0].balance;

            for (int i = 1; i < accountData.account.Count; i ++)
            {
                asset += (float)accountData.account[i].balance;
            }

            foreach(var Asset in assetData.Asset)
            {
                if (Asset.element.Count > 0)
                {
                    foreach(var _Asset in Asset.element)
                    {
                        if (!_Asset.isRent)
                        {
                            asset += _Asset.price * _Asset.amount;
                        }
                    }
                }
            }

            return asset;
        }
    }

    public float LiabilityValue
    {
        get 
        {
            float liability = 0;
            foreach(var Liability in liabilityData.Liability)
            {
                if (Liability.element.Count > 0 && Liability.typeName != "Rent")
                {
                    foreach(var _Liability in Liability.element)
                    {
                        liability += (float)_Liability.loanAmount;
                    }
                }
            }
            return liability;
        }
    }

    public float NetCool
    {
        get
        {
            int MissionStar = 0;

            for (int i = 0; i < missionData.saving.Length; i++)
            {
                if (missionData.saving[i].isCompleted)
                {
                    MissionStar += missionData.saving[i].missionStar;
                }
            }

            for (int i = 0; i < missionData.wealth.Length; i++)
            {
                if (missionData.wealth[i].isCompleted)
                {
                    MissionStar += missionData.wealth[i].missionStar;
                }
            }

            for (int i = 0; i < missionData.invest.Length; i++)
            {
                if (missionData.invest[i].isCompleted)
                {
                    MissionStar += missionData.invest[i].missionStar;
                }
            }

            return MissionStar + playerData.cool;
        }
    }

    // ----- Time

    public void UpdateSleepTime(int _Value)
    {
        playerData.sleepTimeInHour = _Value;
    }

    public void SleepTime()
    {
        playerData.health = 0;
        playerData.happiness = 0;
        SubtractTime(playerData.sleepTimeInHour * CalManager.Instance.MIN_IN_HOUR);
        ApplyHappiness(playerData.sleepTimeInHour * 4);
        ApplyHealth(playerData.sleepTimeInHour * 5);
    }

    public void SubtractTime(int _Value)
    {
        if (playerData.Time.minute >= 0)
        {
            playerData.Time.minute -= _Value;
            if (playerData.Time.minute < 0)
            {
                playerData.Time.minute = 0;
            }
        }
        // CalManager.Instance.CheckTime();
    }

    public void SubtractTimeAll()
    {
        if (playerData.Time.minute >= 0)
        {
            playerData.Time.minute -= playerData.Time.minute;
            if (playerData.Time.minute < 0)
            {
                playerData.Time.minute = 0;
            }
        }
        // CalManager.Instance.CheckTime();
    }

    // ----- Money

    public void ApplyMoney(float _Value, System.Action callbackYes = null)
    {
        float fValue = Mathf.RoundToInt(_Value * 100f) / 100f;
        decimal dValue = (decimal)fValue;
        double value = (double)dValue;
        double result = accountData.account[0].balance + value;
        accountData.account[0].balance = System.Math.Round(result * 100f) / 100f;
        // accountData.account[0].balance += value;
        if (accountData.account[0].balance < 0.01)
        {
            accountData.account[0].balance = 0;
        }

        if (callbackYes != null)
        {
            callbackYes();
        }

        CheckMission(State.Mission.Wealth);
    }

    public void ApplyMoney(int _accountID, float _Value, System.Action callbackYes = null)
    {
        float fValue = Mathf.RoundToInt(_Value * 100f) / 100f;
        decimal dValue = (decimal)fValue;
        double value = (double)dValue;
        double result = accountData.account[_accountID].balance + value;
        accountData.account[_accountID].balance = System.Math.Round(result * 100f) / 100f;
        // accountData.account[_accountID].balance += value;
        if (accountData.account[_accountID].balance < 0.01)
        {
            accountData.account[_accountID].balance = 0;
        }
        

        if (callbackYes != null)
        {
            callbackYes();
        }

        CheckMission(State.Mission.Wealth);
    }
    
    public void SubtractMoney(float _Value, System.Action callbackYes = null, System.Action callbackNo = null)
    {
        float fValue = Mathf.RoundToInt(_Value * 100f) / 100f;
        decimal dValue = (decimal)fValue;
        double value = (double)dValue;
        if (accountData.account[0].balance >= value)
        {
            double result = accountData.account[0].balance - value;
            accountData.account[0].balance = System.Math.Round(result * 100f) / 100f;
            // accountData.account[0].balance -= value;
            if (accountData.account[0].balance < 0.01)
            {
                accountData.account[0].balance = 0;
            }
            if (callbackYes != null)
            {
                callbackYes();
            }
        }
        else
        {
            if (callbackNo != null)
            {
                callbackNo();
            }
            else
            {
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.MoneyLimit);
                Debug.LogWarning(BankManager.Instance.allAccount.accounts[0].accountName + " not enough money");
            }
        }

        CheckMission(State.Mission.Wealth);
    }

    public void SubtractMoney(int _accountID, float _Value, System.Action callbackYes = null, System.Action callbackNo = null)
    {
        float fValue = Mathf.RoundToInt(_Value * 100f) / 100f;
        decimal dValue = (decimal)fValue;
        double value = (double)dValue;
        if (accountData.account[_accountID].balance >= value)
        {
            double result = accountData.account[_accountID].balance - value;
            accountData.account[_accountID].balance = System.Math.Round(result * 100f) / 100f;
            // accountData.account[_accountID].balance -= value;
            if (accountData.account[_accountID].balance < 0.01)
            {
                accountData.account[_accountID].balance = 0;
            }
            
            if (callbackYes != null)
            {
                callbackYes();
            }
        }
        else
        {
            Debug.Log(accountData.account[_accountID].balance + " || " + value);
            if (callbackNo != null)
            {
                callbackNo();
            }
            else
            {
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.MoneyLimit);
                Debug.LogWarning(BankManager.Instance.allAccount.accounts[_accountID].accountName + " not enough money");
            }
        }

        CheckMission(State.Mission.Wealth);
    }

    public void TransferMoney(int _accountID1 , int _accountID2, float _Value, System.Action callbackYes = null, System.Action callbackNo = null)
    {
        float fValue = Mathf.RoundToInt(_Value * 100f) / 100f;
        decimal dValue = (decimal)fValue;
        double value = (double)dValue;
        if ((float)accountData.account[_accountID1].balance >= value)
        {
            double result1 = accountData.account[_accountID1].balance - value;
            accountData.account[_accountID1].balance = System.Math.Round(result1 * 100f) / 100f;
            // accountData.account[_accountID1].balance -= value;

            double result2 = accountData.account[_accountID2].balance + value;
            accountData.account[_accountID2].balance = System.Math.Round(result2 * 100f) / 100f;
            // accountData.account[_accountID2].balance += value;
            if (callbackYes != null)
            {
                callbackYes();
            }
        }
        else
        {
            if (callbackNo != null)
            {
                callbackNo();
            }
            else
            {
                Debug.LogWarning("Can't transfer money from " + BankManager.Instance.allAccount.accounts[_accountID1].accountName + " to " + BankManager.Instance.allAccount.accounts[_accountID2].accountName);
            }
        }

        CheckMission(State.Mission.Saving);
        CheckMission(State.Mission.Wealth);
    }

    public void UseCredit(int _CardIndex, float _Value, string _Name, System.Action callbackYes = null, System.Action callbackNo = null)
    {
        float value = Mathf.RoundToInt(_Value * 100f) / 100f;
        if ((float)liabilityData.creditCard[_CardIndex].enoughCredit >= value)
        {
            float result = liabilityData.creditCard[_CardIndex].usedCredit + value;
            liabilityData.creditCard[_CardIndex].usedCredit = Mathf.Round(result * 100f) / 100f;
            // liabilityData.creditCard[_CardIndex].usedCredit += value;
            PlayerCreditCardElement newElement = new PlayerCreditCardElement();
            newElement.paymentName = _Name;
            newElement.loanAmount = value;

            if (newElement.loanAmount > 0)
            {
                liabilityData.creditCard[_CardIndex].date[(CalManager.Instance.Day() - 1)].element.Add(newElement);
            }

            if (callbackYes != null)
            {
                callbackYes();
            }
        }
        else
        {
            if (callbackNo != null)
            {
                callbackNo();
            }
            else
            {
                UIManager.Instance.uIElement[(int)State.UIState.PopupPage].GetComponent<UIPopupPage>().OpenPopup(State.PopupPage.CreditLimit);
                Debug.Log("NoCredit");
            }
        }

        CheckMission(State.Mission.Wealth);
    }

    public void RecordBalance(PlayerBalanceElement _Balance, State.Expense _State)
    {
        if (_State == State.Expense.None)
        {
            return;
        }
        
        if (_Balance.amount > 0)
        {
            balanceData.balanceElement[CalManager.Instance.Month() - 1].balanceType[(int)_State].expense.Add(_Balance);
            float totalCost = 0;
            for (int i = 0; i < balanceData.balanceElement[CalManager.Instance.Month() - 1].balanceType[(int)_State].expense.Count; i++)
            {
                totalCost += balanceData.balanceElement[CalManager.Instance.Month() - 1].balanceType[(int)_State].expense[i].amount;
            }
            balanceData.balanceElement[CalManager.Instance.Month() - 1].balanceType[(int)_State].totalCost = totalCost;
        }
        else if (_Balance.amount < 0)
        {
            Debug.LogWarning(_Balance.balanceName);
        }
    }

    public void ApplyHappiness(float _Value)
    {
        playerData.happiness += _Value;

        if (playerData.happiness >= playerData.maxHappiness)
        {
            playerData.happiness = playerData.maxHappiness;
        }
        else if (playerData.happiness <= 0)
        {
            playerData.happiness = 0;
        }
    }

    public void ApplyHealth(float _Value)
    {
        playerData.health += _Value;
        
        if (playerData.health >= playerData.maxHealth)
        {
            playerData.health = playerData.maxHealth;
        }
        else if (playerData.health <= 0)
        {
            playerData.health = 0;
        }
    }

    public void ApplyCool(float _Value)
    {
        playerData.cool += _Value;
    }

    public void DoTask(int _Task = 1)
    {
        jobData.fullTimeJob.dailyWorked += (playerData.lvSkill.JobSpeed * _Task);
        jobData.currenTask += (playerData.lvSkill.JobSpeed * _Task);
        // JobManager.Instance.Job[(int)State.JobType.FullTime].exp += jobData.fullTimeJob.workTimeHour;

        if (jobData.fullTimeJob.workpoint > 0 
        && jobData.currenTask >= jobData.fullTimeJob.workpoint)
        {
            jobData.currenTask = 0;
            jobData.completedTask += 1;
            jobData.fullTimeJob.dailyFinish += 1;
        }

        if (jobData.fullTimeJob.jobID < JobManager.Instance.Job[(int)State.JobType.FullTime].jobs.Length
        && jobData.completedTask >= jobData.fullTimeJob.kpi)
        {
            jobData.completedTask = 0;
            JobManager.Instance.ApplyFullTime(JobManager.Instance.Job[(int)State.JobType.FullTime].jobs[jobData.fullTimeJob.jobID + 1]);
        }
    }

    public void BuyLotto(int _Number, int _Amount = 1)
    {
        int _Index = -1;
        for (int i = 0; i < assetData.lottoHistory[BankManager.Instance.lottoData.drawTime].lottery.Count; i++)
        {
            if (_Number == assetData.lottoHistory[BankManager.Instance.lottoData.drawTime].lottery[i].lotto)
            {
                _Index = i;
                break;
            }
        }

        if (_Index > -1)
        {
            if (_Number == assetData.lottoHistory[BankManager.Instance.lottoData.drawTime].lottery[_Index].lotto)
            {
                assetData.lottoHistory[BankManager.Instance.lottoData.drawTime].lottery[_Index].amount += _Amount;
            }
        }
        else
        {
            Lotto newLotto = new Lotto();
            newLotto.lotto = _Number;
            newLotto.amount = _Amount;
            assetData.lottoHistory[BankManager.Instance.lottoData.drawTime].lottery.Add(newLotto);
        }
    }

    // Tax

    public void ApplyTaxIncome(float _Value, int _Index = 0)
    {
        // taxData.totalIncome += (float)_Value;
        taxData.income[_Index].incomeSum += _Value;
    }

    public void ResetTax()
    {
        taxData.totalIncome = 0;
        for (int i = 0; i < taxData.income.Length; i++)
        {
            taxData.income[i].incomeSum = 0;
        }

        taxData.totalTax = 0;
        for (int i = 0; i < taxData.tax.Length; i++)
        {
            taxData.tax[i].taxFill = 0;
            taxData.tax[i].taxCal = 0;
        }
        
        taxData.totalDeduction = 0;
        for (int i = 0; i < taxData.deduction.Length; i++)
        {
            taxData.deduction[i].deductionRec = 0;
        }

        Debug.Log("Tax Reseted");
    }

    public void CalculateTax()
    {
        CalculateIncome();
        CalculateDeduction();

        taxData.totalTax = 0;
        float newNetIncome = taxData.netIncome;

        for (int i = 1; i < taxData.tax.Length; i++)
        {
            if (taxData.tax[i].taxLength >=0)
            {
                float taxLength = taxData.tax[i].taxLength - taxData.tax[i - 1].taxLength;
                
                if (newNetIncome <= 0)
                {
                    break;
                }
                else if (newNetIncome >= taxLength)
                {
                    taxData.tax[i].taxFill = taxLength;
                    newNetIncome -= taxLength;
                    
                }
                else if (newNetIncome < taxLength)
                {
                    taxData.tax[i].taxFill = newNetIncome;
                    newNetIncome = 0;
                }
            }
            else
            {
                taxData.tax[i].taxFill = newNetIncome;
                newNetIncome = 0;
            }

            taxData.tax[i].taxCal = taxData.tax[i].taxFill * taxData.tax[i].taxRate / 100f;
            taxData.totalTax += taxData.tax[i].taxCal;
        }
    }

    public void CalculateIncome()
    {
        taxData.totalIncome = 0;

        for (int i = 0; i < taxData.income.Length; i++)
        {
            taxData.income[i].incomeHidden = 0;

            if (i == 0)
            {
                int monthPast = 0;
                if (CalManager.Instance.Day() == CalManager.Instance.DAY_IN_MONTH)
                {
                    monthPast = CalManager.Instance.Month();
                }
                else if (CalManager.Instance.Day() == 1 && CalManager.Instance.Month() == 1
                && StateManager.Instance.currentDayState < State.DayState.Night)
                {
                    monthPast = CalManager.Instance.MONTH_IN_YEAR;
                }
                else if (CalManager.Instance.Day() == 1 && CalManager.Instance.Month() == 1
                && StateManager.Instance.currentDayState == State.DayState.Night)
                {
                    if (taxData.income[i].incomeSum != 0)
                    {
                        monthPast = CalManager.Instance.MONTH_IN_YEAR;
                    }
                    else
                    {
                        monthPast = CalManager.Instance.Month() - 1;
                    }
                }
                else
                {
                    monthPast = CalManager.Instance.Month() - 1;
                }
                taxData.income[i].incomeHidden += jobData.fullTimeJob.salary * (CalManager.Instance.MONTH_IN_YEAR - monthPast);
                
                // Debug.Log(monthPast);
                // Debug.Log(taxData.income[i].incomeSum);
                // Debug.Log(taxData.income[i].incomeHidden);
            }
            taxData.totalIncome += taxData.income[i].incomeSum;
            taxData.totalIncome += taxData.income[i].incomeHidden;
        }
    }

    public void CalculateDeduction()
    {
        taxData.totalDeduction = 0;

        for (int i = 0; i < taxData.deduction.Length; i++)
        {
            CalculateDeduction(i);
            taxData.totalDeduction += taxData.deduction[i].deductionCal;
        }
    }

    public void CalculateDeduction(int _Index)
    {
        switch (_Index) 
        {
            case (int)State.Deduction.FixedCost:
                taxData.deduction[_Index].deductionRec = taxData.totalIncome;
                break;
            case (int)State.Deduction.Personal:
                taxData.deduction[_Index].deductionRec = taxData.deduction[_Index].deductionLimit;
                break;
            // case (int)State.Deduction.DeptEstate:
                
            //     break;
            // case (int)State.Deduction.LifeInsurance:
                
            //     break;
            // case (int)State.Deduction.HealthInsurance:
                
            //     break;
            // case (int)State.Deduction.SSFfund:
                
            //     break;
            // case (int)State.Deduction.RMFfund:
                
            //     break;
            // case (int)State.Deduction.Donation:
                
            //     break;
        }

        taxData.deduction[_Index].deductionCal = taxData.deduction[_Index].deductionRec;

        if (taxData.deduction[_Index].deductionCal <= 0)
        {
            return;
        }

        float CalLimit = 0;
        if (taxData.deduction[_Index].deductionRate > 0)
        {
            CalLimit = (taxData.deduction[_Index].deductionRate / 100) * taxData.totalIncome;
        }

        if (CalLimit > taxData.deduction[_Index].deductionLimit || CalLimit == 0)
        {
            CalLimit = taxData.deduction[_Index].deductionLimit;
        }

        if (taxData.deduction[_Index].deductionCal > CalLimit)
        {
            taxData.deduction[_Index].deductionCal = CalLimit;
        }
    }

    public void CheckMission(State.Mission _Type)
    {
        missionData.allMission = State.Mission.None;
        
        if (_Type == State.Mission.Saving)
        {
            for (int i = 0; i < missionData.saving.Length; i++)
            {
                if (!missionData.saving[i].isCompleted)
                {
                    missionData.saving[i].isCompleted = accountData.account[1].balance >= missionData.saving[i].missionTarget;
                }
            }
        }
        else if (_Type == State.Mission.Wealth)
        {
            for (int i = 0; i < missionData.wealth.Length; i++)
            {
                if (!missionData.wealth[i].isCompleted)
                {
                    missionData.wealth[i].isCompleted = AssetValue >= missionData.wealth[i].missionTarget;
                }
            }
        }
        else if (_Type == State.Mission.Invest)
        {
            for (int i = 0; i < missionData.invest.Length; i++)
            {
                if (!missionData.invest[i].isCompleted)
                {
                    missionData.invest[i].isCompleted = missionData.investTotal >= missionData.invest[i].missionTarget;
                }
            }
        }
    }

    public void TotalInvest(float _Value)
    {
        missionData.investTotal += _Value;
    }
}
