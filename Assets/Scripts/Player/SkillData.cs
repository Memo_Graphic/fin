﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SkillElement
{
    public string skill = "skillName";
    public int JobSpeed;
    public int maxEXP;
    public int currentEXP;
}


[System.Serializable]
public class UpSkillElement
{
    public string skillName = "UpSkill";
    public float cost;
    public int learnTime;
    public int getSpeed;
    public int getExp;
    public int maxExp;
    public float useHealth;
    public float useHappy;
}