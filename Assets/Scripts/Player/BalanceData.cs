using System.Collections.Generic;

[System.Serializable]
public struct BalanceData
{
    public List<BalanceTypeElement> balanceElement;
}

[System.Serializable]
public class BalanceTypeElement
{
    public List<BalanceElement> balanceType;
    public float totalExpense
    {
        get
        {
            float total = 0;
            for (int i = 0; i < balanceType.Count; i++)
            {
                total += balanceType[i].totalCost;
            }
            return total;
        }
    }
}

[System.Serializable]
public class BalanceElement
{
    public string expenseName;
    public float totalCost;
    public List<PlayerBalanceElement> expense;
}

[System.Serializable]
public class PlayerBalanceElement
{
    public string balanceName = "balanceName";
    // หมวดของรายจ่าย
    public float amount;
    public int accountID;
}