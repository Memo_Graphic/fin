﻿using System.Collections.Generic;

[System.Serializable]
public struct PlayerData
{
    [UnityEngine.Header("Date")]
    public TimeMinute Time;
    public int sleepTimeInHour;


    [UnityEngine.Header("Profile")]
    public string playerName;
    public string playerGender;
    public int lifespan;
    public int retire;
    public int age;

    [UnityEngine.Header("Status")]
    public float happiness;
    public float maxHappiness;
    public float health;
    public float maxHealth;
    public float cool;


    [UnityEngine.Header("Skill")]
    public State.Skill allSkill;
    public SkillElement lvSkill;
}

[System.Serializable]
public struct MissionData
{
    public State.Mission allMission;
    public StackMission[] saving;
    public StackMission[] wealth;
    public float investTotal;
    public StackMission[] invest;
}

[System.Serializable]
public class StackMission
{
    public string missionName;
    public bool isCompleted;
    public int missionTarget;
    public int missionStar;
}