using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoanElement : MonoBehaviour
{
    [Header("TMP")]
    [SerializeField] private TextMeshProUGUI m_IRText;
    [SerializeField] private TextMeshProUGUI m_CurrentLoanText;
    [SerializeField] private TextMeshProUGUI m_MaxLoanText;
    [SerializeField] private TextMeshProUGUI m_TermText;
    [SerializeField] private TextMeshProUGUI m_InstallmentText;
    [SerializeField] private TextMeshProUGUI m_InterestText;

    [Header("Slider")]
    [SerializeField] private Slider m_LoanSlider;

    [Header("GameObject")]
    [SerializeField] private GameObject m_HaveObj;

    private int index;
    private float loanAmount;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetLoan(int _Index)
    {
        index = _Index;
        m_IRText.text = "อัตราดอกเบี้ย " + BankManager.Instance.currentIR.ToString("N2") + "% /ปี";
        
        if (index >= 0)
        {
            PlayerLiabilityElement newPersonal = PlayerManager.Instance.liabilityData.Liability[(int)State.LiabilityType.Personal].element[index];

            m_CurrentLoanText.text = newPersonal.loanAmount.ToString("N0") + " ฿";
            m_MaxLoanText.text = "/" + newPersonal.maxAmount.ToString("N0") + " ฿";
            m_TermText.text = "(" + (newPersonal.maxTerm - newPersonal.term + 1) + "/" + newPersonal.maxTerm.ToString("N0") + ")";
            
            loanAmount = Mathf.RoundToInt((float)(decimal)newPersonal.loanAmount * 100f) / 100f;
            if (newPersonal.term > 1)
            {
                loanAmount = Mathf.RoundToInt((float)(decimal)newPersonal.loanAmount * 100f) / 100f;
            }
            float interest = loanAmount * (newPersonal.irPerMonth);
            m_InstallmentText.text = newPersonal.installment.ToString("N0") + " ฿";
            m_InterestText.text = interest.ToString("N0") + " ฿";

            m_LoanSlider.maxValue = (float)newPersonal.maxAmount;
            m_LoanSlider.maxValue = (float)newPersonal.loanAmount;

            // loanAmount = (float)newPersonal.loanAmount;
            Debug.Log(loanAmount);

            m_HaveObj.SetActive(true);
        }
        else
        {
            m_HaveObj.SetActive(false);
        }
    }

    public void OnClickPayAll()
    {
        PlayerManager.Instance.SubtractMoney(loanAmount,
                                () =>
                                {
                                    BankManager.Instance.RemoveLiability(index, State.LiabilityType.Personal);
                                    UIManager.Instance.UpdateCurrentUI();
                                    Debug.Log(loanAmount);
                                    DestroySelf();
                                });
    }

    public void OnClickLoan()
    {
        UIManager.Instance.uIElement[(int)State.UIState.LoanPage].GetComponent<UILoanPage>().OpenLoanPage();
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
