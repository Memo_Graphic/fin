using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

public class SaveManager : Singleton<SaveManager>
{
    public const string FILE_NAME_GAME = "GDP_Game_SaveData.json";
    public string GAME_DATA_PATH
    {
        get
        {
            return Path.Combine(Application.persistentDataPath, FILE_NAME_GAME);
        }
    }
    public bool IS_LOAD;
    public bool IS_ONLINE_SAVE;
    public bool IS_RAND_ID;

    public GameData gameData;
    
    [UnityEngine.HideInInspector]
    public string uniqueID;
    private string glyphs= "abcdefghi13579jklmnopq0123456789rstuvwxyz02468";
    public float timer = 10;

    public bool IsHaveGameData()
    {
        if (File.Exists(GAME_DATA_PATH))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (IS_ONLINE_SAVE)
        {
            if (GameManager.Instance.isTutorial)
            {
                if (timer > 0)
                {
                    timer -= Time.deltaTime;
                }
                else
                {
                    TimeAction();
                }
            }
        }
    }

    void TimeAction()
    {
        timer = 10;
        OnRegisterWithCustomID();
    }

    // private void SettingTitleID(string _TitleId)
    // {
    //     if (string.IsNullOrEmpty(PlayFabSettings.staticSettings.TitleId))
    //     {
    //         PlayFabSettings.staticSettings.TitleId = _TitleId;
    //     }
    // }

    public void OnRegisterWithCustomID()
    {
        uniqueID = GetUID();
        var request = new LoginWithCustomIDRequest { CreateAccount = true, CustomId = uniqueID };
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
    }

    private void OnLoginSuccess(LoginResult result)
    {
        Debug.Log("OnLoginSuccess: " + uniqueID);
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.LogError(error.GenerateErrorReport());
    }
    
    public string GetUID(bool AutoGen = false)
    {
        string deviceID = "ABC";
        if (IS_RAND_ID || AutoGen)
        {
            int charAmount = 32;
            string newString = "";
            for (int i = 0; i < charAmount; i++)
            {
                newString += glyphs[Random.Range(0, glyphs.Length)];
            }
            deviceID = newString;
        }
        else
        {
            deviceID = SystemInfo.deviceUniqueIdentifier;
        }
        return deviceID;
    }

    public void SaveGameData()
    {
        gameData = new GameData();

        gameData.playerData = PlayerManager.Instance.playerData;
        gameData.jobData = PlayerManager.Instance.jobData;
        gameData.balanceData = PlayerManager.Instance.balanceData;
        gameData.accountData = PlayerManager.Instance.accountData;
        gameData.taxData = PlayerManager.Instance.taxData;
        gameData.billData = PlayerManager.Instance.billData;
        gameData.assetData = PlayerManager.Instance.assetData;
        gameData.liabilityData = PlayerManager.Instance.liabilityData;
        gameData.missionData = PlayerManager.Instance.missionData;

        gameData.hospitalData = ActionManager.Instance.hospitalData;
        gameData.locationData = LocationManager.Instance.locationData;
        gameData.scamData = BankManager.Instance.scamData;
        gameData.lottoData = BankManager.Instance.lottoData;

        gameData.gameLevel = GameManager.Instance.gameLevel;
        gameData.currentState = StateManager.Instance.currentState;
        gameData.currentDayState = StateManager.Instance.currentDayState;
        gameData.realDay = CalManager.Instance.realDay;
        gameData.useCredit = ActionManager.Instance.useCredit;

        gameData.Asset = new AssetElement[AssetManager.Instance.Asset.Length];
        for (int i = 0; i < AssetManager.Instance.Asset.Length; i++)
        {
            gameData.Asset[i] = AssetManager.Instance.Asset[i];
        }

        gameData.insurances = new InsuranceElement[AssetManager.Instance.insurances.Length];
        for (int i = 0; i < AssetManager.Instance.insurances.Length; i++)
        {
            gameData.insurances[i] = AssetManager.Instance.insurances[i];
        }

        gameData.activityPlanner = new ActivityPlanner[ActionManager.Instance.activityPlanner.Count];
        for (int i = 0; i < ActionManager.Instance.activityPlanner.Count; i++)
        {
            gameData.activityPlanner[i] = ActionManager.Instance.activityPlanner[i];
        }

        string json = JsonUtility.ToJson(gameData, true);
        Save(json, GAME_DATA_PATH);
    }

    public void LoadGameData()
    {
        LoadGameDataFromJson();
        PlayerManager.Instance.playerData = gameData.playerData;
        PlayerManager.Instance.jobData = gameData.jobData;
        PlayerManager.Instance.balanceData = gameData.balanceData;
        PlayerManager.Instance.accountData = gameData.accountData;
        PlayerManager.Instance.taxData = gameData.taxData;
        PlayerManager.Instance.billData = gameData.billData;
        PlayerManager.Instance.assetData = gameData.assetData;
        PlayerManager.Instance.liabilityData = gameData.liabilityData;
        if (gameData.missionData.allMission != State.Mission.Saving)
        {
            PlayerManager.Instance.missionData = gameData.missionData;
        }

        ActionManager.Instance.hospitalData = gameData.hospitalData;
        LocationManager.Instance.locationData = gameData.locationData;
        BankManager.Instance.scamData = gameData.scamData;
        BankManager.Instance.lottoData = gameData.lottoData;

        GameManager.Instance.gameLevel = gameData.gameLevel;
        StateManager.Instance.currentState = gameData.currentState;
        StateManager.Instance.currentDayState = gameData.currentDayState;
        CalManager.Instance.realDay = gameData.realDay;
        ActionManager.Instance.useCredit = gameData.useCredit;

        for (int i = 0; i < AssetManager.Instance.Asset.Length; i++)
        {
            AssetManager.Instance.Asset[i] = gameData.Asset[i];
        }

        for (int i = 0; i < AssetManager.Instance.insurances.Length; i++)
        {
            AssetManager.Instance.insurances[i] = gameData.insurances[i];
        }

        ActionManager.Instance.activityPlanner = new List<ActivityPlanner>(gameData.activityPlanner);
        // for (int i = 0; i < ActionManager.Instance.activityPlanner.Count; i++)
        // {
        //     gameData.activityPlanner[i] = ActionManager.Instance.activityPlanner[i];
        // }
        
        UIManager.Instance.UpdateUI(State.UIState.MainPage);
    }

    public void LoadGameDataFromJson()
    {
        if (File.Exists(GAME_DATA_PATH))
        {
            string dataAsJson = File.ReadAllText(GAME_DATA_PATH);
            gameData = JsonUtility.FromJson<GameData>(dataAsJson);
        }
        else
        {
            Debug.LogError("Cannot load game data! ");
        }
    }

    public static void Save<T>(T data, string path)
    {
        using (StreamWriter streamWriter = File.CreateText(path))
        {
            streamWriter.Write(data);
        }
    }

    public static T Load<T>(string _path)
    {
        if (File.Exists(_path))
        {
            string dataAsJson = File.ReadAllText(_path);
            return JsonUtility.FromJson<T>(dataAsJson);
        }
        else
        {
            Debug.LogError("Cannot load game data!");
            return default(T);
        }
    }

    public void DeleteFile(string _path)
    {
        if(File.Exists(_path))
        {
            File.Delete(_path);
        }
    }
}
